# Shijing-net

Project launched by Marie Bizais-Lillig (Principal Investigator)

Research Engineer: Ilaine Wang


## Description

Shijing-net is part of the CHI-KNOW-PO Project which aims at shedding light on the role played by poetry in the economy of
knowledge in Medieval China. In order to identify cooccurrences and textual influences down to the
Tang Dynasty (618-907) among the Classic of Poetry, poetry in general, but also sources of knowledge
such as commentaries, encyclopedias, technical treatises and lexicons, this project requires a large
diachronic corpus of Ancient and Medieval Chinese Texts.

The Shijing-net data set is composed of the poetic *Shijing* 詩經 Anthology and its orthodox commentaries by Mao, Zheng Xuan and Kong Yingda, along with Lu Deming's phonetic commentaries.

As a means to establish a version of reference of the poems and their commentaries, it has been decided to lean on Ruan Yuan’s 阮元 (1764-1849) xylographic edition of the *Thirteen Classics* whose scanned version stands nowadays as a reference. Within the Classics, our digital edition focuses on Kong Yingda's 孔穎達 (574-648) *Maoshi Zhengyi*: Kong Yingda (ed.), *Mao* Shi *Zhengyi* 毛詩正義 [The right meaning of the *Poems* according to Mao], In *Shisan jing zhushu* 十三經注疏 [The Thirteen Classics with Commentaries], Shanghai, Shanghai guji chubanshe, 1997, pp. 259–629.

However, other editions are taken into account. For precise reference, *cf.* the header of XML-TEI files.

## Content

In this repository, you may find:
- `/corpus`: the Shijing corpus in [XML-TEI](https://tei-c.org/) (106 poems out of 311 in total in January 2021 but will be frequently updated) ;
- `/database`: concordance tables containing metadata useful to understand the corpus, as well as two tables that list all references within the poems (these tables are also subject to continuous update) ;
- `/scripts`: all the scripts that were used to build the corpus ;
- `/analysis`: an analysis of the similarities among the poems, performed with an adaptation of P. Vierthaler's [chinesetextreuse](https://github.com/vierth/chinesetextreuse) program, available at https://github.com/ilaine/chinesetextreuse.

More information about the corpus, the tables, and work flow is to be found in relevant "Readme" files distributed in each folder.

## License

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

The corpus is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

Scripts are licensed under the MIT license (see corresponding `LICENSE` in `scripts`). 
