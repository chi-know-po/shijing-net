# Shijing-net: Databases

This part of the repository contains databases in relation to a corpus composed of the poetic *Shijing* 詩經 Anthology and its orthodox commentaries by Mao, Zheng Xuan and Kong Yingda, along with Lu Deming's phonetic commentaries.


## Concordance table of the Anthology

It includes a concordance table between the Anthology's list of sections and poems and the names chosen for the files of the digital edition of the corpus. This table is organized as a tsv file with the following columns:
- `Poem N° Shi`: poem number in the received anthology
- `Title Poem Shi`: poem title in Chinese
- `Subsection (Shi KYD)`: title of the subsection to which the poem belongs in Kong Yingda's edition (Kong 1997)
-	`Section Shi`: title of the section to which the poem belongs
- `Alternative Classification Shi`: alternative classification (the poem is included in a subsection with a different title)
- `File Title`: original file name
- `Generated Title`: automatically generated file name, actually used in our corpus
- `Link Ctext`: link to the digital edition of the poem in the [Chinese Text Project website](https://ctext.org/library.pl?if=en&file=80142)
- `Beginning Page`: first page of the poem in the Reprint of the *Siku Quanshu* Edition
- `End Page`: last page of the poem in the Reprint of the *Siku Quanshu* Edition

## References

Two tables list references within the poems (people and textual references). Both tables establish identifiers for all references. They are meant to allow additional tagging within XMl-TEI files for quotations.

In both tables, references are sorted by frequency: the first references are the most frequent in the whole corpus.
Please bear in mind that these lists were built automatically using textual hints (quotation verbs and quotes) and are therefore not exhaustive. A manual revision shall complete this work soon.

### Database of people

- `name`: reference as appearing in the *Shijing*
- `ID`: ID from the [VIAF database](http://viaf.org/) if existent, created identifier in other cases
- `nb`: frequency in the *Shijing*
- `comment`: comments by Marie Bizais-Lillig

### Database of textual references

- `name`: reference as appearing in the *Shijing*
- `complete name`: complete reference's name
- `type`: type of the reference (book, chapter, poem, *etc.*)
- `section of`: name of the work from which it was extracted, if relevant
- `author ID`: author's ID from the [VIAF database](http://viaf.org/) if existent, created identifier in other cases
- `ID`: created identifier for the textual reference listed
- `link`: link to the [Chinese Text Project database](https://ctext.org)
- `comment`: comments by Marie Bizais-Lillig
- `nb`: frequency in the *Shijing*
