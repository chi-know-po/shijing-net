# Shijing-net: Corpus



## References for edition

This part of the Shijing-net repository contains a corpus composed of the poetic *Shijing* 詩經 Anthology and its orthodox commentaries by Mao, Zheng Xuan and Kong Yingda, along with Lu Deming's phonetic commentaries.

As a means to establish a version of reference of the poems and their commentaries, it has been decided to lean on Ruan Yuan’s 阮元 (1764-1849) xylographic edition of the *Thirteen Classics* whose scanned version stands nowadays as a reference. Within the Classics, our digital edition focuses on Kong Yingda's 孔穎達 (574-648) *Maoshi Zhengyi*: Kong Yingda (ed.), *Mao* Shi *Zhengyi* 毛詩正義 [The right meaning of the *Poems* according to Mao], In *Shisan jing zhushu* 十三經注疏 [The Thirteen Classics with Commentaries], Shanghai, Shanghai guji chubanshe, 1997, pp. 259–629.

However, other editions are taken into account. For precise reference, *cf.* the header of XML-TEI files.

## Editorial choices

In order to allow research on distinct parts of the corpus (*i.e.* poems, prefaces, commentaries) and each commentator's parts within prefaces and commentaries, each stata was tagged in xml-tei files.

Also, each commentary and subcommentary points to its target (*i.e.* the part of the text it comments upon).

## Editorial process

Original text was extracted from [Wikisource](https://zh.wikisource.org/wiki/%E6%AF%9B%E8%A9%A9%E6%AD%A3%E7%BE%A9) and semi-automatically annotated in a human-readable raw format by Ilaine Wang (automatic preannotation) and Marie Bizais-Lillig (expert manual annotation).

Using a script (`/scripts/txt2tei.py`), the txt files were then automatically converted into XML-TEI, the recommended format for resources in Digital Humanities. This format also allows us to select and study distinct parts of the corpus, i.e. poems, prefaces, commentaries and each commentator's parts within prefaces and commentaries, either altogether or separately. For example, the preliminary study in `/analysis` is based on the prefaces alone.

For precise information on sources used to edit texts, please refer to the metadata in the header of each XML-TEI file.

## For further tagging

The XML-TEI files are based on txt files. It is therefore advised to modify the txt files and to convert them into XML-TEI using the given script for further modification of the annotation.
