黃鳥

S1
E1	交交黃鳥，止於棘。
E2	誰從穆公？子車奄息。
E3	維此奄息，百夫之特。
E4	臨其穴，惴惴其栗。
E5	彼蒼者天，殲我良人！
E6	如可贖兮，人百其身！
S2
E1	交交黃鳥，止於桑。
E2	誰從穆公？子車仲行。
E3	維此仲行，百夫之防。
E4	臨其穴，惴惴其慄。
E5	彼蒼者天，殲我良人！
E6	如可贖兮，人百其身！
S3
E1	交交黃鳥，止於楚。
E2	誰從穆公？子車針虎。
E3	維此針虎，百夫之禦。
E4	臨其穴，惴惴其栗。
E5	彼蒼者天，殲我良人！
E6	如可贖兮，人百其身！

preface
Mao	《黃鳥》，哀三良也。國人刺穆公以人從死，而作是詩也。
ZX	三良，三善臣也，謂奄息、仲行、鍼虎也。從死，自殺以從死。
LDM	行，戶郎反，下皆同。鍼，其廉反，徐又音針。從死，上才容反。
sous-commentaire KYD
ZX	箋「三良」至「從死」。
正義曰：文六年《左傳》云：「秦伯任好卒，以子車氏之三子奄息、仲行、針虎為殉，皆秦之良也。國人哀之，為之賦《黃鳥》。」服虔云：「子車，秦大夫氏也。殺人以葬，璿環其左右曰殉。」又《秦本紀》云：「穆公卒，葬於雍，從死者百七十人。」然則死者多矣。主傷善人，故言「哀三良也」。殺人以殉葬，當是後有為之，此不刺康公，而刺穆公者，是穆公命從己死，此臣自殺從之，非後主之過，故箋辯之云：「從死，自殺以從死。」

S1
E1 交交黃鳥，止於棘。
commentaire
Mao	興也。交交，小貌。黃鳥以時往來得其所，人以壽命終亦得其所。
ZX	箋云：黃鳥止於棘，以求安己也。此棘若不安則移，興者，喻臣之事君亦然。今穆公使臣從死，刺其不得黃鳥止於棘之本意。
E2 誰從穆公？子車奄息。
commentaire
Mao	子車，氏。奄息，名。
ZX	箋云：言誰從穆公者，傷之。
E3 維此奄息，百夫之特。
commentaire
Mao	乃特百夫之德。
ZX	箋云：百夫之中最雄俊也。
E4 臨其穴，惴惴其栗。
commentaire
Mao	栗栗，懼也。
ZX	箋云：穴，謂塚壙中也。秦人哀傷此奄息之死，臨視其壙，皆為之悼栗。
LDM	惴，之瑞反。慄音栗。壙，苦晃反。
E5 彼蒼者天，殲我良人！
commentaire
Mao	殲盡良善也。
ZX	箋云：言彼蒼者天，之。
LDM	殲，子廉反，徐又息廉反。，蘇路反
E6 如可贖兮，人百其身！
commentaire
ZX	箋云：如此奄息之死，可以他人贖之者，人皆百其身。謂一身百死猶為之，惜善人之甚。
LDM	贖，食燭反，又音樹。
sous-commentaire KYD
S1 疏「交交」至「其身」。
<--!avis Mao-->毛以為，交交然而小者，是黃鳥也。黃鳥飛而往來，止於棘木之上，得其所，以興人以壽命終亦得其所。今穆公使良臣從死，是不得其所也。有誰從穆公死乎？有子車氏名奄息者從穆公死也。此奄息何等人哉？乃是百夫之中特立雄俊者也。今從穆公而死，秦人悉哀傷之，臨其壙穴之上，皆惴惴然恐懼而其心悼栗。乃之於天，彼蒼蒼者是在上之天，今穆公盡殺我善人也，如使此人可以他人贖代之兮，我國人皆百死其身以贖之。愛惜良臣，寧一人百死代之。<--!avis ZX-->鄭以為，交交然之黃鳥，止於棘木以求安。棘若不安則移去。以興臣仕於君，以求行道，道若不行則移去。言臣有去留之道，不得生死從君。今穆公以臣從死，失仕於君之本意。餘同。
Mao E1	傳「交交」至「其所」。
正義曰：黃鳥，小鳥也，故以交交為小貌。《桑扈》箋云：「交交猶佼佼，飛而往來貌。」則此亦當然，故云「往來得其所」，是交交為往來狀也。以此哀三良不得其所，故以鳥止得所，喻人命終得所。
ZX E1	箋「黃鳥」至「本意」。
正義曰：箋以鳥之集木，似臣之仕君，故易傳也。以鳥止木，喻臣仕君，故言「不得黃鳥止於棘之本意」，正謂不得臣仕於君之本意也。言其若得鳥止之意，知有去留之道，則不當使之從死。
Mao E2	傳「子車，氏。奄息，名」。
正義曰：《左傳》作「子輿」，輿、車字異義同。傳以奄息為名，仲行亦為名。箋以仲行為字者，以伯仲叔季為字之常，故知仲行是字也。然則針虎亦名矣。或名或字，取其韻耳。
Mao E3	傳「乃特百夫之德」。
正義曰：言百夫之德，莫及此人。此人在百夫之中，乃孤特秀立，故箋申之云：「百夫之中最雄俊也。」
Mao E4	傳「惴惴，懼」。
正義曰：《釋訓》文。

S2
E1 交交黃鳥，止於桑。
E2 誰從穆公？子車仲行。
commentaire
ZX	箋云：仲行，字也。
E3 維此仲行，百夫之防。
commentaire
Mao	防，比也。
ZX	箋云：防猶當也。言此一人當百夫。
LDM	防，徐云：「毛音方，鄭音房。」
E4 臨其穴，惴惴其慄。
E5 彼蒼者天，殲我良人！
E6 如可贖兮，人百其身！

S3
E1 交交黃鳥，止於楚。
E2 誰從穆公？子車針虎。
E3 維此針虎，百夫之禦。
commentaire
Mao	禦，當也。
LDM	禦，魚呂反，注同。
E4 臨其穴，惴惴其栗。
E5 彼蒼者天，殲我良人！
E6 如可贖兮，人百其身！

fin
《黃鳥》三章，章十二句。