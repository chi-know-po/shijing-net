<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風鄭風之遵大路（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Zundalu in the Zhengfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="340" to="340"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=78</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj081" type="preface">
        <ab resp="#Mao" xml:id="sj081_com_Mao">《遵大路》，思君子也。莊公失道，君子去之，國人思望焉。</ab>
      </div>
    </div>
    <div type="poem" xml:id="sj081">
      <lg type="stanza" xml:id="sj081s01">
        <lg type="couplet" xml:id="sj081s01e1">
          <l xml:id="sj081s01l1">遵大路兮，</l>
          <l xml:id="sj081s01l2">摻執子之袪兮！</l>
        </lg>
        <lg type="couplet" xml:id="sj081s01e2">
          <l xml:id="sj081s01l3">無我惡兮，</l>
          <l xml:id="sj081s01l4">不寁故也。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj081s02">
        <lg type="couplet" xml:id="sj081s02e1">
          <l xml:id="sj081s02l1">遵大路兮，</l>
          <l xml:id="sj081s02l2">摻執子手兮！</l>
        </lg>
        <lg type="couplet" xml:id="sj081s02e2">
          <l xml:id="sj081s02l3">無我魗兮，</l>
          <l xml:id="sj081s02l4">不寁好也！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj081s01" type="commentaries">
      <div source="#sj081s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj081s01e1_com_Mao">
          <ab>遵，循。路，道。摻，攬。袪，袂也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj081s01e1_com_ZX">
          <head>箋云：</head>
          <ab>思望君子，於道中見之，則欲攬持其袂而留之。</ab>
        </div>
        <div resp="#LDM" xml:id="sj081s01e1_com_LDM">
          <ab>摻，所覽反；徐所斬反。袪，起居反，又起據反，袂也。攬音覽。袂，麵世反。</ab>
        </div>
      </div>
      <div source="#sj081s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj081s01e2_com_Mao">
          <ab>寁，速也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj081s01e2_com_ZX">
          <head>箋云：</head>
          <ab>子無惡我攬持子之袂，我乃以莊公不速於先君之道使我然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj081s01e2_com_LDM">
          <ab>惡，烏路反，注同。寁，市坎反。「故也」，一本作「故兮」。後「好也」亦爾。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj081s01">
          <head>疏「遵大」至「故也」。正義曰：</head>
          <ab>國人思望君子，假說得見之狀，言己循彼大路之上兮，若見此君子之人，我則攬執君子之衣袪兮。君子若忿我留之，我則謂之云：無得於我之處怨惡我留兮，我乃以莊公不速於先君之道故也。言莊公之意，不速於先君之道，不愛君子，令子去之，我以此固留子。</ab>
        </div>
        <div source="#sj081s01e1_com_Mao">
          <head>傳「遵循」至「袪袂」。正義曰：</head>
          <ab>「遵，循」，《釋詁》文。《地官·遂人》云：「澮上有道，川上有路。」對文則有廣狹之異，散則道路通也。以摻字從手，又與執共文，故為攬也。《說文》摻字，參山音反聲，訓為斂也。操字，喿此遙反聲，訓為奉也。二者義皆小異。《喪服》云：「袂屬幅。袪尺二寸。」則袂是袪之本，袪為袂之末。《唐·羔裘》傳云：「袪，袂末。」則袂、袪不同。此云「袪，袂」者，以袪、袂俱是衣袖，本末別耳，故舉類以曉人。《唐風》取本末為義，故言「袂末」。</ab>
        </div>
        <div source="#sj081s01e2_com_Mao">
          <head>傳「寁，速」。正義曰：</head>
          <ab>《釋詁》文。舍人曰：「寁，意之速。」</ab>
        </div>
      </div>
    </div>
    <div source="#sj081s02" type="commentaries">
      <div source="#sj081s02e1" type="commentary">
        <div resp="#ZX" xml:id="sj081s02e1_com_ZX">
          <head>箋云：</head>
          <ab>言執手者，思望之甚。</ab>
        </div>
      </div>
      <div source="#sj081s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj081s02e2_com_Mao">
          <ab>魗，棄也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj081s02e2_com_ZX">
          <head>箋云：</head>
          <ab>魗亦惡也。好猶善也。子無惡我，我乃以莊公不速於善道使我然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj081s02e2_com_LDM">
          <ab>魗，本亦作「&lt;壽欠&gt;」，又作「&lt;壽殳&gt;」，市由反。或云鄭音為醜。好如字，鄭云：「善也。」或呼報反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj081s02e2_com_Mao">
          <head>疏傳「壽鬼，棄」。正義曰：</head>
          <ab>魗與醜古今字。醜惡，可棄之物，故傳以為棄。言子無得棄遺我。箋準上章，故云「魗亦惡」，意小異耳。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《遵大路》二章，章四句。</ab>
  </back>
</text>
</TEI>

