<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風秦風之權輿（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Quanyu in the Qinfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="374" to="374"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷六</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷六</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80145&amp;amp;page=51</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjQinfeng">
        <ab resp="#Mao" xml:id="sjQinfeng_com_Mao">《權輿》，刺康公也。忘先君之舊臣，與賢者有始而無終也。</ab>
        <ab resp="#LDM" xml:id="sjQinfeng_com_LDM">權輿，音餘。權輿，始也。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfeng_com_Mao">
          <head>疏「《權輿》二章，章五句」至「無終」。正義曰：</head>
          <ab>作《權輿》詩者，刺康公也。康公遺忘其先君穆公之舊臣，不加禮餼，與賢者交接，有始而無終，初時殷勤，後則疏薄，故刺之。經二章，皆言禮待賢者有始無終之事。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjQinfeng">
      <lg type="stanza" xml:id="sjQinfengs01">
        <lg type="tercet" xml:id="sjQinfengs01e1">
          <l xml:id="sjQinfengs01l1">於我乎！</l>
          <l xml:id="sjQinfengs01l2">夏屋渠渠，</l>
          <l xml:id="sjQinfengs01l3">今也每食無餘。</l>
        </lg>
        <lg type="couplet" xml:id="sjQinfengs01e2">
          <l xml:id="sjQinfengs01l4">於嗟乎！</l>
          <l xml:id="sjQinfengs01l5">不承權輿！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjQinfengs02">
        <lg type="couplet" xml:id="sjQinfengs02e1">
          <l xml:id="sjQinfengs02l1">於我乎！</l>
          <l xml:id="sjQinfengs02l2">每食四簋，</l>
        </lg>
        <lg type="tercet" xml:id="sjQinfengs02e2">
          <l xml:id="sjQinfengs02l3">今也每食不飽。</l>
          <l xml:id="sjQinfengs02l4">於嗟乎！</l>
          <l xml:id="sjQinfengs02l5">不承權輿！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjQinfengs01">
      <div type="commentary" source="#sjQinfengs01e1">
        <div resp="#Mao" xml:id="sjQinfengs01e1_com_Mao">
          <ab>夏，大也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjQinfengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>屋，具也。渠渠，猶勤勤也。言君始於我，厚設禮食大具以食我，其意勤勤然。</ab>
        </div>
        <div resp="#LDM" xml:id="sjQinfengs01e1_com_LDM">
          <ab>夏，胡雅反。屋如字，具也。食我，音嗣，注篇內同。此言君今遇我薄，其食我才足耳。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjQinfengs01e2">
        <div resp="#Mao" xml:id="sjQinfengs01e2_com_Mao">
          <ab>承，繼也。權輿，始也。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfengs01e1_com_Mao">
          <head>傳「夏，大」。正義曰：</head>
          <ab>《釋詁》文。</ab>
        </div>
        <div source="#sjQinfengs01e1_com_ZX">
          <head>箋「屋具」至「勤勤然」。正義曰：</head>
          <ab>「屋，具」，《釋言》文。渠渠猶勤勤。言設食既具，意又勤勤也。案崔駰《七依》說宮室之美云：「夏屋渠渠。」王肅云：「屋則立之於先君，食則受之於今君，故居大屋而食無餘。」義似可通。鄭不然者，詩刺有始無終。上言「於我乎」，謂始時也。下言「今也」，謂其終時也。始則大具，今終則無餘，猶下章始則四簋，今則不飽，皆說飲食之事，不得言屋宅也。若先君為立大屋，今君每食無餘，則康公本自無始，何責其無終也？且《爾雅》「屋，具」正訓，以此故知謂禮物大具。</ab>
        </div>
        <div source="#sjQinfengs01e2_com_Mao">
          <head>傳「承，繼也。權輿，始」。正義曰：</head>
          <ab>承其後是繼嗣，故以承為繼。「權輿，始」，《釋詁》文。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjQinfengs02">
      <div type="commentary" source="#sjQinfengs02e1">
        <div resp="#Mao" xml:id="sjQinfengs02e1_com_Mao">
          <ab>四簋，黍稷稻粱。</ab>
        </div>
        <div resp="#LDM" xml:id="sjQinfengs02e1_com_LDM">
          <ab>簋音軌，內方外圓曰簋，以盛黍稷。外方內圓曰簠，用貯稻粱。皆容一斗二升。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfengs02e1_com_Mao">
          <head>疏傳「四簋」至「稻粱」。正義曰：</head>
          <ab>《考工記》云：「瓬人為簋，其實一觳。豆實三而成觳。」昭三年《左傳》云：「四升為豆。」然則簋是瓦器，容貐二升也。《易·損卦》：「二簋可用享。」注云：「離為日，日體圓。巽為木，木器圓，簋象。」則簋亦以木為之也。《地官·舍人》注云：「方曰簠。圓曰簋。」則簠、簋之制，其形異也。案《公食大夫禮》云：「宰夫設黍稷六簋。」又云：「宰夫授公粱，公設之。宰夫膳稻於粱西。」注云：「膳猶進也。進稻粱者以簠。」《秋官·掌客》注云：「簠，稻粱器也。簋，黍稷器也。」然則稻粱當在簠，而云「四簋，黍稷稻粱」者，以詩言「每食四簋」，稱君禮物大具，則宜每器一物，不應以黍稷二物分為四簋。以公食大夫禮有稻有粱，知此四簋之內兼有稻粱。公食大夫之禮，是主國之君與聘客禮食，備設器物，故稻粱在簠。此言每食，則是平常燕食，器物不具，故稻粱在簋。公食大夫，黍稷六簋，猶有稻粱。此唯四簋者，亦燕食差於禮食也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《權輿》二章，章五句。</ab>
  </back>
</text>
</TEI>

