<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風召南之野有死麕（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Yeyousijun in the Shaonan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="292" to="293"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=193</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj023" type="preface">
        <ab resp="#Mao" xml:id="sj023_com_Mao">《野有死麕》，惡無禮也。天下大亂，強暴相陵，遂成淫風。被文王之化，雖當亂世，猶惡無禮也。</ab>
        <ab resp="#ZX" xml:id="sj023_com_ZX">無禮者，為不由媒妁，雁幣不至，劫脅以成昏，謂紂之世。</ab>
        <ab resp="#LDM" xml:id="sj023_com_LDM">麕，本亦作「麏」，又作「麇」，俱倫反。麏，獸名也。《草木疏》云：「麏，獐也，青州人謂之麏。」惡，烏路反，下同。被，皮寄反。劫脅，上居業反，下許業反。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj023 #sj023_com_Mao">
          <head>疏「《野有死麕》三章，二章章四句，一章三句」至「惡無禮」。正義曰：</head>
          <ab>作《野有死麕》詩者，言「惡無禮」，謂當紂之世，天下大亂，強暴相陵，遂成淫風之俗。被文王之化，雖當亂世，其貞女猶惡其無禮。經三章皆惡無禮之辭也。</ab>
        </div>
        <div source="#sj023_com_ZX">
          <head>箋「無禮」至「紂之世」。正義曰：</head>
          <ab>經言「吉士誘之」，女思媒氏導之，故知不由媒妁也。思其麕肉為禮，故知雁幣不至也。欲令舒而脫脫兮，故知劫脅以成昏也。箋反經為說，而先媒後幣，與經倒者，便文，見昏禮先媒。經主惡無禮，故先思所持之物也。或有俗本以「天下大亂」以下同為鄭注者，誤。定本、《集注》皆不然。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj023">
      <lg type="stanza" xml:id="sj023s01">
        <lg type="couplet" xml:id="sj023s01e1">
          <l xml:id="sj023s01l1">野有死麕，</l>
          <l xml:id="sj023s01l2">白茅包之。</l>
        </lg>
        <lg type="couplet" xml:id="sj023s01e2">
          <l xml:id="sj023s01l3">有女懷春，</l>
          <l xml:id="sj023s01l4">吉士誘之。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj023s02">
        <lg type="couplet" xml:id="sj023s02e1">
          <l xml:id="sj023s02l1">林有樸樕，</l>
          <l xml:id="sj023s02l2">野有死鹿。</l>
        </lg>
        <lg type="couplet" xml:id="sj023s02e2">
          <l xml:id="sj023s02l3">白茅純束，</l>
          <l xml:id="sj023s02l4">有女如玉。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj023s03">
        <lg type="line" xml:id="sj023s03e1">
          <l xml:id="sj023s03l1">舒而脫脫兮！</l>
        </lg>
        <lg type="line" xml:id="sj023s03e2">
          <l xml:id="sj023s03l2">無感我帨兮，</l>
        </lg>
        <lg type="line" xml:id="sj023s03e3">
          <l xml:id="sj023s03l3">無使尨也吠！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj023s01" type="commentaries">
      <div source="#sj023s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj023s01e1_com_Mao">
          <ab>郊外曰野。包，裹也。凶荒則殺禮，猶有以將之。野有死麕，群田之獲而分其肉。白茅，取絜清也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s01e1_com_ZX">
          <head>箋云：</head>
          <ab>亂世之民貧，而強暴之男多行無禮，故貞女之情，欲令人以白茅裹束野中田者所分麕肉為禮而來。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s01e1_com_LDM">
          <ab>包，逋茅反。裹音果。殺，所戒反，徐所例反。清如字，沈音淨。令，力呈反。</ab>
        </div>
      </div>
      <div source="#sj023s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj023s01e2_com_Mao">
          <ab>懷，思也。春，不暇待秋也。誘，道也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s01e2_com_ZX">
          <head>箋云：</head>
          <ab>有貞女思仲春以禮與男會，吉士使媒人道成之。疾時無禮而言然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s01e2_com_LDM">
          <ab>誘音酉。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj023">
          <head>疏「野有」至「誘之」。</head>
          <ab>毛以為皆惡無禮之辭也。言凶荒則殺禮，猶須禮以將之，故貞女欲男於野田中有死麕之肉，以白茅裹之為禮而來也。既欲其禮，又欲其及時，故有貞女思開春以禮與男會，不欲過時也。又欲令此吉士，先使媒人導成之，不欲無媒妁而自行也。鄭唯「懷春」為異，言思仲春正昏之時，以禮與男會也。餘與毛同。言「春」，據成昏之時。「吉士誘之」，乃於納采之先，在春前矣。但以昏時為重，故先言「懷春」也。此詩所陳，皆是女之所欲，計有女懷春之文，應最在上。但昏禮主於交接，春是合昏之時，故以女懷配春為句，見春是所思之主。其實裹束麕肉亦是女之所思，故箋云貞女之情，欲令以白茅裹束死麕肉為禮而來，是也。</ab>
        </div>
        <div source="#sj023s01e1_com_Mao">
          <head>傳「凶荒」至「絜清」。正義曰：</head>
          <ab>解以死麕之意。昏禮五禮用雁，唯納徵用幣，無麕鹿之肉。言死麕者，凶荒則殺禮，謂減殺其禮，不如豐年也。禮雖殺，猶須有物以將行之，故欲得用麕肉也。此由世亂民貧，故思以麕肉當雁幣也。故《有狐序》曰「古者凶荒，則殺禮多昏」。《司徒》「以荒政十有二聚萬民，十曰多昏」，鄭司農云「多昏，不備禮而昏，娶者多」，是也。傳文解野中所以有死麕者，由群聚於田獵之中，獲而分得其肉。《繢人》注云「齊人謂麕為獐」，則麕是獐也。必以白茅包之者，由取其絜清也。《易》曰：「藉用白茅，無咎。」傳曰「爾貢包茅不入，王祭不供，無以縮酒，以供祭祀」，明其絜清。</ab>
        </div>
        <div source="#sj023s01e2_com_Mao">
          <head>傳「春，不暇待秋」。正義曰：</head>
          <ab>傳以秋冬為正昏，此云春者，此女年二十，期已盡，不暇待秋也。此思春，思開春，欲其以禮來。若仲春，則不待禮會而行之，無為思麕肉矣。此女惡其無禮，恐其過晚，故舉春而言。其實往歲之秋冬，亦可以為昏矣。《釋詁》云：「誘，進也。」《曲禮》注「進客謂導之」，明進、導一也，故以誘為導也。</ab>
        </div>
        <div source="#sj023s01e2_com_ZX">
          <head>箋「有貞」至「言然」。正義曰：</head>
          <ab>箋以仲春為昏時，故知貞女思仲春之月以禮與男會也。言吉士誘之者，女欲令吉士使媒人導達成昏禮也。疾時無媒，故言然也。言「懷春」，自思及時與男會也。言「誘之」，自吉士遣媒也，非謂仲春之月始思遣媒。何者？女十五許嫁，已遣媒以納采，二十仲春始親迎，故知非仲春月始思媒也。吉士者，善士也，述女稱男之意，故以善士言之。「士如歸妻」，「求我庶士」，皆非女所稱，故不言吉。《卷阿》云「用吉士」，謂朝廷之士有善德，故稱吉士也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj023s02" type="commentaries">
      <div source="#sj023s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj023s02e1_com_Mao">
          <ab>樸樕，小木也。野有死鹿，廣物也。純束，猶包之也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s02e1_com_ZX">
          <head>箋云：</head>
          <ab>樸樕之中及野有死鹿，皆可以白茅包裹束以為禮，廣可用之物，非獨麕也。純讀如屯。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s02e1_com_LDM">
          <ab>樸，蒲木反，又音仆。樕音速。純，徒本反，沈云：「鄭徒尊反。」屯，舊徒本反，沈徒尊反，云：「屯，聚也。」</ab>
        </div>
      </div>
      <div source="#sj023s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj023s02e2_com_Mao">
          <ab>德如玉也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s02e2_com_ZX">
          <head>箋云：</head>
          <ab>如玉者，取其堅而絜白。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj023s02">
          <head>疏「林有」至「如玉」。正義曰：</head>
          <ab>言凶荒殺禮，非直麕肉可用，貞女又欲男子於林中有樸樕小木之處，及野之中有群田所分死鹿之肉，以白茅純束而裹之，以為禮而來也。由有貞女，堅而絜白，德如玉然，故惡此無禮，欲有以將之。</ab>
        </div>
        <div source="#sj023s02e1_com_Mao">
          <head>傳「樸樕，小木」。正義曰：</head>
          <ab>《釋木》云：「樸樕，心。」某氏曰：「樸樕，斛樕也，有心能濕，江河間以作柱。」孫炎曰：「樸樕一名心。」是樸樕為木名也。言小木者，以林有此木，故言小木也。「林有樸樕」，謂林中有樸樕之木也，故箋云「樸樕之中及野有死鹿」，不言林者，則林與樸樕為一也。知不別者，以樸樕，木名，若一木，不得有死鹿；若木眾，即是林矣，不得林與樸樕並言也。且下云有死鹿，言有，足得蒙林，林下之有，不為鹿施，明是林中有樸樕之處也。樸樕與林不別，《正月》箋云：「林中大木之處。」此小木得為林者，謂林中有此小木，非小木獨為林也。此宜云「林中小木之處」。</ab>
        </div>
        <div source="#sj023s02e1_com_ZX">
          <head>箋「純讀如屯」。正義曰：</head>
          <ab>「純讀為屯」者，以純非束之義，讀為屯，取肉而裹束之，故傳云「純束，猶包之」。</ab>
        </div>
        <div source="#sj023s02e2_com_ZX">
          <head>箋「如玉」至「絜白」。正義曰：</head>
          <ab>此皆比白玉，故言堅而絜白。《弁師》云「五采玉」，則非一色。獨以白玉比之者，比其堅而絜白，不可汙以無禮。《小戎》箋云「玉有五德」，不云堅而絜白者，以男子百行，不可止貞絜故也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj023s03" type="commentaries">
      <div source="#sj023s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj023s03e1_com_Mao">
          <ab>舒，徐也。脫脫，舒遲也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s03e1_com_ZX">
          <head>箋云：</head>
          <ab>貞女欲吉士以禮來，脫脫然舒也。又疾時無禮，強暴之男相劫脅。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s03e1_com_LDM">
          <ab>脫，敕外反，注同。</ab>
        </div>
      </div>
      <div source="#sj023s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj023s03e2_com_Mao">
          <ab>感，動也。帨，佩巾也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj023s03e2_com_ZX">
          <head>箋云：</head>
          <ab>奔走失節，動其佩飾。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s03e2_com_LDM">
          <ab>感如字，又胡坎反。帨，始銳反，沈始悅反。</ab>
        </div>
      </div>
      <div source="#sj023s03e3" type="commentary">
        <div resp="#Mao" xml:id="sj023s03e3_com_Mao">
          <ab>尨，狗也。非禮相陵則狗吠。</ab>
        </div>
        <div resp="#LDM" xml:id="sj023s03e3_com_LDM">
          <ab>尨，美邦反。吠，符廢反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj023s03">
          <head>疏「舒而」至「也吠」。正義曰：</head>
          <ab>此貞女思以禮來，惡其劫脅。言吉士當以禮而來，其威儀舒遲而脫脫兮，無動我之佩巾兮，又無令狗也吠。但以禮來，我則從之。疾時劫脅成昏，不得安舒，奔走失節，動其佩巾，其使尨也吠，己所以惡之，是謂惡無禮也。</ab>
        </div>
        <div source="#sj023s03e1_com_Mao">
          <head>傳「脫脫，舒遲」。正義曰：</head>
          <ab>脫脫，舒鷃之貌。不言貌者，略之。《采蘩》傳曰「僮僮，竦敬。祁祁，舒遲」，亦略而不言貌。定本「脫脫，舒貌」，有貌字，與俗本異。</ab>
        </div>
        <div source="#sj023s03e2_com_Mao">
          <head>傳「帨，佩巾」。正義曰：</head>
          <ab>《內則》云子事父母，婦事舅姑，皆云「左佩紛帨」。注云：「帨，拭物之巾。」又曰「女子設帨於門右」。然則帨者是巾，為拭物，名之曰帨紛，其自佩之，故曰佩巾。</ab>
        </div>
        <div source="#sj023s03e3_com_Mao">
          <head>傳「尨狗」至「狗吠」。正義曰：</head>
          <ab>「尨，狗」，《釋畜》文。李巡曰：「尨一名狗。」非禮相陵，主不迎客，則有狗吠。此女原其禮來，不用驚狗，故《鄭志》答張逸云「正行昏禮，不得有狗吠」，是也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《野有死麕》三章，二章四句，一章三句。</ab>
  </back>
</text>
</TEI>

