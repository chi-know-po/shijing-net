<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風秦風之無衣（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Wuyi in the Qinfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="373" to="374"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷六</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷六</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80145&amp;amp;page=44</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjQinfeng">
        <ab resp="#Mao" xml:id="sjQinfeng_com_Mao">《無衣》，刺用兵也。秦人刺其君好攻戰，亟用兵，而不與民同欲焉。</ab>
        <ab resp="#LDM" xml:id="sjQinfeng_com_LDM">好，呼報反，下注同。攻，古弄反，又如字，下注同。亟，欺冀反。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfeng_com_Mao">
          <head>疏「《無衣》三章，章五句」至「欲焉」。正義曰：</head>
          <ab>康公以文七年立，十八年卒。案《春秋》文七年，晉人、秦人戰於令狐。十年，秦伯伐晉。十二年，晉人、秦人戰於河曲。十六年，楚人、秦人滅庸。見於經、傳者已如是，是其好攻戰也。《葛生》刺好攻戰，序云「刺獻公」，此亦刺好攻戰，不云刺康公，而云「刺用兵」者，《葛生》以君好戰，故「國人多喪」，指刺獻公，然後追本其事。此指刺用兵，序順經意，故云刺用兵也。不與民同欲，章首二句是也。好攻戰者，下三句是也。經、序倒者，經刺君不與民同欲，與民同怨，故先言不同欲，而後言好攻戰。序本其怨之所由，由好攻戰而不與民同欲，故民怨。各自為次，所以倒也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjQinfeng">
      <lg type="stanza" xml:id="sjQinfengs01">
        <lg type="couplet" xml:id="sjQinfengs01e1">
          <l xml:id="sjQinfengs01l1">豈曰無衣？</l>
          <l xml:id="sjQinfengs01l2">與子同袍。</l>
        </lg>
        <lg type="tercet" xml:id="sjQinfengs01e2">
          <l xml:id="sjQinfengs01l3">王於興師，</l>
          <l xml:id="sjQinfengs01l4">修我戈矛，</l>
          <l xml:id="sjQinfengs01l5">與子同仇！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjQinfengs02">
        <lg type="couplet" xml:id="sjQinfengs02e1">
          <l xml:id="sjQinfengs02l1">豈曰無衣？</l>
          <l xml:id="sjQinfengs02l2">與子同澤。</l>
        </lg>
        <lg type="tercet" xml:id="sjQinfengs02e2">
          <l xml:id="sjQinfengs02l3">王於興師，</l>
          <l xml:id="sjQinfengs02l4">修我矛戟，</l>
          <l xml:id="sjQinfengs02l5">與子偕作！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjQinfengs03">
        <lg type="couplet" xml:id="sjQinfengs03e1">
          <l xml:id="sjQinfengs03l1">豈曰無衣？</l>
          <l xml:id="sjQinfengs03l2">與子同裳。</l>
        </lg>
        <lg type="tercet" xml:id="sjQinfengs03e2">
          <l xml:id="sjQinfengs03l3">王於興師，</l>
          <l xml:id="sjQinfengs03l4">修我甲兵，</l>
          <l xml:id="sjQinfengs03l5">與子偕行！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjQinfengs01">
      <div type="commentary" source="#sjQinfengs01e1">
        <div resp="#Mao" xml:id="sjQinfengs01e1_com_Mao">
          <ab>興也。袍，襺也。上與百姓同欲，則百姓樂致其死。</ab>
        </div>
        <div resp="#ZX" xml:id="sjQinfengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>此責康公之言也。君豈嘗曰：女無衣，我與女共袍乎？言不與民同欲。</ab>
        </div>
        <div resp="#LDM" xml:id="sjQinfengs01e1_com_LDM">
          <ab>袍，抱毛反。襺，古顯反，本亦作「繭」。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjQinfengs01e2">
        <div resp="#Mao" xml:id="sjQinfengs01e2_com_Mao">
          <ab>戈長六尺六寸，矛長二丈。天下有道，則禮樂征伐自天子出。仇，匹也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjQinfengs01e2_com_ZX">
          <head>箋云：</head>
          <ab>於，於也。怨耦曰仇。君不與我同欲，而於王興師，則云：修我戈矛，與子同仇，往伐之。刺其好攻戰。</ab>
        </div>
        <div resp="#LDM" xml:id="sjQinfengs01e2_com_LDM">
          <ab>仇音求。長，直亮反，又如字，下同。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfengs01e1_com_Mao">
          <head>傳「袍襺」至「其死」。正義曰：</head>
          <ab>「袍，襺」，《釋言》文。《玉藻》云：「纊為襺。縕為袍。」注云：「衣有著之異名也。縕謂今纊及舊絮也。」然則純著新綿名為襺，雜用舊絮名為袍。雖著有異名，其制度是一，故云「袍，襺也」。傳既以此為興，又言「上與百姓同欲，則百姓樂致其死」，則此經所言朋友相與同袍，以興上與百姓同欲，故王肅云：「豈謂子無衣乎？樂有是袍，與子為朋友，同共弊之。以興上與百姓同欲，則百姓樂致其死，如朋友樂同衣袍也。」</ab>
        </div>
        <div source="#sjQinfengs01e1_com_ZX">
          <head>箋「此責」至「同欲」。正義曰：</head>
          <ab>易傳者，以此刺康公不與民同欲。而經言子、我，是述康公之意，謂民自稱為我。然則士卒眾矣，人君不可皆與同衣。而責君不與己共袍者，以仁者在上，恤民饑寒，知其有無，救其困乏，故假同袍以為辭耳，非百姓皆欲望君與之共袍也。</ab>
        </div>
        <div source="#sjQinfengs01e2_com_Mao">
          <head>傳「戈長」至「仇匹」。正義曰：</head>
          <ab>「戈長六尺六寸」，《考工記·廬人》文也。《記》又云：「酋矛常有四尺。」注云：「八尺曰尋。倍尋曰常。常有四尺。」是矛長二丈也。矛長二丈，謂酋矛也。夷矛則三尋，長二丈四尺矣。《記》又云：「攻國之兵用短，守國之兵用長。」此言興師以伐人國，知用二丈之矛，非夷矛也。又解稱王於興師之意。天下有道，禮樂征伐自天子出，諸侯不得專輒用兵。疾君不由王命，自好攻戰，故言王也。王肅云：「疾其好攻戰，不由王命，故思王興師是也。」「仇，匹」，《釋詁》文。</ab>
        </div>
        <div source="#sjQinfengs01e2_com_ZX">
          <head>箋「於於」至「攻戰」。正義曰：</head>
          <ab>「於，於」，《釋詁》文。「怨耦曰仇」，桓二年《左傳》文。易傳者，以上二句假為康公之言，則此亦康公之言，陳其號令之辭。刺其好攻戰也。案此時當周頃王、匡王，天子之命不行於諸侯。檢《左傳》，於時天子未嘗出師，又不見康公從王征伐。且從王出征，乃是為臣之義，而刺其好攻戰者，箋言「王於興師」，謂於王法興師。今是康公自興之，王不興師也。以出師征伐是王者之法，故以王為言耳。猶《北門》言「王事敦我」，《鴇羽》云「王事靡盬」，皆非天子之事，亦稱王事。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjQinfengs02">
      <div type="commentary" source="#sjQinfengs02e1">
        <div resp="#Mao" xml:id="sjQinfengs02e1_com_Mao">
          <ab>澤，潤澤也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjQinfengs02e1_com_ZX">
          <head>箋云：</head>
          <ab>澤，褻衣，近汙垢。</ab>
        </div>
        <div resp="#LDM" xml:id="sjQinfengs02e1_com_LDM">
          <ab>澤如字，《說文》作「襗」，云：「袴也。」褻，仙列反。近，附近之近。汙音烏，又汙穢之汙。垢，古口反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjQinfengs02e2">
        <div resp="#Mao" xml:id="sjQinfengs02e2_com_Mao">
          <ab>作，起也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjQinfengs02e2_com_ZX">
          <head>箋云：</head>
          <ab>戟，車戟常也。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjQinfengs02e1_com_Mao">
          <head>疏傳「澤，潤澤」。正義曰：</head>
          <ab>衣服之暖於身，猶甘雨之潤於物，故言與子同澤，正謂同袍、裳是共潤澤也。箋以上袍下裳，則此亦衣名，故易傳為「襗」。《說文》云：「襗，袴也。」是其褻衣近汙垢也。襗是袍類，故《論語》注云：「褻衣，袍襗也。」</ab>
        </div>
        <div source="#sjQinfengs02e2_com_ZX">
          <head>疏箋「戟，車戟常」。正義曰：</head>
          <ab>「車戟常」，《考工記·廬人》文。常長丈六。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjQinfengs03">
      <div type="commentary" source="#sjQinfengs03e2">
        <div resp="#Mao" xml:id="sjQinfengs03e2_com_Mao">
          <ab>行，往也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《無衣》三章，章五句。</ab>
  </back>
</text>
</TEI>

