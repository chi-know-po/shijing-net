<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風王風之采葛（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Caige in the Wangfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="333" to="333"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=30</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj072" type="preface">
        <ab resp="#Mao" xml:id="sj072_com_Mao">《采葛》，懼讒也。</ab>
        <ab resp="#ZX" xml:id="sj072_com_ZX">桓王之時，政事不明，臣無大小使出者，則為讒人所毀，故懼之。</ab>
        <ab resp="#LDM" xml:id="sj072_com_LDM">使，所吏反，下並同。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj072+_com_poem">
          <head>疏「《采葛》三章，章三句」至「讒也」。正義曰：</head>
          <ab>三章如此次者，既以葛、蕭、艾為喻，因以月、秋、歲為韻。積日成月，積月成時，積時成歲，欲先少而後多，故以月、秋、歲為次也。臣之懼讒於小事大事，其憂等耳，未必小事之憂則如月，急事之憂則如歲。設文各從其韻，不由事大憂深也。年有四時，時皆三月，三秋謂九月也。設言三春三夏，其義亦同，作者取其韻耳。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj072">
      <lg type="stanza" xml:id="sj072s01">
        <lg type="tercet" xml:id="sj072s01e1">
          <l xml:id="sj072s01l1">彼采葛兮，</l>
          <l xml:id="sj072s01l2">一日不見，</l>
          <l xml:id="sj072s01l3">如三月兮。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj072s02">
        <lg type="tercet" xml:id="sj072s02e1">
          <l xml:id="sj072s02l1">彼采蕭兮，</l>
          <l xml:id="sj072s02l2">一日不見，</l>
          <l xml:id="sj072s02l3">如三秋兮。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj072s03">
        <lg type="tercet" xml:id="sj072s03e1">
          <l xml:id="sj072s03l1">彼采艾兮，</l>
          <l xml:id="sj072s03l2">一日不見，</l>
          <l xml:id="sj072s03l3">如三歲兮。</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj072s01" type="commentaries">
      <div source="#sj072s01" type="commentary">
        <div resp="#Mao" xml:id="sj072s01_com_Mao">
          <ab>興也。葛所以為絺綌也。事雖小，一日不見於君，憂懼於讒矣。</ab>
        </div>
        <div resp="#ZX" xml:id="sj072s01_com_ZX">
          <head>箋云：</head>
          <ab>興者，以采葛喻臣以小事使出。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj072s01">
          <head>疏「彼采」至「月兮」。正義曰：</head>
          <ab>彼采葛草以為絺綌兮，以興臣有使出而為小事兮。其事雖小，憂懼於讒，一日不得見君，如三月不見君兮，日久情疏，為懼益甚，故以多時況少時也。</ab>
        </div>
        <div source="#sj072s01_com_Mao">
          <head>傳「葛所」至「讒矣」。正義曰：</head>
          <ab>言所以為絺綌者，以其所采，疑作當暑之服，比於祭祀療疾乃緩而且小，故以喻小事使出也。大事容或多過，小事當無愆咎，但桓王信讒之故，其事唯小，一日不見於君，已憂懼於讒矣。</ab>
        </div>
      </div>
    </div>
    <div source="#sj072s02" type="commentaries">
      <div source="#sj072s02" type="commentary">
        <div resp="#Mao" xml:id="sj072s02_com_Mao">
          <ab>蕭所以共祭祀。</ab>
        </div>
        <div resp="#ZX" xml:id="sj072s02_com_ZX">
          <head>箋云：</head>
          <ab>彼采蕭者，喻臣以大事使出。</ab>
        </div>
        <div resp="#LDM" xml:id="sj072s02_com_LDM">
          <ab>共音恭。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj072s02_com_Mao">
          <head>疏傳「蕭所以共祭祀」。正義曰：</head>
          <ab>《釋草》云：「蕭，荻。」李巡曰：「荻，一名蕭。」陸機云：「今人所謂荻蒿者是也。或云牛尾蒿，似白蒿，白葉莖粗，科生多者數十莖，可作燭，有香氣，故祭祀以脂爇之為香。許慎以為艾蒿，非也。」《郊特牲》云：「既奠，然後爇蕭合馨香。」《生民》云：「取蕭祭脂。」 是蕭所以供祭祀也。成十三年《左傳》曰「國之大事，在祀與戎」，故以祭祀所須者喻大事使出。</ab>
        </div>
      </div>
    </div>
    <div source="#sj072s03" type="commentaries">
      <div source="#sj072s03" type="commentary">
        <div resp="#Mao" xml:id="sj072s03_com_Mao">
          <ab>艾所以療疾。</ab>
        </div>
        <div resp="#ZX" xml:id="sj072s03_com_ZX">
          <head>箋云：</head>
          <ab>彼采艾者，喻臣以急事使出。</ab>
        </div>
        <div resp="#LDM" xml:id="sj072s03_com_LDM">
          <ab>艾，五蓋反。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《采葛》三章，章三句。</ab>
  </back>
</text>
</TEI>

