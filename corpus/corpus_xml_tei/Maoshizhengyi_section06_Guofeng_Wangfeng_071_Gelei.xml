<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風王風之葛藟（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Gelei in the Wangfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="332" to="333"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=26</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj071" type="preface">
        <ab resp="#Mao" xml:id="sj071_com_Mao">《葛藟》，王族刺平王也。周室道衰，棄其九族焉。</ab>
        <ab resp="#ZX" xml:id="sj071_com_ZX">九族者，據己上至高祖，下及玄孫之親。</ab>
        <ab resp="#LDM" xml:id="sj071_com_LDM">藟，力軌反。藟似葛。《廣雅》云：「藟，藤也。」「刺桓王」，本亦作「刺平王」。按《詩譜》是平王詩，皇甫士安以為桓王之詩，崔《集注》本亦作桓王。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj071+_com_poem">
          <head>疏「《葛藟》三章，章六句」至「族焉」。正義曰：</head>
          <ab>棄其九族者，不復以族食族燕之禮敘而親睦之，故王之族人作此詩以刺王也。此敘其刺王之由，經皆陳族人怨王之辭。定本云「刺桓王」，義雖通，不合鄭《譜》。</ab>
        </div>
        <div source="#sj071_com_ZX">
          <head>箋「九族」至「之親」。正義曰：</head>
          <ab>此《古尚書》說，鄭取用之。《異義》，「九族，今《戴禮》、《尚書》歐陽說云：』九族，乃異姓有親屬者。父族四：五屬之內為一族，父女昆弟適人者與其子為一族，己女昆弟適人者與其子為一族，己之子適人者與其子為一族。母族三：母之父姓為一族，母之母姓為一族，母女昆弟適人者為一族。妻族二：妻之父姓為一族，妻之母姓為一族。』《古尚書》說：『九族者，上從高祖，下至玄孫，凡九，皆為同姓。』謹案：『《禮》，緦麻三月以上，恩之所及。《禮》，為妻父母有服。明在九族，不得但施於同姓。』」玄之聞也，婦人婦宗，女子雖適人，字猶係姓，明不與父兄為異族，其子則然。《昏禮》請期辭曰：「惟是三族之不虞。」欲及今三族未有不億度之事而迎婦也。如此所云，則三族當有異姓。異姓其服皆緦麻，緦麻之服，不禁嫁女聚妻，是為異姓不在族中明矣。《周禮》：「小宗伯掌三族之別。」《喪服小記》說族之義曰：「親親以三為五，以五為九。」以此言之，知高祖至玄孫，昭然察矣。是鄭以古說長，宜從之事也。《古尚書》說直云高祖至玄孫，凡九，不言「之親」。此言「之親」，欲見同出高祖者當皆親之。此言「棄其九族」，正謂棄其同出高祖者，非棄高祖之身。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj071">
      <lg type="stanza" xml:id="sj071s01">
        <lg type="couplet" xml:id="sj071s01e1">
          <l xml:id="sj071s01l1">綿綿葛藟，</l>
          <l xml:id="sj071s01l2">在河之滸。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s01e2">
          <l xml:id="sj071s01l3">終遠兄弟，</l>
          <l xml:id="sj071s01l4">謂他人父。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s01e3">
          <l xml:id="sj071s01l5">謂他人父，</l>
          <l xml:id="sj071s01l6">亦莫我顧！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj071s02">
        <lg type="couplet" xml:id="sj071s02e1">
          <l xml:id="sj071s02l1">綿綿葛藟，</l>
          <l xml:id="sj071s02l2">在河之涘。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s02e2">
          <l xml:id="sj071s02l3">終遠兄弟，</l>
          <l xml:id="sj071s02l4">謂他人母。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s02e3">
          <l xml:id="sj071s02l5">謂他人母，</l>
          <l xml:id="sj071s02l6">亦莫我有！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj071s03">
        <lg type="couplet" xml:id="sj071s03e1">
          <l xml:id="sj071s03l1">綿綿葛藟，</l>
          <l xml:id="sj071s03l2">在河之漘。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s03e2">
          <l xml:id="sj071s03l3">終遠兄弟，</l>
          <l xml:id="sj071s03l4">謂他人昆。</l>
        </lg>
        <lg type="couplet" xml:id="sj071s03e3">
          <l xml:id="sj071s03l5">謂他人昆，</l>
          <l xml:id="sj071s03l6">亦莫我聞！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj071s01" type="commentaries">
      <div source="#sj071s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj071s01e1_com_Mao">
          <ab>興也。綿綿，長不絕之貌。水厓曰滸。</ab>
        </div>
        <div resp="#ZX" xml:id="sj071s01e1_com_ZX">
          <head>箋云：</head>
          <ab>葛也藟也，生於河之厓，得其潤澤，以長大而不絕。興者，喻王之同姓，得王之恩施，以生長其子孫。</ab>
        </div>
        <div resp="#LDM" xml:id="sj071s01e1_com_LDM">
          <ab>滸，呼五反。長不，張丈反，下同。涯，本亦作「厓」，魚佳反。施，始豉反，下同。</ab>
        </div>
      </div>
      <div source="#sj071s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj071s01e2_com_Mao">
          <ab>兄弟之道已相遠矣。</ab>
        </div>
        <div resp="#ZX" xml:id="sj071s01e2_com_ZX">
          <head>箋云：</head>
          <ab>兄弟，猶言族親也。王寡於恩施，今巳遠棄族親矣，是我謂他人為己父。族人尚親親之辭。</ab>
        </div>
        <div resp="#LDM" xml:id="sj071s01e2_com_LDM">
          <ab>遠，於萬反，又如字，注下皆同。</ab>
        </div>
      </div>
      <div source="#sj071s01e3" type="commentary">
        <div resp="#ZX" xml:id="sj071s01e3_com_ZX">
          <head>箋云：</head>
          <ab>謂他人為己父，無恩於我，亦無顧眷我之意。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj071s01">
          <head>疏「綿綿」至「我顧」。正義曰：</head>
          <ab>綿綿然枝葉長而不絕者，乃是葛藟之草，所以得然者，由其在河之滸，得河之潤故也。以興子孫長而昌盛者，乃是王族之人。所以得然者，由其與王同姓，得王之恩故也。王族宜得王之恩施，猶葛藟宜得河之潤澤，王何故棄遺我宗族之人乎？王終是遠於兄弟，無復恩施於我，是我謂他人為己父也。謂他人為己父，則無恩於我，亦無肯於我有顧戀之意。言王無恩於己，與他人為父同，責王無父之恩也。</ab>
        </div>
        <div source="#sj071s01e1_com_Mao">
          <head>傳「水厓曰滸」。正義曰：</head>
          <ab>《釋水》云：「滸，水厓。」李巡曰：「滸，水邊地，名厓也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj071s02" type="commentaries">
      <div source="#sj071s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj071s02e1_com_Mao">
          <ab>涘，厓也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj071s02e1_com_LDM">
          <ab>涘音俟，涯也。</ab>
        </div>
      </div>
      <div source="#sj071s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj071s02e2_com_Mao">
          <ab>王又無母恩。</ab>
        </div>
      </div>
      <div source="#sj071s02e3" type="commentary">
        <div resp="#ZX" xml:id="sj071s02e3_com_ZX">
          <head>箋云：</head>
          <ab>有，識有也。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj071s02e1_com_Mao">
          <head>疏傳「涘，厓」。正義曰：</head>
          <ab>《釋丘》云：「涘為厓。」李巡曰：「涘一名厓。」郭璞曰：「謂水邊也。」</ab>
        </div>
        <div source="#sj071s02+_com_ZX">
          <head>箋「王又無母恩」。正義曰：</head>
          <ab>又者，亞前之辭。上言謂他人父，責王無父恩也。此言謂他人母，責王又無母恩也。然則下章謂他人昆，責王無兄恩也。定本及諸本「又」作「后」，義亦通。</ab>
        </div>
      </div>
    </div>
    <div source="#sj071s03" type="commentaries">
      <div source="#sj071s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj071s03e1_com_Mao">
          <ab>漘，水溓也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj071s03e1_com_LDM">
          <ab>漘，順春反。《爾雅》云：「夷上灑下水漘。」旁從水。郭云：「涯上平坦而下水深為漘。不，發聲也。」隒，魚檢反，何音檢。《爾雅》云：「重甗，隒。」郭云：「形似累兩重甑，上大下小。」李巡云：「隒，阪也。」詩本又作水旁兼者，字書音呂恬、理染二反，《廣雅》云「溓，清也」，與此義乖。</ab>
        </div>
      </div>
      <div source="#sj071s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj071s03e2_com_Mao">
          <ab>昆，兄也。</ab>
        </div>
      </div>
      <div source="#sj071s03e3" type="commentary">
        <div resp="#ZX" xml:id="sj071s03e3_com_ZX">
          <head>箋云：</head>
          <ab>不與我相聞命也。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj071s03e1_com_Mao">
          <head>疏傳「漘，水隒」。正義曰：</head>
          <ab>《釋丘》云：「夷上灑下不漘。」李巡曰：「夷上，平上；灑下，峭下，故名漘。」孫炎曰：「平上峭下故名曰漘。不者，蓋衍字。」郭璞曰：「厓上平坦而下水深者為漘。不，發聲也。」此在河之漘，即彼漘也。《釋山》云：「重甗，隒。」孫炎曰：「山基有重岸也。」隒是山岸，漘是水岸，故云「水隒」。</ab>
        </div>
        <div source="#sj071s03e2_com_Mao">
          <head>傳「昆，兄」。正義曰：</head>
          <ab>《釋親》文。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《葛藟》三章，章六句。</ab>
  </back>
</text>
</TEI>

