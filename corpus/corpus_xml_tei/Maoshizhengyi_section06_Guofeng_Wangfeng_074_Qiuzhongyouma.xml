<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風王風之丘中有麻（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Qiuzhongyouma in the Wangfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="333" to="334"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=39</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj074" type="preface">
        <ab resp="#Mao" xml:id="sj074_com_Mao">《丘中有麻》，思賢也。莊王不明，賢人放逐，國人思之，而作是詩也。</ab>
        <ab resp="#ZX" xml:id="sj074_com_ZX">思之者，思其來，已得見之。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj074+_com_poem">
          <head>疏「《丘中有麻》三章，章四句」至「是詩」。正義曰：</head>
          <ab>毛以為，放逐者，本在位有功，今去，而思之。鄭以為，去治賤事，所在有功，故思之。意雖小異，三章俱是思賢之事。</ab>
        </div>
        <div source="#sj074_com_ZX">
          <head>箋「思之」至「見之」。正義曰：</head>
          <ab>箋以為「施施」為見已之貌，「來食」謂已得食之，故以「思之」為「思其來，己得見之」。毛以 「來食」為「子國復來，我乃得食」，則思其更來在朝，非徒思見而已，其意與鄭小異。子國是子嗟之父，俱是賢人，不應同時見逐。若同時見逐，當先思子國，不應先思其子。今首章先言子嗟，二章乃言子國，然則賢人放逐，止謂子嗟耳。但作者既思子嗟，又美其弈世有德，遂言及子國耳。故首章傳曰「麻、麥、草、木，乃彼子嗟之所治」，是言麥亦子嗟所治，非子國之功也。二章箋言「子國使丘中有麥，著其世賢」，言著其世賢，則是引父以顯子，其意非思子國也。卒章言「彼留之子」，亦謂子嗟耳。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj074">
      <lg type="stanza" xml:id="sj074s01">
        <lg type="couplet" xml:id="sj074s01e1">
          <l xml:id="sj074s01l1">丘中有麻，</l>
          <l xml:id="sj074s01l2">彼留子嗟。</l>
        </lg>
        <lg type="couplet" xml:id="sj074s01e2">
          <l xml:id="sj074s01l3">彼留子嗟，</l>
          <l xml:id="sj074s01l4">將其來施施。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj074s02">
        <lg type="couplet" xml:id="sj074s02e1">
          <l xml:id="sj074s02l1">丘中有麥，</l>
          <l xml:id="sj074s02l2">彼留子國。</l>
        </lg>
        <lg type="couplet" xml:id="sj074s02e2">
          <l xml:id="sj074s02l3">彼留子國，</l>
          <l xml:id="sj074s02l4">將其來食。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj074s03">
        <lg type="couplet" xml:id="sj074s03e1">
          <l xml:id="sj074s03l1">丘中有李，</l>
          <l xml:id="sj074s03l2">彼留之子。</l>
        </lg>
        <lg type="couplet" xml:id="sj074s03e2">
          <l xml:id="sj074s03l3">彼留之子，</l>
          <l xml:id="sj074s03l4">貽我佩玖。</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj074s01" type="commentaries">
      <div source="#sj074s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj074s01e1_com_Mao">
          <ab>留，大夫氏。子嗟，字也。丘中墝埆之處，盡有麻、麥、草、木，乃彼子嗟之所治。</ab>
        </div>
        <div resp="#ZX" xml:id="sj074s01e1_com_ZX">
          <head>箋云：</head>
          <ab>子嗟放逐於朝，去治卑賤之職而有功，所在則治理，所以為賢。</ab>
        </div>
        <div resp="#LDM" xml:id="sj074s01e1_com_LDM">
          <ab>墝，本亦作「墽」，苦交反。埆，苦角反，又音學。本或作「遠」，此從孫義而誤耳。</ab>
        </div>
      </div>
      <div source="#sj074s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj074s01e2_com_Mao">
          <ab>施施，難進之意。</ab>
        </div>
        <div resp="#ZX" xml:id="sj074s01e2_com_ZX">
          <head>箋云：</head>
          <ab>施施，舒行，伺閒獨來見己之貌。</ab>
        </div>
        <div resp="#LDM" xml:id="sj074s01e2_com_LDM">
          <ab>將，王申毛如字，鄭七良反，下同。施，如字。伺音司。閒音閑，又如字。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj074s01">
          <head>疏「丘中」至「來施施」。</head>
          <ab>毛以為，子嗟在朝有功，今而放逐在外，國人睹其業而思之。言丘中墝埆之處，所以得有麻者，乃留氏子嗟之所治也，由子嗟教民農業，使得有之。今放逐於外，國人思之，乃遙述其行。彼留氏之子嗟，其將來之時，施施然甚難進而易退，其肯來乎？言不肯復來，所以思之特甚。鄭以為，子嗟放逐於朝，去治卑賤之職。言丘中墝埆之處，今日所以有麻者，彼留氏之子嗟往治之耳，故云「所在則治理」，信是賢人。國人之意，原得彼留氏之子嗟。其將欲來，舒行施施然，伺候閒暇，獨來見己。閔其放逐，愛其德義，冀來見己，與之盡歡。</ab>
        </div>
        <div source="#sj074s01e1_com_Mao">
          <head>傳「留大」至「所治」。正義曰：</head>
          <ab>賢人放逐，明為大夫而去。下云 「彼留之子」與易稱「顏氏之子」，其文相類，故知劉氏，大夫氏也。子者，有德之稱，古人以子為字，與嗟連文，故知字也。《釋丘》云：「非人力為之丘。」丘是地之高者，在丘之中，故云墝埆之處。墝埆，謂地之瘠薄者也。傳探下章而解之，故言麻、麥、草、木也。木即下章李也，兼言草以足句，乃彼子嗟之所治。謂子嗟未去之日，教民治之也。定本云「丘中墝埆，遠盡有麻、麥、草、木」，與俗本不同也。</ab>
        </div>
        <div source="#sj074s01e1_com_ZX">
          <head>箋「子嗟」至「為賢」。正義曰：</head>
          <ab>箋以「有麻」之下即云「彼留子嗟」，則是子嗟今日所居有麻麥也。且丘中是隱遁之處，故易傳以為「去治卑賤之職而有功」。《孝經》云：「居家理，故治可移於官。」子嗟在朝則能助教行政，隱遁則能使墝埆生物，所在則治理，是其所以為賢也。</ab>
        </div>
        <div source="#sj074s01e2_com_Mao">
          <head>傳「施施，難進之意」。正義曰：</head>
          <ab>傳亦以施施為舒行，由賢者難進，故來則舒行，言其本性為然，恐將不復更來，故思之也。</ab>
        </div>
        <div source="#sj074s01e2_com_ZX">
          <head>箋「施施」至「之貌」。正義曰：</head>
          <ab>箋以思之欲使更來，不宜言其難進。且言其「將」者，是冀其復來，故易傳以為「伺候閒暇，獨來見己之貌」。此章欲其獨來見己，下章冀得設食以待之，亦事之次也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj074s02" type="commentaries">
      <div source="#sj074s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj074s02e1_com_Mao">
          <ab>子國，子嗟父。</ab>
        </div>
        <div resp="#ZX" xml:id="sj074s02e1_com_ZX">
          <head>箋云：</head>
          <ab>言子國使丘中有麥，著其世賢。</ab>
        </div>
      </div>
      <div source="#sj074s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj074s02e2_com_Mao">
          <ab>子國復來，我乃得食。</ab>
        </div>
        <div resp="#ZX" xml:id="sj074s02e2_com_ZX">
          <head>箋云：</head>
          <ab>言其將來食，庶其親己，己得厚待之。</ab>
        </div>
        <div resp="#LDM" xml:id="sj074s02e2_com_LDM">
          <ab>食如字，一云鄭音嗣。復，扶又反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj074s02e1_com_Mao">
          <head>疏傳「子國，子嗟父」。正義曰：</head>
          <ab>毛時書籍猶多，或有所據，未詳毛氏何以知之。</ab>
        </div>
        <div source="#sj074s02e1_com_ZX">
          <head>箋「言子」至「世賢」。正義曰：</head>
          <ab>箋以丘中有麻，是子嗟去往治之，而此章言子國亦能使丘中有麥，是顯著其世賢。言其父亦是治理之人耳，非子國實使丘中有麥也。</ab>
        </div>
        <div source="#sj074s02e2_com_Mao">
          <head>傳「子國」至「得食」。正義曰：</head>
          <ab>傳言以子國教民稼穡，能使年歲豐穰，及其放逐，下民思之，乏於飲食，故言子國其將來，我乃得有食耳。</ab>
        </div>
        <div source="#sj074s02e2_com_ZX">
          <head>箋「言其」至「待之」。正義曰：</head>
          <ab>準上章思者欲令子國見己，言其獨來，就我飲食，庶其親己。來至己家，己得厚禮以待之。思賢之至，欲飲食之也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj074s03" type="commentaries">
      <div source="#sj074s03e1" type="commentary">
        <div resp="#ZX" xml:id="sj074s03e1_com_ZX">
          <head>箋云：</head>
          <ab>丘中而有李，又留氏之子所治。</ab>
        </div>
      </div>
      <div source="#sj074s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj074s03e2_com_Mao">
          <ab>玖，石次玉者。言能遺我美寶。</ab>
        </div>
        <div resp="#ZX" xml:id="sj074s03e2_com_ZX">
          <head>箋云：</head>
          <ab>留氏之子，於思者則朋友之子，庶其敬己而遺己也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj074s03e2_com_LDM">
          <ab>貽音怡。玖音久，《說文》紀又反，云「石之次玉，黑色者」。遺，唯季反，下同。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj074s03e2_com_Mao">
          <head>疏傳「玖石」至「美寶」。正義曰：</head>
          <ab>玖是佩玉之名，故以美寶言之。美寶猶美道。傳言以為作者思而不能見，乃陳其昔日之功，言彼留氏之子，有能遺我以美道，謂在朝所施之政教。</ab>
        </div>
        <div source="#sj074s03e2_com_ZX">
          <head>箋「留氏」至「遺已」。正義曰：</head>
          <ab>箋亦以佩玖喻美道，所異者，正謂今日冀望其來，敬己而遺已耳，非是昔日所遺。上章欲其見己，己得食之，言己之待留氏。此章留氏之子遺我以美道，欲留氏之子教己，是思者與留氏情親，故云「留氏之子，於思者則朋友之子」，正謂朋友之身，非與其父為朋友。孔子謂子路「賊夫人之子」，亦此類也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《丘中有麻》三章，章四句。</ab>
    <ab>王國十篇，二十八章，百六十二句。</ab>
  </back>
</text>
</TEI>

