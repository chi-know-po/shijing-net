<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風唐風之鴇羽（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Baoyu in the Tangfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="365" to="365"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷六</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷六</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80144&amp;amp;page=131</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjTangfeng">
        <ab resp="#Mao" xml:id="sjTangfeng_com_Mao">《鴇羽》，刺時也。昭公之後，大亂五世，君子下從征役，不得養其父母，而作是詩也。</ab>
        <ab resp="#ZX" xml:id="sjTangfeng_com_ZX">大亂五世者，昭公、孝侯、鄂侯、哀侯、小子侯。</ab>
        <ab resp="#LDM" xml:id="sjTangfeng_com_LDM">鴇音保，似雁而大，無後指。政役，音征，篇內注同。養，羊亮反。鄂，五各反。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjTangfeng_com_Mao">
          <head>疏「《鴇羽》二章，章七句」至「是詩」。正義曰：</head>
          <ab>言下從征役者，君子之人當居平安之處，不有征役之勞。今乃退與無知之人共從征役，故言下也。定本作「下從征役」。經三章，皆上二句言君子從征役之苦，下五句恨不得供養父母之辭。</ab>
        </div>
        <div source="#sjTangfeng_com_ZX">
          <head>箋「大亂」至「子侯」。正義曰：</head>
          <ab>案《左傳》桓二年稱「魯惠公三十年，晉潘父弑昭侯而納桓叔，不克。晉人立孝侯。惠之四十五年，曲沃莊伯伐翼，弑孝侯。翼人立其弟鄂侯。」隱五年傳稱「曲沃莊伯伐翼，翼侯奔隨。秋，王命虢公伐曲沃，而立哀侯於翼」。隱六年傳稱「翼人逆晉侯於隨，納諸鄂，晉人謂之鄂侯」。桓二年傳「鄂侯生哀侯。哀侯侵陘庭之田。陘庭南鄙啟曲沃伐翼」。桓三年，「曲沃武公伐翼，逐翼侯於汾隰，夜獲之」。桓七年傳「冬，曲沃伯誘晉小子侯殺之」。「八年春，滅翼」。是大亂五世之事。案桓八年傳云：「冬，王命虢仲立晉哀侯之弟緡於晉。」則小子侯之後，復有緡為晉君。此大亂五世，不數緡者，以此言昭公之後，則是昭公之詩，自昭公數之，至小子而滿五，故數不及緡也。此言大亂五世，則亂後始作，但亂從昭起，追刺昭公，故為昭公詩也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjTangfeng">
      <lg type="stanza" xml:id="sjTangfengs01">
        <lg type="couplet" xml:id="sjTangfengs01e1">
          <l xml:id="sjTangfengs01l1">肅肅鴇羽，</l>
          <l xml:id="sjTangfengs01l2">集於苞栩。</l>
        </lg>
        <lg type="tercet" xml:id="sjTangfengs01e2">
          <l xml:id="sjTangfengs01l3">王事靡盬，</l>
          <l xml:id="sjTangfengs01l4">不能蓺稷黍，</l>
          <l xml:id="sjTangfengs01l5">父母何怙？</l>
        </lg>
        <lg type="couplet" xml:id="sjTangfengs01e3">
          <l xml:id="sjTangfengs01l6">悠悠蒼天！</l>
          <l xml:id="sjTangfengs01l7">曷其有所？</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjTangfengs02">
        <lg type="couplet" xml:id="sjTangfengs02e1">
          <l xml:id="sjTangfengs02l1">肅肅鴇翼，</l>
          <l xml:id="sjTangfengs02l2">集於苞棘。</l>
        </lg>
        <lg type="tercet" xml:id="sjTangfengs02e2">
          <l xml:id="sjTangfengs02l3">王事靡盬，</l>
          <l xml:id="sjTangfengs02l4">不能蓺黍稷，</l>
          <l xml:id="sjTangfengs02l5">父母何食？</l>
        </lg>
        <lg type="couplet" xml:id="sjTangfengs02e3">
          <l xml:id="sjTangfengs02l6">悠悠蒼天！</l>
          <l xml:id="sjTangfengs02l7">曷其有極？</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjTangfengs03">
        <lg type="couplet" xml:id="sjTangfengs03e1">
          <l xml:id="sjTangfengs03l1">肅肅鴇行，</l>
          <l xml:id="sjTangfengs03l2">集於苞桑。</l>
        </lg>
        <lg type="tercet" xml:id="sjTangfengs03e2">
          <l xml:id="sjTangfengs03l3">王事靡盬，</l>
          <l xml:id="sjTangfengs03l4">不能蓺稻粱，</l>
          <l xml:id="sjTangfengs03l5">父母何嘗？</l>
        </lg>
        <lg type="couplet" xml:id="sjTangfengs03e3">
          <l xml:id="sjTangfengs03l6">悠悠蒼天！</l>
          <l xml:id="sjTangfengs03l7">曷其有常？</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjTangfengs01">
      <div type="commentary" source="#sjTangfengs01e1">
        <div resp="#Mao" xml:id="sjTangfengs01e1_com_Mao">
          <ab>興也。肅肅，鴇羽聲也。集，止。苞，稹。栩，杼也。鴇之性不樹止。</ab>
        </div>
        <div resp="#ZX" xml:id="sjTangfengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>興者，喻君子當居安平之處，今下從征役，其為危苦，如鴇之樹止然。稹者，根相迫迮梱致也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjTangfengs01e1_com_LDM">
          <ab>苞，補交反。栩，況羽反。稹本又作「縝」，之忍反，何之人反，沈音田，又音振，《廣雅》云：「概也。」杼，食汝反，徐治與反。處，昌慮反。迮，側百反。梱，口本反。致，直置反，下同。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjTangfengs01e2">
        <div resp="#Mao" xml:id="sjTangfengs01e2_com_Mao">
          <ab>盬，不攻緻也。怙，恃也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjTangfengs01e2_com_ZX">
          <head>箋云：</head>
          <ab>蓺，樹也。我迫王事，無不攻致，故盡力焉。既則罷倦，不能播種五穀，今我父母將何怙乎？</ab>
        </div>
        <div resp="#LDM" xml:id="sjTangfengs01e2_com_LDM">
          <ab>盬音古。蓺，魚世反。怙音戶。罷音皮。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjTangfengs01e3">
        <div resp="#ZX" xml:id="sjTangfengs01e3_com_ZX">
          <head>箋云：</head>
          <ab>曷，何也。何時我得其所哉？</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjTangfengs01">
          <head>疏「肅肅」至「有所」。正義曰：</head>
          <ab>言肅肅之為聲者，是鴇鳥之羽飛而集於苞栩之上，以興君子之人，乃下從於征役之事。然鴇之性不樹止，今乃集於苞栩之上，極為危苦，喻君子之人當居平安之處，今乃下從征役，亦甚為危苦。君子之人既從王事，此王家之事無不攻緻，故盡力為之。既則罷倦，雖得還家，不復能種蓺黍稷。既無黍稷，我之父母當為何所依怙乎！乃告於天云：悠悠乎遠者蒼蒼之上天，何時乎使我得其所，免此征役，復平常人乎！人窮則反本，困則告天。此時征役未止，故訴天告怨也。</ab>
        </div>
        <div source="#sjTangfengs01e1_com_Mao">
          <head>傳「肅肅」至「樹止」。正義曰：</head>
          <ab>「苞，稹」，《釋言》文。孫炎曰：「物叢生曰苞，齊人名曰稹。」郭璞曰：「今人呼物叢緻者為稹。」</ab>
        </div>
        <div source="#sjTangfengs01e1_com_ZX">
          <head>箋云：稹者，根相迫迮梱緻貌，亦謂叢生也。「栩，杼」，《釋木》文。郭璞曰：「柞樹也。」陸機《疏》云：「今柞櫟也，徐州人謂櫟為杼，或謂之為栩。其子為皂，或言皂鬥，其殼為鬥，可以染。皂，今京洛及河內多言杼鬥。謂櫟為杼，五方通語也。」鴇鳥連蹄，性不樹止，樹止則為苦，故以喻君子從征役為危苦也。</head>
          <ab>Mao E2	傳「盬不」至「怙恃」。</ab>
        </div>
        <div source="#sjTangfengs01e2_com_Mao">
          <head>傳「盬不」至「怙恃」。正義曰：</head>
          <ab>盬與蠱，字異義同。昭元年《左傳》云：「於文皿蟲為蠱。穀之飛亦為蠱。」杜預云：「皿器受蟲害者為蠱，穀久積則變為飛蟲，名曰蠱。」然則蟲害器、敗穀者皆謂之蠱，是盬為不攻牢不堅緻之意也。此云「盬，不攻緻」，《四牡》傳云「盬，不堅固」，其義同也。定本「緻」皆作「致」。《蓼莪》云「無父何怙，無母何恃」，怙、恃義同。言父母當何恃食，故下言「何食」、「何嘗」，與此相接成也。</ab>
        </div>
        <div source="#sjTangfengs01e2_com_ZX">
          <head>箋「蓺樹」至「怙乎」。正義曰：</head>
          <ab>何知不為身在役所，不得營農，而云王事盡力，雖歸既則罷倦不能播種者，以經不云「不得」，而云「不能」，明是筋力疲極，雖歸而不能也。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjTangfengs02">
      <div type="commentary" source="#sjTangfengs02e3">
        <div resp="#ZX" xml:id="sjTangfengs02e3_com_ZX">
          <head>箋云：</head>
          <ab>極，已也。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjTangfengs03">
      <div type="commentary" source="#sjTangfengs03e1">
        <div resp="#Mao" xml:id="sjTangfengs03e1_com_Mao">
          <ab>行，翮也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjTangfengs03e1_com_LDM">
          <ab>行，戶郎反，注同。翮，戶革反，《爾雅》云：「羽本謂之翮。」</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjTangfengs03e4_com_Mao">
          <head>疏傳「行，翮也」。正義曰：</head>
          <ab>以上言羽翼，明行亦羽翼，以鳥翮之毛有行列，故稱行也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《鴇羽》三章，章七句。</ab>
  </back>
</text>
</TEI>

