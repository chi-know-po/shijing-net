<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風曹風之蜉蝣（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Fuyou in the Caofeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="384" to="384"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷七</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷七</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80146&amp;amp;page=2</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjCaofeng">
        <ab resp="#Mao" xml:id="sjCaofeng_com_Mao">《蜉蝣》，刺奢也。昭公國小而迫，無法以自守，好奢而任小人，將無所依焉。</ab>
        <ab resp="#LDM" xml:id="sjCaofeng_com_LDM">蜉蝣，上音浮，下音由，渠略也。國小，一本作「昭公國小而迫」。案《鄭譜》云：「昭公好奢而任小人，曹之變風始作。」此詩箋云：「喻昭公之朝。」是《蜉蝣》為昭公詩也。《譜》又云《蜉蝣》至《下泉》四篇，共公時作。今諸本此序多無「昭公」字，崔《集注》本有，未詳其正也。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjCaofeng_com_Mao">
          <head>疏「《蜉蝣》三章，章四句」至「依焉」。正義曰：</head>
          <ab>作《蜉蝣》詩者，刺奢也。昭公之國既小，而迫脅於大國之間，又無治國之法以自保守，好為奢侈而任用小人，國家危亡無日，君將無所依焉，故君子憂而刺之也。好奢而任小人者，三章上二句是也。將無所依，下二句是也。三章皆刺好奢，又互相見。首章言「衣裳楚楚」，見其鮮明。二章言「采采」，見其眾多。卒章言「麻衣」，見其衣體。卒章「麻衣」，是諸侯夕時所服，則首章是朝時所服及其餘衣服也。二章言眾多，見其上下之服皆眾多也。首章言「蜉蝣之羽」，二章言「之翼」，言有羽翼而己，不言其美。卒章乃言其色美，亦互以為興也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjCaofeng">
      <lg type="stanza" xml:id="sjCaofengs01">
        <lg type="couplet" xml:id="sjCaofengs01e1">
          <l xml:id="sjCaofengs01l1">蜉蝣之羽，</l>
          <l xml:id="sjCaofengs01l2">衣裳楚楚。</l>
        </lg>
        <lg type="couplet" xml:id="sjCaofengs01e2">
          <l xml:id="sjCaofengs01l3">心之憂矣，</l>
          <l xml:id="sjCaofengs01l4">於我歸處。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjCaofengs02">
        <lg type="couplet" xml:id="sjCaofengs02e1">
          <l xml:id="sjCaofengs02l1">蜉蝣之翼，</l>
          <l xml:id="sjCaofengs02l2">采采衣服。</l>
        </lg>
        <lg type="couplet" xml:id="sjCaofengs02e2">
          <l xml:id="sjCaofengs02l3">心之憂矣，</l>
          <l xml:id="sjCaofengs02l4">於我歸息。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjCaofengs03">
        <lg type="couplet" xml:id="sjCaofengs03e1">
          <l xml:id="sjCaofengs03l1">蜉蝣掘閱，</l>
          <l xml:id="sjCaofengs03l2">麻衣如雪。</l>
        </lg>
        <lg type="couplet" xml:id="sjCaofengs03e2">
          <l xml:id="sjCaofengs03l3">心之憂矣，</l>
          <l xml:id="sjCaofengs03l4">於我歸說。</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjCaofengs01">
      <div type="commentary" source="#sjCaofengs01e1">
        <div resp="#Mao" xml:id="sjCaofengs01e1_com_Mao">
          <ab>興也。蜉蝣，渠略也，朝生夕死，猶有羽翼以自修飾。楚楚，鮮明貌。</ab>
        </div>
        <div resp="#ZX" xml:id="sjCaofengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>興者，喻昭公之朝，其群臣皆小人也。徒整飾其衣裳，不知國之將迫脅，君臣死亡無日，如渠略然。</ab>
        </div>
        <div resp="#LDM" xml:id="sjCaofengs01e1_com_LDM">
          <ab>楚楚，如字，《說文》作「𪓌𪓌」，云「會五采鮮色也」。渠，本或作「蟝」，音同，其居反；略，本或作「𧐣」，音同，沈云：「二字並不施蟲。」是也。朝，直遙反，下皆同；一讀下朝夕字，張遙反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjCaofengs01e2">
        <div resp="#ZX" xml:id="sjCaofengs01e2_com_ZX">
          <head>箋云：</head>
          <ab>歸，依歸。君當於何依歸乎？言有危亡之難，將無所就往。</ab>
        </div>
        <div resp="#LDM" xml:id="sjCaofengs01e2_com_LDM">
          <ab>難，乃旦反。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjCaofengs01e1_com_Mao">
          <head>傳「蜉蝣」至「明貌」。正義曰：</head>
          <ab>《釋蟲》云：「蜉蝣，渠略。」舍人曰：「蜉蝣，一名渠略，南陽以東曰蜉蝣，梁、宋之間曰渠略。」孫炎曰：「《夏小正》云：『蜉蝣，渠略也，朝生而暮死。』」郭璞曰：「似蛣蜣，身狹而長，有角，黃黑色。叢生糞土中，朝生暮死。豬好啖之。」陸機《疏》云：「蜉蝣，方土語也，通謂之渠略，似甲蟲，有角，大如指，長三四寸，甲下有翅，能飛。夏月陰雨時，地中出。今人燒炙啖之，美如蟬也。「樊光謂之糞中蠍蟲，隨陰雨時為之，朝生而夕死。定本亦云「渠略」，俗本作「渠螻」者，誤也。</ab>
        </div>
        <div source="#sjCaofengs01e1_com_ZX">
          <head>箋「興者」至「渠略」。正義曰：</head>
          <ab>以序云「任小人」，故云其群臣皆小人耳。其實此言衣裳楚楚，亦刺昭公之身，非獨刺小人也。何則？卒章「麻衣」謂諸侯之身夕服深衣，則知此章衣裳亦有君之衣裳。以蜉蝣朝生夕死，故知喻國將迫脅，死亡無日。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjCaofengs02">
      <div type="commentary" source="#sjCaofengs02e1">
        <div resp="#Mao" xml:id="sjCaofengs02e1_com_Mao">
          <ab>采采，眾多也。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjCaofengs02e2">
        <div resp="#Mao" xml:id="sjCaofengs02e2_com_Mao">
          <ab>息，止也。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjCaofengs02e1_com_Mao">
          <head>疏傳「采采，眾多」。正義曰：</head>
          <ab>以《卷耳》、《芣苡》言「采采」者，眾多非一之辭，知此「采采」亦為眾多。「楚楚」於「衣裳」之下，是為衣裳之貌。今「采采」在「衣服」之上，故知言多有衣服，非衣裳之貌也。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjCaofengs03">
      <div type="commentary" source="#sjCaofengs03e1">
        <div resp="#Mao" xml:id="sjCaofengs03e1_com_Mao">
          <ab>掘閱，容閱也。如雪，言鮮絜。</ab>
        </div>
        <div resp="#ZX" xml:id="sjCaofengs03e1_com_ZX">
          <head>箋云：</head>
          <ab>掘閱，掘地解，謂其始生時也。以解閱喻君臣朝夕變易衣服也。麻衣，深衣。諸侯之朝朝服，朝夕則深衣也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjCaofengs03e1_com_LDM">
          <ab>掘，求勿反。閱音悅。解音蟹，下同。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjCaofengs03e2">
        <div resp="#ZX" xml:id="sjCaofengs03e2_com_ZX">
          <head>箋云：</head>
          <ab>說猶舍息也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjCaofengs03e2_com_LDM">
          <ab>說音稅，協韻如字。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjCaofengs03e1_com_Mao">
          <head>傳「掘閱」至「鮮絜」。正義曰：</head>
          <ab>此蟲土裏化生。閱者，悅懌之意。掘閱者，言其掘地而出，形容解閱也。麻衣者，白布衣。如雪，言甚鮮絜也。</ab>
        </div>
        <div source="#sjCaofengs03e1_com_ZX">
          <head>箋「掘地」至「深衣」。正義曰：</head>
          <ab>定本云「掘地解閱，謂開解而容閱」，義亦通也。上言羽翼，謂其成蟲之後。此掘閱，舉其始生之時。蟲以朝夕容貌不同，故知喻君臣朝夕變易衣服也。言麻衣，則此衣純用布也。衣裳即布，而色白如雪者，謂深衣為然，故知麻衣是深衣也。鄭又自明己意，所以知麻是布深衣者，以諸侯之朝夕則深衣故也。《玉藻》說諸侯之禮云：「夕深衣，祭牢肉。」是諸侯之服夕深衣也。深衣，布衣，升數無文也。《雜記》云：「朝服十五升。」然則深衣之布亦十五升矣，故《間傳》云「大祥素縞麻衣」，注云：「麻衣，十五升，布深衣也。純用布，無采飾。」是鄭以深衣之布為十五升也。彼是大祥之服，故云「無采飾」耳。而《禮記·深衣》之篇說深衣之制云：「孤子衣純以素。非孤子者，皆不用素純。」此諸侯夕服當用十五升布深衣，而純以采也。以其衣用布，故稱麻耳。案《喪服記》：「公子為其母麻衣，縓緣。」注云：「麻衣者，小功布深衣。」引詩云：「麻衣如雪。」若深衣用十五升布為，而彼注以麻衣為小功布者，以大功章云：「公之庶昆弟為其母。」言公之昆弟，則父卒矣。父卒為母大功，父在之時，雖不在五服之例，其縷粗細宜降大功一等，用小功布深衣。引此者，證麻衣是布深衣耳，不謂此言麻衣，其縷亦如小功布也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《蜉蝣》三章，章四句。</ab>
  </back>
</text>
</TEI>

