<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風周南之卷耳（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Juaner in the Zhounan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="277" to="278"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=67</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj003" type="preface">
        <ab resp="#Mao" xml:id="sj003_com_Mao">《卷耳》，后妃之志也，又當輔佐君子，求賢審官，知臣下之勤勞。內有進賢之志，而無險詖私謁之心，朝夕思念，至於憂勤也。</ab>
        <ab resp="#ZX" xml:id="sj003_com_ZX">謁，請也。</ab>
        <ab resp="#LDM" xml:id="sj003_com_LDM">卷耳，卷勉反，苓耳也。《廣雅》云：「枲耳也。」郭云：「亦曰胡枲，江南呼常枲。」《草木疏》云：「幽州人謂之爵耳。」詖，彼寄反，妄加人以罪也。崔云：「險詖，不正也。」苓音零。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj003_com_Mao">
          <head>［疏］「《卷耳》四章，章四句」至「憂勤」。正義曰：</head>
          <ab>作《卷耳》詩者，言后妃之志也。后妃非直憂在進賢，躬率婦道，又當輔佐君子，其志欲令君子求賢德之人，審置於官位，復知臣下出使之勤勞，欲令君子賞勞之。內有進賢人之志，唯有德是用，而無險詖不正，私請用其親戚之心，又朝夕思此，欲此君子官賢人，乃至於憂思而成勤。此是后妃之志也。言「又」者，繫前之辭，雖則異篇，而同是一人之事，故言「又」，為亞次也。輔佐君子，總辭也。求賢審官，至於憂勤，皆是輔佐君子之事，君子所專，后妃志意如然，故云后妃之志也。險詖者，情實不正，譽惡為善之辭也。私謁者，婦人有寵，多私薦親戚，故厲王以豔妻方煽；七子在朝，成湯謝過。婦謁盛與險詖私謁，是婦人之常態，聖人猶恐不免。后妃能無此心，故美之也。至於憂勤，勤為勞心，憂深不已，至於勞勤，后妃之篤志也。至於憂勤，即首章上二句是也。求賢審官，即首章下二句是也。經、敘倒者，敘見后妃求賢而憂勤，故先言求賢，經主美后妃之志，能為此憂勤，故先言其憂也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj003">
      <lg type="stanza" xml:id="sj003s01">
        <lg type="couplet" xml:id="sj003s01e1">
          <l xml:id="sj003s01l1">采采卷耳ㆍ</l>
          <l xml:id="sj003s01l2">不盈頃筐ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj003s01e2">
          <l xml:id="sj003s01l3">嗟我懷人ㆍ</l>
          <l xml:id="sj003s01l4">寘彼周行ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj003s02">
        <lg type="couplet" xml:id="sj003s02e1">
          <l xml:id="sj003s02l1">陟彼崔嵬ㆍ</l>
          <l xml:id="sj003s02l2">我馬虺隤ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj003s02e2">
          <l xml:id="sj003s02l3">我姑酌彼金罍ㆍ</l>
          <l xml:id="sj003s02l4">維以不永懷ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj003s03">
        <lg type="couplet" xml:id="sj003s03e1">
          <l xml:id="sj003s03l1">陟彼高岡ㆍ</l>
          <l xml:id="sj003s03l2">我馬玄黃ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj003s03e2">
          <l xml:id="sj003s03l3">我姑酌彼兕觥ㆍ</l>
          <l xml:id="sj003s03l4">維以不永傷ㆍ</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj003s04">
        <lg type="couplet" xml:id="sj003s04e1">
          <l xml:id="sj003s04l1">陟彼砠矣ㆍ</l>
          <l xml:id="sj003s04l2">我馬瘏矣ㆍ</l>
        </lg>
        <lg type="couplet" xml:id="sj003s04e2">
          <l xml:id="sj003s04l3">我僕痡矣ㆍ</l>
          <l xml:id="sj003s04l4">云何吁矣ㆍ</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj003s01" type="commentaries">
      <div source="#sj003s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj003s01e1_com_Mao">
          <ab>憂者之興也。采采，事采之也。卷耳，苓耳也。頃筐，畚屬，易盈之器也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s01e1_com_ZX">
          <head>箋云：</head>
          <ab>器之易盈而不盈者，志在輔佐君子，憂思深也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s01e1_com_LDM">
          <ab>頃音傾。筐，起狂反。《韓詩》云：「頃筐，欹筐也。」畚音本，何休云「草器也」，《說文》同。易，以豉反，下同。思，息吏反，下「憂思」同。</ab>
        </div>
      </div>
      <div source="#sj003s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj003s01e2_com_Mao">
          <ab>懷，思。寘，置。行，列也。思君子官賢人，置周之列位。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s01e2_com_ZX">
          <head>箋云：</head>
          <ab>周之列位，謂朝廷臣也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s01e2_com_LDM">
          <ab>寘，之豉反。行，戶康反。注下同。朝，直遙反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj003s01">
          <head>［疏］「采采」至「周行」。正義曰：</head>
          <ab>言有人事采此卷耳之菜，不能滿此頃筐。頃筐，易盈之器，而不能滿者，由此人志有所念，憂思不在於此故也。此采菜之人憂念之深矣，以興后妃志在輔佐君子，欲其官賢賞勞，朝夕思念，至於憂勤。其憂思深遠，亦如采菜之人也。此后妃之憂為何事，言后妃嗟呼而嘆，我思君子官賢人，欲令君子置此賢人於彼周之列位，以為朝廷臣也。我者，后妃自我也。(ZX S2 E1+E2)下箋云「我，我使臣」，「我，我君」。此不解者，以詩主美后妃，故不特言也。言彼者，后妃主求賢人為此，故以周行為彼也。</ab>
        </div>
        <div source="#sj003s01e1_com_Mao">
          <head>傳「憂者」至「之器」。正義曰：</head>
          <ab>不云興也，而云憂者之興，明有異於餘興也。餘興言采菜，即取采菜喻；言生長，即以生長喻。此言采菜而取憂為興，故特言憂者之興，言興取其憂而已，不取其采菜也。言事采之者，言勤事采此菜也。此與《芣苡》俱言「采采」，彼傳云「非一辭」，與此不同者，此取憂為興，言勤事采菜，尚不盈筐，言其憂之極，故云「事采之」；彼以婦人樂有子，明其采者眾，故云「非一辭」。其實采采之義同，故《鄭志》答張逸云：「事謂事事一一用意之事。《芣苡》亦然。雖說異，義則同。」是也。然則此謂一人之身念采非一，彼《芣苡》謂采人眾多非一，故鄭云「義則同」也。「卷耳，苓耳」，《釋草》文。郭璞曰：「《廣雅》云枲耳，亦云胡枲，江東呼常枲，或曰苓耳。形似鼠耳，叢生似盤。」陸璣《疏》云：「葉青白色，似胡荽，白華細莖蔓生，可煮為茹，滑而少味。四月中生子，如婦人耳中璫，今或謂之耳璫，幽州人謂之爵耳是也。」言「頃筐，畚屬」者，《說文》云：「畚，草器，所以盛種。」此頃筐可盛菜，故言畚屬以曉人也。言「易盈之器」者，明此器易盈，自有所憂，不能盈耳。解以不盈為喻之意也。</ab>
        </div>
        <div source="#sj003s01e2_com_ZX">
          <head>箋「周之」至「延臣」。正義曰：</head>
          <ab>知者，以其言周行是周之列位，周是后妃之朝，故知官人是朝廷臣也。襄十五年傳引「《詩》曰『嗟我懷人，寘彼周行』，能官人也。王及公、侯、伯、子、男、采、衛、大夫各居其列，所謂周行也」。彼非朝廷臣，亦言周行者，傳證楚能官人，引《詩》斷章，故不與此同。</ab>
        </div>
      </div>
    </div>
    <div source="#sj003s02" type="commentaries">
      <div source="#sj003s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj003s02e1_com_Mao">
          <ab>陟，升也。崔嵬，土山之戴石者，虺隤，病也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s02e1_com_ZX">
          <head>箋云：</head>
          <ab>我，我使臣也。臣以兵役之事行出，離其列位，身勤勞於山險，而馬又病，君子宜知其然。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s02e1_com_LDM">
          <ab>崔，徂回反。嵬，五回反。虺，呼回反，徐呼懷反，《說文》作「㾯」。隤，徒回反，徐徒壞反，《爾雅》同。孫炎云：「馬退不能升之病也。」《說文》作「頹」。使，色吏反，下同。離，力智反。</ab>
        </div>
      </div>
      <div source="#sj003s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj003s02e2_com_Mao">
          <ab>姑，且也。人君黃金罍。永，長也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s02e2_com_ZX">
          <head>箋云：</head>
          <ab>我，我君也。臣出使，功成而反，君且當設饗燕之禮，與之飲酒以勞之，我則以是不復長憂思也。言且者，君賞功臣，或多於此。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s02e2_com_LDM">
          <ab>姑如字，《說文》作「夃」，音同，云「秦以市買多得為夃」。罍，盧回反，酒樽也。《韓詩》云：「天子以玉飾，諸侯、大夫皆以黃金飾，士以梓。」《禮記》云：「夏曰山罍，其形似壺，容一斛，刻而畫之，為雲雷之形。」勞，力到反。「不復」，扶富反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj003s02">
          <head>［疏］「陟彼」至「永懷」。正義曰：</head>
          <ab>后妃言升彼崔嵬山巔之上者，我使臣也。我使臣以兵役之事行出，離其列位，在於山險，身已勤苦矣，其馬又虺隤而病，我之君子當宜知其然。若其還也，我君子且酌彼金罍之酒，饗燕以勞之，我則維以此之故，不復長憂思矣。我所以憂思，恐君子不知之耳。君子知之，故不復憂也。</ab>
        </div>
        <div source="#sj003s02e1_com_Mao">
          <head>傳「崔嵬」至「隤病」。正義曰：</head>
          <ab>《釋山》云：「石戴土謂之崔嵬。」孫炎曰：「石山上有土者。」又云：「土戴石為砠。」孫炎曰：「土山上有石者。」此及下傳云「石山戴土曰砠」，與《《爾雅》正反者，或傳寫誤也。《釋詁》云：「虺隤、玄黃，病也。」孫炎曰：「虺隤，馬罷不能升高之病。玄黃，馬更黃色之病。」然則虺隤者病之狀，玄黃者病之變色，二章互言之也。</ab>
        </div>
        <div source="#sj003s02e1_com_ZX">
          <head>箋「我我」至「其然」。正義曰：</head>
          <ab>序云「知臣下之勤勞」，故知使臣也。定本云「我，我臣也」，無「使」字。言勤勞，故知兵役之事。事莫勞於兵役，故舉其尢苦而言之。其實聘使之勞，亦閔念之，《四牡》之篇是其事也。言君子宜知其然，謂未還宜知之，還則宜賞之，故上句欲君子知其勞，下句欲君子加其賞也。</ab>
        </div>
        <div source="#sj003s02e2_com_Mao">
          <head>傳「人君黃金罍」。正義曰：</head>
          <ab>此無文也，故《異義》：罍制，《韓詩》說「金罍，大夫器也。天子以玉，諸侯、大夫皆以金，士以梓」；《毛詩》說「金罍，酒器也，諸臣之所酢。人君以黃金飾尊，大一碩，金飾龜目，蓋刻為雲雷之象」。謹案《韓詩》說天子以玉，經無明文。謂之罍者，取象雲雷博施，如人君下及諸臣。又《司尊彞》云：「皆有罍，諸臣之所酢。」注云：「罍亦刻而畫之，為山雲之形。」言刻畫，則用木矣，故《禮圖》依制度云刻木為之。《韓詩》說言士以梓，士無飾，言其木體則以上同用梓而加飾耳。毛說言大一碩，《禮圖》亦云大一斛，則大小之制，尊卑同也。雖尊卑飾異，皆得畫雲雷之形，以其名罍，取於雲雷故也。《毛詩》說諸臣之所酢，與《周禮》文同，則「人君黃金罍」，謂天子也。《周南》王者之風，故皆以天子之事言焉。</ab>
        </div>
        <div source="#sj003s02e2_com_ZX">
          <head>箋「我我」至「於此」。正義曰：</head>
          <ab>以后妃有其志耳。事不敢專，故知所勞臣者，君也。言臣出使，功成而反者，《聘義》云：「使者聘而誤，主君不親饗」，明功不成不勞之也；將率之敗，非徒無賞，亦自有罪。故知功成而反也。設饗燕之禮者，以經云金罍兕觥皆陳酒事，與臣飲酒，唯饗燕耳。言且者，君賞功臣，或多於此，言或當更有賞賜，非徒饗燕而已。僖三十三年，郤鈌獲白狄子，受一命之服；宣十五年，茍林父滅潞，晉侯賜以千室之邑，是其多也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj003s03" type="commentaries">
      <div source="#sj003s03e1 #sj003s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj003s03e1e2_com_Mao">
          <ab>山脊曰岡。玄，馬病則黃。兕觥，角爵也。傷，思也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s03e1e2_com_ZX">
          <head>箋云：</head>
          <ab>此章為意不盡，申殷勤也。觥，罰爵也。饗燕所以有之者，禮自立司正之後，旅酬必有醉而失禮者，罰之亦所以為樂。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s03e1e2_com_LDM">
          <ab>岡，古康反。兕，徐履反。觥，古橫反，以兕角為之。《韓詩》云容五升，《禮圖》云容七升。「為意」，于偽反。殷勤並如字，俗本下並加「心」，非也。樂音洛。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj003s03_com_Mao">
          <head>［疏］傳「山脊」至「角爵」。正義曰：</head>
          <ab>《釋山》云：「山脊，岡。」孫炎曰：「長山之脊也。」《釋獸》云：「兕，似牛。」郭璞曰：「一角，青色，重千斤者。」以其言兕，必以兕角為之觥者。爵，稱也。爵總名，故云角爵也。</ab>
        </div>
        <div source="#sj003s03_com_ZX">
          <head>箋「此章」至「為樂」。正義曰：</head>
          <ab>詩本畜志發憤，情寄於辭，故有意不盡，重章以申殷勤。詩之初始有此，故解之。傳云「兕觥，角爵」，言其體。此言「觥，罰爵」，解其用。言兕表用角，言觥顯其罰，二者相接也異義。《韓詩》說「一升曰爵，爵，盡也，足也。二升曰觚，觚，寡也，飲當寡少。三升曰觶，觶，適也，飲當自適也。四升曰角，角，觸也，不能自適，觸罪過也。五升曰散，散，訕也，飲不自節，為人謗訕。總名曰爵，其實曰觴。觴者，餉也。觥亦五升，所以罰不敬。觥，廓也，所以著明之貌，君子有過，廓然著明，非所以餉，不得名觴」。《詩》毛說觥大七，升許慎謹案：「觥罰有過，一飲而盡，七升為過多。」由此言之，則觥是觚、觶、角、散之外別有此器，故《禮器》曰：「宗廟之祭，貴者獻以爵，賤者獻以散，尊者舉觶，卑者舉角。」《特牲》二爵、二觚、四觶、一角、一散，不言觥之所用，是正禮無觥，不在五爵之例。《禮圖》云：「觥大七升，以兕角為之。」先師說云：「刻木為之。形似兕角。」蓋無兕者，用木也。知觥必以罰者，《地官·閭胥》：「掌其比、觥撻罰之事。」注云：「觥撻者，失禮之罰也。觥用酒，其爵以兕角為之。」《春官·小胥職》亦云：「觥其不敬者。」是以觥罰人之義也。故《桑扈》、《絲衣》皆云「兕觥其觩」，明為罰而不犯矣。饗燕之禮有兕觥者，以饗燕之禮，立司正之後，旅酬無算，必有醉而失禮者，以觥罰之，亦所以為樂也。然則此后妃志使君勞臣，宜是賢者，不應失禮而用觥者。禮法饗燕須設之耳，不謂即以罰人也。知饗有觥者，《七月》云：「朋酒斯饗，稱彼兕觥。」成十四年《左傳》「衛侯饗苦成成叔」，甯惠子引《詩》云：「兕觥其觩，旨酒思柔。」故知饗有觥也。饗以訓恭儉，不應醉而用觥者。饗禮之初示敬，故酒清而不敢飲，肉乾而不敢食，其末亦如燕法。鄉飲酒，大夫之饗禮，亦有旅酬，無算爵，則饗末亦有旅酬，恐其失禮，故用觥也。知燕亦有觥者，昭元年《左傳》鄭人燕趙孟、穆叔子皮及曹大夫，「興拜，舉兕爵」，是燕有兕觥也。鄉飲酒禮無觥者，說行禮，不言其有過之事故也。又知用觥在立司正之後者，《燕禮》立射人為司正之後，乃云：「北面命大夫。君曰：『以我安卿大夫。』皆對曰：『諾。敢不安！』」又曰：「賓反入，及卿大夫，皆脫屨升，就席。公以賓及卿大夫皆坐，乃安。」又：「司正升受命。君曰：『無不醉。』賓及卿大夫皆興，對曰：『諾。敢不醉！』以此言之，立司正之後，君命安，賓又升堂，皆坐，命之無不醉。於此以後，恐其失禮，故知宜有觥也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj003s04" type="commentaries">
      <div source="#sj003s04e1 #sj003s04e2" type="commentary">
        <div resp="#Mao" xml:id="sj003s04e1e2_com_Mao">
          <ab>石山戴土曰砠。瘏，病也。痡，亦病也。籲，憂也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj003s04e1e2_com_ZX">
          <head>箋云：</head>
          <ab>此章言臣既勤勞於外，僕馬皆病，而今云何乎其亦憂矣，深閔之辭。</ab>
        </div>
        <div resp="#LDM" xml:id="sj003s04e1e2_com_LDM">
          <ab>「砠」，七餘反。瘏音塗，本又作「屠」，非。痡音敷，又普烏反，本又作「鋪」，同。籲，香於反。「痡，病也」，一本作「痡，亦病也」者，非。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj003s04_com_Mao">
          <head>［疏］傳「瘏，病。痡，亦病也」。正義曰：</head>
          <ab>《釋詁》云：「痡、瘏，病也。」孫炎曰：「痡，人疲不能行之病。瘏，馬疲不能進之病也。」</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《卷耳》四章，章四句。</ab>
  </back>
</text>
</TEI>

