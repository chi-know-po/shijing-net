<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風魏風之碩鼠（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Shuoshu in the Weifeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="359" to="359"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷五</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷五</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80144&amp;amp;page=91</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjWeifeng">
        <ab resp="#Mao" xml:id="sjWeifeng_com_Mao">《碩鼠》，刺重斂也。國人刺其君重斂，蠶食於民，不修其政，貪而畏人，若大鼠也。</ab>
        <ab resp="#LDM" xml:id="sjWeifeng_com_LDM">碩音石。斂，呂驗反，下同。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjWeifeng+_com_poem">
          <head>疏「《碩鼠》三章，章八句」至「大鼠」。正義曰：</head>
          <ab>蠶食者，蠶之食桑，漸漸以食，使桑盡也。猶君重斂，漸漸以稅，使民囷也。言貪而畏人，若大鼠然，解本以碩鼠為喻之意，取其貪且畏人，故序因倒述其事。經三章，皆上二句言重斂，次二句言不修其政。由君重斂，不修其政，故下四句言將棄君而去也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjWeifeng">
      <lg type="stanza" xml:id="sjWeifengs01">
        <lg type="couplet" xml:id="sjWeifengs01e1">
          <l xml:id="sjWeifengs01l1">碩鼠碩鼠，</l>
          <l xml:id="sjWeifengs01l2">無食我黍！</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs01e2">
          <l xml:id="sjWeifengs01l3">三歲貫女，</l>
          <l xml:id="sjWeifengs01l4">莫我肯顧。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs01e3">
          <l xml:id="sjWeifengs01l5">逝將去女，</l>
          <l xml:id="sjWeifengs01l6">適彼樂土。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs01e4">
          <l xml:id="sjWeifengs01l7">樂土樂土，</l>
          <l xml:id="sjWeifengs01l8">爰得我所！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjWeifengs02">
        <lg type="couplet" xml:id="sjWeifengs02e1">
          <l xml:id="sjWeifengs02l1">碩鼠碩鼠，</l>
          <l xml:id="sjWeifengs02l2">無食我麥！</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs02e2">
          <l xml:id="sjWeifengs02l3">三歲貫女，</l>
          <l xml:id="sjWeifengs02l4">莫我肯德。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs02e3">
          <l xml:id="sjWeifengs02l5">逝將去女，</l>
          <l xml:id="sjWeifengs02l6">適彼樂國。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs02e4">
          <l xml:id="sjWeifengs02l7">樂國樂國，</l>
          <l xml:id="sjWeifengs02l8">爰得我直！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjWeifengs03">
        <lg type="couplet" xml:id="sjWeifengs03e1">
          <l xml:id="sjWeifengs03l1">碩鼠碩鼠，</l>
          <l xml:id="sjWeifengs03l2">無食我苗！</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs03e2">
          <l xml:id="sjWeifengs03l3">三歲貫女，</l>
          <l xml:id="sjWeifengs03l4">莫我肯勞。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs03e3">
          <l xml:id="sjWeifengs03l5">逝將去女，</l>
          <l xml:id="sjWeifengs03l6">適彼樂郊。</l>
        </lg>
        <lg type="couplet" xml:id="sjWeifengs03e4">
          <l xml:id="sjWeifengs03l7">樂郊樂郊，</l>
          <l xml:id="sjWeifengs03l8">誰之永號！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjWeifengs01">
      <div type="commentary" source="#sjWeifengs01e1">
        <div resp="#Mao" xml:id="sjWeifengs01e1_com_Mao">
          <ab>貫，事也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjWeifengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>碩，大也。大鼠大鼠者，斥其君也。女無復食我黍，疾其稅斂之多也。我事女三歲矣，曾無教令恩德來眷顧我，又疾其不修政也。古者三年大比，民或於是徙。</ab>
        </div>
        <div resp="#LDM" xml:id="sjWeifengs01e1_com_LDM">
          <ab>貫，古亂反，徐音官。復，扶又反。稅，始銳反。比，毗誌反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs01e3">
        <div resp="#ZX" xml:id="sjWeifengs01e3_com_ZX">
          <head>箋云：</head>
          <ab>逝，往也。往矣將去女，與之訣別之辭。樂土，有德之國。</ab>
        </div>
        <div resp="#LDM" xml:id="sjWeifengs01e3_com_LDM">
          <ab>樂音洛，注下同。土如字，他古反，沈徒古反。訣，古穴反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs01e4">
        <div resp="#ZX" xml:id="sjWeifengs01e4_com_ZX">
          <head>箋云：</head>
          <ab>爰，曰也。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjWeifengs01">
          <head>疏「碩鼠」至「得我所」。正義曰：</head>
          <ab>國人疾其君重斂畏人，比之碩鼠。言碩鼠碩鼠，無食我黍，猶言國君國君，無重斂我財。君非直重斂於我，又不修其政。我三歲以來事汝矣，曾無於我之處肯以教令恩德眷顧我也。君既如是，與之訣別，言往矣將去汝之彼樂土有德之國。我所以之彼樂土者，以此樂土，若往則曰得我所宜故也。言往將去汝者，謂我往之他國，將去汝國也。</ab>
        </div>
        <div source="#sjWeifengs01e2_com_Mao">
          <head>傳「貫，事」。正義曰：</head>
          <ab>《釋詁》文。</ab>
        </div>
        <div source="#sjWeifengs01e1_com_ZX">
          <head>箋「碩大」至「是徙」。正義曰：</head>
          <ab>「碩，大」，《釋詁》文。《釋獸》於鼠屬有鼫鼠，孫炎曰：「五技鼠。」郭璞曰：「大鼠，頭似兔，尾有毛青黃色，好在田中食粟豆，關西呼鼩音瞿鼠。」舍人、樊光同引此詩，以碩鼠為彼五技之鼠也。許慎云：「碩鼠五技，能飛不能上屋，能遊不能渡穀，能綠不能窮木，能走不能先人，能穴不能覆身，此之謂五技。」陸機《疏》云：「今河東有大鼠，能人立，交前兩腳於頸上跳舞，善鳴，食人禾苗。人逐則走入樹空中。亦有五技，或謂之雀鼠，其形大，故序云『大鼠也』。魏國，今河北縣是也。言其方物，宜謂此鼠非鼫鼠也。」按此經作「碩鼠」，訓之為大，不作「鼫鼠」之字，其義或如陸言也。序云「貪而畏人，若大鼠然」，故知大鼠為斥君，亦是興喻之義也。箋又以此民居魏，蓋應久矣。正言「三歲貫汝」者，以古者三歲大比，民或於是遷徙，故以三歲言之。《地官·小司徒》及《鄉大夫職》皆云三年則大比。言比者，謂大校，比其民之數而定其版籍，明於此時民或得徙。《地官·比長職》曰：「徙於國中及郊，則從而授之。」注云：徙謂不便其居也。或國中之民出徙郊，或郊民入徙國中，皆從而付所處之吏。是大比之際，民得徙矣。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjWeifengs02">
      <div type="commentary" source="#sjWeifengs02e1">
        <div resp="#ZX" xml:id="sjWeifengs02e1_com_ZX">
          <head>箋云：</head>
          <ab>不肯施德於我</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs02e3">
        <div resp="#Mao" xml:id="sjWeifengs02e3_com_Mao">
          <ab>直，得其直道。</ab>
        </div>
        <div resp="#ZX" xml:id="sjWeifengs02e3_com_ZX">
          <head>箋云：</head>
          <ab>直猶正也。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjWeifengs03">
      <div type="commentary" source="#sjWeifengs03e1">
        <div resp="#Mao" xml:id="sjWeifengs03e1_com_Mao">
          <ab>苗，嘉穀也。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs03e2">
        <div resp="#ZX" xml:id="sjWeifengs03e2_com_ZX">
          <head>箋云：</head>
          <ab>不肯勞來我。</ab>
        </div>
        <div resp="#LDM" xml:id="sjWeifengs03e2_com_LDM">
          <ab>勞，如字，又力報反，注同。徠，本亦作「來」，同力代反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs03e3">
        <div resp="#ZX" xml:id="sjWeifengs03e3_com_ZX">
          <head>箋云：</head>
          <ab>郭外曰郊。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjWeifengs03e4">
        <div resp="#Mao" xml:id="sjWeifengs03e4_com_Mao">
          <ab>號，呼也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjWeifengs03e4_com_ZX">
          <head>箋云：</head>
          <ab>之，往也。永，歌也。樂郊之地，誰獨當往而歌號者。言皆喜說無憂苦。</ab>
        </div>
        <div resp="#LDM" xml:id="sjWeifengs03e4_com_LDM">
          <ab>詠，本亦作「永」，同音詠。號，戶毛反，注同。呼，火故反。說音悅。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjWeifengs03e1_com_Mao">
          <head>疏傳「苗，嘉穀」。正義曰：</head>
          <ab>黍麥指穀實言之，是鼠之所食。苗之莖葉，以非鼠能食之，故云「嘉穀」，謂穀實也。穀生於苗，故言苗以韻句。</ab>
        </div>
        <div source="#sjWeifengs03e4">
          <head>疏「誰之永號」。正義曰：</head>
          <ab>言彼有德之樂郊，誰往而獨長歌號呼？言往釋皆歌號，喜樂得所，故我欲往也。箋「之，往。永，歌」。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《碩鼠》三章，章八句。</ab>
    <ab>魏國七篇，十八章，百二十八句。</ab>
  </back>
</text>
</TEI>

