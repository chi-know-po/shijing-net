<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風召南之采蘋（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Caipin in the Shaonan section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="286" to="287"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷一</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷一</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80140&amp;amp;page=140</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
    <front>
      <div type="introduction">
        <div source="#sj015" type="preface">
          <ab resp="#Mao" xml:id="sj015_com_Mao">《采蘋》，大夫妻能循法度也。能循法度，則可以承先祖，共祭祀矣。</ab>
          <ab resp="#ZX" xml:id="sj015_com_ZX">女子十年不出，姆教婉娩聽從，執麻枲，治絲繭，織紝組紃，學女事以共衣服。觀於祭祀，納酒漿籩豆菹醢，禮相助奠。十有五而笄，二十而嫁」。此言能循法度者，今既嫁為大夫妻，能循其為女之時所學所觀之事以為法度。</ab>
          <ab resp="#LDM" xml:id="sj015_com_LDM">蘋，符申反。《韓詩》云：「沈者曰蘋，浮者曰藻。」共音恭，本或作「供」，注同。姆，莫豆反，《字林》亡甫反，云「女師也」。鄭云：「婦人五十無子，出不復嫁，以婦道教人，若今時乳母也。」婉，怨遠反。娩音晚。枲，絲似反。繭，古顯反，本亦作「蠒」。紝，女金反，何如鴆反，繒帛之屬。組音祖，線也。紃音旬，絛也。漿，子詳反。醢音海。相，息亮反。笄，古兮反。</ab>
        </div>
        <div resp="#KYD" type="subcommentary">
          <div source="#sj015 #sj015_com_Mao">
            <head>疏「《采蘋》三章，章四句」至「祭祀矣」。正義曰：</head>
            <ab>作《采蘋》詩者，言大夫妻能循法度也。謂為女之時所學所觀之法度，今既嫁為大夫妻，能循之以為法度也。言既能循法度，即可以承事夫之先祖，供奉夫家祭祀矣。此謂已嫁為大夫妻，能循其為女時事也。經所陳在父母之家作教成之祭，經、序轉互相明也。</ab>
          </div>
          <div source="#sj015_com_ZX">
            <head>箋云「女子」至「法度」。正義曰：</head>
            <ab>從「二十而嫁」以上，皆《內則》文也。言女子十年不出者，對男子十年出就外傅也。《內則》注云：「婉謂言語也。娩之言媚也，媚謂容貌也。」則婉謂婦言，娩謂婦容。聽從者，聽受順從於人，所謂婦德也。執麻枲者，執治緝績之事。枲，麻也。《釋草》云：「枲，麻。」孫炎曰：「麻一名枲。」是也。治絲繭者，繭則繅之，絲則絡之。織紝組紃者，紝也、組也、紃也，三者皆織之。服虔注《左傳》曰「織紝，治繒帛」者，則紝謂繒帛也。《內則》注云：「紃，絛也。」組亦絛之類，大同小異耳。學女事者，謂治葛縫線之事，皆學之所以供衣服，是謂婦功也。此已上謂女所學四德之事。又觀於父母之家祭祀之事，納酒漿籩豆菹醢之禮。酒漿及籩豆，皆連上「納」文，謂當薦獻之節，納以進屍。《虞夏傳》曰「納以教成」，鄭云「謂薦獻時」，引此納酒漿以下證之。鄭知納謂薦獻者，《內則》云 「納酒漿」，與「納以教成」文同。菹醢以薦，酒漿以獻，納者進名，故知薦獻之時也。獻無漿而言之者，所以協句也。「籩豆菹醢」，菹醢在豆，籩盛脯羞，皆薦所用也。籩不言所盛，文不備耳。《少牢》、《特牲》皆先薦後獻，故鄭亦云「薦獻時」。此先酒後菹醢者，便文言之。禮相助奠者，言非直觀薦獻，又觀祭祀之相佐助奠設器物也。觀之，皆為婦當知之。此上謂所觀之事也。十五許嫁，故笄。未許嫁，二十而笄。二十而嫁，歸於夫家也。鄭引此者，序言「能循法度」，明先有法度，今更循之，故引此。是先有法度之事，乃言所循之時，故疊。序云「能循法度」者，為今嫁為大夫妻，能循其為女之時所學所觀之事以為法度也。此女之四德，十年以後，傳姆當教。至於先嫁三月，又重教之。此引《內則》論十年之後，下箋引《昏義》論三月之前，皆是為女之時法度，二注乃具也。鄭知經非正祭者，以《昏義》教成之祭，言「芼之以蘋藻」，此亦言蘋藻，故知為教成祭也。定本云「姆教婉娩」，勘禮本亦然，今俗云「傳姆教之」，誤也。又「十有五而笄」上無「女子」二字，有者亦非。</ab>
          </div>
        </div>
      </div>
      <div type="poem" xml:id="sj015">
        <lg type="stanza" xml:id="sj015s01">
          <lg type="couplet" xml:id="sj015s01e1">
            <l xml:id="sj015s01l1">于以采蘋？</l>
            <l xml:id="sj015s01l2">南澗之濱。</l>
          </lg>
          <lg type="couplet" xml:id="sj015s01e2">
            <l xml:id="sj015s01l3">于以采藻？</l>
            <l xml:id="sj015s01l4">于彼行潦。</l>
          </lg>
        </lg>
        <lg type="stanza" xml:id="sj015s02">
          <lg type="couplet" xml:id="sj015s02e1">
            <l xml:id="sj015s02l1">于以盛之？</l>
            <l xml:id="sj015s02l2">維筐及筥。</l>
          </lg>
          <lg type="couplet" xml:id="sj015s02e2">
            <l xml:id="sj015s02l3">于以湘之？</l>
            <l xml:id="sj015s02l4">維錡及釜。</l>
          </lg>
        </lg>
        <lg type="stanza" xml:id="sj015s03">
          <lg type="couplet" xml:id="sj015s03e1">
            <l xml:id="sj015s03l1">于以奠之？</l>
            <l xml:id="sj015s03l2">宗室牖下。</l>
          </lg>
          <lg type="couplet" xml:id="sj015s03e2">
            <l xml:id="sj015s03l3">誰其屍之？</l>
            <l xml:id="sj015s03l4">有齊季女。</l>
          </lg>
        </lg>
      </div>
    </front>
    <body>
      <div source="#sj015s01" type="commentaries">
        <div source="#sj015s01e1" type="commentary">
          <div resp="#Mao" xml:id="sj015s01e1_com_Mao">
            <ab>蘋，大蓱也。濱，涯也。藻，聚藻也。行潦，流潦也。</ab>
          </div>
          <div resp="#ZX" xml:id="sj015s01e1_com_ZX">
            <head>箋云：</head>
            <ab>「古者婦人先嫁三月，祖廟未毀，教於公宮；祖廟既毀，教於宗室。教以婦德、婦言、婦容、婦功。教成之祭，牲用魚，芼用蘋藻，所以成婦順也。」此祭，祭女所出祖也。法度莫大於四教，是又祭以成之，故舉以言焉。蘋之言賓也，藻之言澡也。婦人之行，尚柔順，自絜清，故取名以為戒。</ab>
          </div>
          <div resp="#LDM" xml:id="sj015s01e1_com_LDM">
            <ab>濱音賓，涯也。藻音早，水菜也。潦音老。蓱本又作「萍」，薄經反，一本作 「蘋」，音平。涯，本亦作「厓」，五隹反。先，蘇遍反。芼，莫報反，沈音毛。澡音早。行，下孟反。清如字，又音淨。</ab>
          </div>
        </div>
      </div>
      <div source="#sj015s02" type="commentaries">
        <div source="#sj015s02e1" type="commentary">
          <div resp="#Mao" xml:id="sj015s02e1_com_Mao">
            <ab>方曰筐。圓曰筥。湘，亨也。錡，釜屬，有足曰錡，無足曰釜。</ab>
          </div>
          <div resp="#ZX" xml:id="sj015s02e1_com_ZX">
            <head>箋云：</head>
            <ab>亨蘋藻者於魚湆之中，是鉶之芼。</ab>
          </div>
          <div resp="#LDM" xml:id="sj015s02e1_com_LDM">
            <ab> 盛音成。筐音匡。筥，居呂反。湘，息良反。錡，其綺反，三足釜也，《玉篇》宜綺反。釜，符甫反。亨本又作「烹」，同普更反，煮也。湆，去急反，汁也。鉶本或作「饣刑」，音形，鄭云三足兩耳，有蓋，和羹之器。羹音庚，劉昌宗音《儀禮》音衡。</ab>
          </div>
        </div>
      </div>
      <div source="#sj015s03" type="commentaries">
        <div source="#sj015s03e1" type="commentary">
          <div resp="#Mao" xml:id="sj015s03e1_com_Mao">
            <ab>奠，置也。宗室，大宗之廟也。大夫士祭於宗廟，奠於牖下。</ab>
          </div>
          <div resp="#ZX" xml:id="sj015s03e1_com_ZX">
            <head>箋云：</head>
            <ab>牖下，戶牖閒之前。祭不於室中者，凡昏事，於女禮設几筵於戶外，此其義也與？宗子主此祭，維君使有司為之。</ab>
          </div>
          <div resp="#LDM" xml:id="sj015s03e1_com_LDM">
            <ab>牖音酉，下如字，協韻則音戶，後皆放此。與音餘。</ab>
          </div>
        </div>
        <div source="#sj015s03e2" type="commentary">
          <div resp="#Mao" xml:id="sj015s03e2_com_Mao">
            <ab>屍，主。齊，敬。季，少也。蘋藻，薄物也。澗潦，至質也。筐筥錡釜，陋器也。少女，微主也。古之將嫁女者，必先禮之於宗室，牲用魚，芼之以蘋藻。</ab>
          </div>
          <div resp="#ZX" xml:id="sj015s03e2_com_ZX">
            <head>箋云：</head>
            <ab>主設羹者季女，則非禮也。女將行，父禮之而俟迎者，蓋母薦之，無祭事也。祭禮主婦設羹，教成之祭，更使季女者，成其婦禮也。季女不主魚，魚俎實男子設之，其粢盛蓋以黍稷。</ab>
          </div>
          <div resp="#LDM" xml:id="sj015s03e2_com_LDM">
            <ab>齊本亦作「齋」，同側皆反。少，詩照反，下同。迎，宜敬反。俎，側所反。齍音資，本或作「粢」。</ab>
          </div>
        </div>
      </div>
      <div source="#sj015s01 #sj015s02 #sj015s03" type="commentaries">
        <div resp="#KYD" type="subcommentary">
          <div source="#sj015">
            <head>疏「於以采蘋」至「季女」。正義曰：</head>
            <ab>三章勢連，須通解之也。大夫之妻，將行嫁，欲為教成之祭。言往何處采此蘋菜？於彼南澗之厓采之。往何處采此藻菜？於彼流潦之中采之。南澗言濱，行潦言彼，互言也。既得此菜，往何器盛之？維筐及筥盛之。既盛此菜而還，往何器烹煮之？維錡及釜之中煮之也。既煮之為羹，往何處置設之？於宗子之室戶外牖下設之。當設置之時，使誰主之？有齊莊之德少女主設之。</ab>
          </div>
          <div source="#sj015s01_com_Mao">
            <head>傳「蘋大」至「流潦」。正義曰：</head>
            <ab>《釋草》云：蘋，萍。其大者蘋。舍人曰：「蘋一名萍。」郭璞曰：「今水上浮蓱也，江東謂之薸。」音瓢。《左傳》曰：「蘋蘩蘊藻之菜。」蘊，聚也，故言藻聚。藻，陸機云：「藻，水草也，生水底。有二種：其一種葉如雞蘇，莖大如箸，長四五尺。其一種莖大如釵股，葉如蓬蒿，謂之聚藻。」然則藻聚生，故謂之聚藻也。行者，道也。《說文》云：「潦，雨水也。」然則行潦，道路之上流行之水。</ab>
          </div>
          <div source="#sj015s01_com_ZX">
            <head>箋「古者」至「為戒」。正義曰：</head>
            <ab>「成婦順」於上，皆《昏義》文。引之者，以此經陳教成之祭，以《昏義》亦為教成之祭，故引之，欲明教之早晚及其處所，故先言先嫁三月，祖廟未毀，教於公宮；祖廟既毀，教於宗室。既言其處，又說所教之事，故言教以婦德、婦言、婦容、婦功。既教之三月，成則設祭，故言教成之祭，牲用魚，芼之以蘋藻，為此祭所以成婦順也。事次皆為教成之祭，故具引之。必先嫁三月，更教之以四德，以法度之大，就尊者之宮，教之三月，一時天氣變，女德大成也。教之在宮，祭乃在廟也。知此祭，祭女所出祖者，以其言「祖廟既毀」，明未毀，祭其廟也。與天子諸侯同高祖，祭高祖廟；同曾祖，祭曾祖廟，故《昏義》注云：「祖廟，女所出之祖也。」宗室，宗子之家也。然則大宗之家，百世皆往，宗子尊不過卿大夫，立三廟二廟而已，雖同曾、高，無廟可祭，則五屬之外同告於壇，故《昏義》注云「若其祖廟已毀，則為壇而告焉」，是也。以魚為牲者，鄭云：「魚為俎實，蘋藻為羹菜。」祭無牲牢，告事耳，非正祭也。又解此大夫妻能循法度，獨言教成之祭者，以法度莫大於四教，四德既就，是又祭以成之，法度之大者，故詩人舉以言焉。又解祭不以餘菜，獨以蘋藻者，蘋之言賓，賓，服也，欲使婦人柔順服從；藻之言澡，澡，浴也，欲使婦人自絜清，故云「婦人之行尚柔順，自絜清，故取名以為戒」。《左傳》曰：「女贄不過榛、栗、棗、脩，以告虔。」言以告虔，取早起、戰栗、脩治法度、虔敬之義也，則此亦取名為戒，明矣。《昏義》注云「魚蘋藻皆水物，陰類」者，義得兩通。</ab>
          </div>
          <div source="#sj015s02_com_Mao">
            <head>傳「方曰筐」至「曰釜」。正義曰：</head>
            <ab>此皆《爾雅》無文，傳以當時驗之，以錡與釜連文，故知釜屬。《說文》曰：「江淮之間謂釜曰錡。」定本「有足曰錡」下更無傳，俗本「錡」下又云「無足曰釜」。</ab>
          </div>
          <div source="#sj015s02_com_ZX">
            <head>箋「亨蘋」至「之芼」。正義曰：</head>
            <ab>《少牢禮》用羊豕也。經云：「上利執羊俎，下利執豕俎。」下乃云：「上佐食羞兩鉶，取一羊鉶於房中，下佐食又取一豕鉶於房中，皆芼。」注云：「芼，菜也。羊用苦，豕用薇，皆有滑。」牲體在俎，下乃設羊鉶、豕鉶。云皆芼，煮於所亨之湆，始盛之鉶器也。故《特牲》注云：「鉶，肉味之有菜和者。」今教成祭，牲用魚，芼之以蘋藻，則魚體亦在俎，蘋藻亨於魚湆之中矣。故鄭云魚為俎實，蘋藻為羹菜，以準少牢之禮，故知在鉶中為鉶羹之芼。知非大羹盛在鐙者，以大羹不和，貴其質也。此有菜和，不得為大羹矣。《魯頌》曰：「毛炰胾羹。」傳曰：「羹，大羹、鉶羹也。」以經單言羹，故得兼二也。《特牲禮》云：「設大羹湆於醢北。」注云：「大羹湆，煮肉汁。」則湆，汁也。</ab>
          </div>
          <div source="#sj015s03e1_com_Mao">
            <head>傳「宗室」至「牖下」。正義曰：</head>
            <ab>傳以《昏義》云教於宗室是大宗之家，此言牖下，又非於壇，故知是大宗之廟。宗子有廟，則亦為大夫士矣。言大夫士祭於宗室，謂祖廟已毀，或非君同姓，故祭大宗之家也。知非宗子之女自祭家廟者，經言「於以奠之，宗室牖下」，若宗子之女自祭家廟，何須言於宗室乎？定本、《集注》皆云大夫士祭於宗廟，不作室字。</ab>
          </div>
          <div source="#sj015s03e1_com_ZX">
            <head>箋「牖下」至 「為之」。正義曰：</head>
            <ab>箋知「牖下、戶牖閒之前」者，以其正祭在奧西南隅，不直繼牖言之。今此云「牖下」，故為戶牖間之前，戶西牖東，去牖近，故云牖下。又解正祭在室，此所以不於室中者，以其!凡昏事，皆為於女行禮，設几筵於戶外，取外成之義。今教成之祭於戶外設奠，此外成之義。「與」是語助也。《昏禮》云：「納采，主人筵於戶西，西上，右幾。」問名、納吉、納徵、請期皆如初。《昏禮》又云：「主人筵於戶西，西上，右幾。」是其禮皆戶外設几筵也。知宗子主此祭者，以其就宗子家，明告神，宗子所主。引《昏義》，兼言天子諸侯，故又解其言，「唯君使有司為之」。知者，以教成之祭，告事而已，無牲牢。君尊，明使有司為之。</ab>
          </div>
          <div source="#sj015s03e2_com_Mao">
            <head>傳「少女」至「蘋藻」。正義曰：</head>
            <ab>季者，少也。以將嫁，故以少言之，未必伯仲處小也。襄二十八年《左傳》：「濟澤之阿，行潦之蘋藻，寘諸宗室，季蘭屍之，敬也。」隱三年《左傳》曰：「苟有明信，澗谿沼沚之毛，蘋蘩蘊藻之菜，筐筥錡釜之器，潢汙行潦之水，可薦於鬼神，可羞於王公。風有《采蘩》、《采蘋》，雅有《行葦》、《泂酌》，昭忠信也。」二者皆取此篇之義以為說，故傳曆言之。又言「古之將嫁女者，必先禮之於宗室」者，毛意以禮女與教成之祭為一事也。言古之將嫁女者，必先禮之於大宗之室以俟迎者，其牲用魚，芼之以蘋藻，即所設教成之祭也。以此篇說教成之祭事終，故於此總之。毛意以教成之祭與禮女為一者，蓋見《昏禮記》將嫁女之日，「父醴女而俟迎」者，更不見有教成之祭，故謂與禮女為一也。父醴女，以醴酒禮之，今毛傳作禮儀之禮者，《司儀》注云「上於下曰禮」，故《聘禮》用醴酒禮賓，作禮儀之禮。定本「禮」作「醴」。</ab>
          </div>
          <div source="#sj015s03e2_com_ZX">
            <head>箋「主設」至「黍稷」。正義曰：</head>
            <ab>自「無祭事」以上，難毛之辭也。言父禮女，無祭事不得有羹。今經陳采蘋藻為羹，使季女屍之，主設羹者季女，則非禮女也。案《昏禮》女將行嫁，父醴女而俟迎者，其時蓋母薦之，更無祭事，不得有羹矣。今經陳季女設羹，正得為教成之祭，不得為禮女。傳以教成之祭與禮女為一，是毛氏之誤，故非之也。蓋母薦之者，以《士昏禮》云「饗婦姑薦」，鄭注云：「舅獻爵，姑薦脯醢。」舅饗婦既姑薦，明父禮女母薦之可知。故《昏禮記》「父醴女」，注云「父醴之於房中南面，蓋母薦焉，重昏禮」，是也。以無正文，故云「蓋」。知醴之於房中者，以母在房外，故知父禮之在房中也。正祭之禮，主婦設羹。此教成之祭，更使季女設羹者，以三月已來，教之以法度，今為此祭，所以教成其婦禮，故使季女自設其羹也。祭禮主婦設羹，謂《特牲》云 「主婦人及兩鉶鉶芼設於豆南」是也。《少牢》無主婦設羹之事，此宗子或為大夫，其妻不必設羹。要非此祭不得使季女設羹，因《特牲》有主婦設羹之義，故據以言之。又解不言魚者，季女不主魚，魚俎實男子設之，故經不言焉。知俎實男子設之者，以《特牲》、《少牢》俎皆男子主之故也。又魚菜不可空祭，必有其饌，而食事不見，故因約之，「其粢盛蓋以黍稷」耳。知者，以《特牲》、《少牢》止用黍稷，此不得過也。或不用稷，故兼言之。王肅以為，此篇所陳皆是大夫妻助夫氏之祭，采蘋藻以為菹，設之於奧，奧即牖下。又解毛傳禮之宗室，謂教之以禮於宗室，本之季女，取微主也。其毛傳所云「牲用魚，芼之以蘋藻」，亦謂教成之祭，非經文之蘋藻也。自云述毛，非傳旨也。何則？傳稱「古之將嫁女者，必先禮之於宗室」，既言禮之，即云「牲用魚，芼之以蘋藻」，是魚與蘋藻為禮之物。若禮之為以禮教之，則「牲用魚，芼之以蘋藻」何所施乎？明毛以禮女與教成之祭為一，魚為所用之牲矣。而云以禮教之，非傳意也。又上傳云「宗室，大宗之廟。大夫士祭於宗室」，若非教成之祭，則大夫之妻自祭夫氏，何故云大宗之廟？大夫豈皆為宗子也？且大夫之妻助大夫之祭，則無士矣，傳何為兼言「大夫士祭於宗室」乎？又經典未有以奧為牖下者矣。據傳，「禮之宗室」與「大夫士祭於宗室」文同，「芼之以蘋藻」與經采蘋、采藻文協，是毛實以此篇所陳為教成之祭矣。孫毓以王為長，謬矣。</ab>
          </div>
        </div>
      </div>
    </body>
    <back>
      <ab>《采蘋》三章，章四句。</ab>
    </back>
  </text>
</TEI>
