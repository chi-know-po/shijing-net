<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風齊風之甫田（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Futian in the Qifeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="353" to="353"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷五</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷五</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80144&amp;amp;page=39</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj102" type="preface">
        <ab resp="#Mao" xml:id="sj102_com_Mao">《甫田》，大夫刺襄公也。無禮義而求大功，不修德而求諸侯，誌大心勞，所以求者非其道也。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj102 #sj102_com_Mao">
          <head>疏「《甫田》三章，章四句」至「其道」。正義曰：</head>
          <ab>《甫田》詩者，齊之大夫所作以刺襄公也。所以刺之者，以襄公身無禮義，而求己有大功，不能自修其德，而求諸侯從己。有義而後功立，惟德可以來人。今襄公無禮義、無德，諸侯必不從之。其志望大，徒使心勞，而公之所求者非其道也。大夫以公求非其道，故作詩以刺之。求大功與求諸侯，一也，若諸侯從之，則大功克立，所從言之異耳。求大功者，欲求為霸主也。天子衰，諸侯興，故曰霸。《中候》「霸免」，注云： 「霸，猶把也，把天子之事。」於時王室微弱，諸侯無主，齊是大國，故欲求之。鄭以《國語》云「齊莊、僖於是乎小伯」，韋昭曰：「小伯主諸侯盟會。襄即莊孫、僖子，以父祖已作盟會之長，可以為霸業之基。又自以國大民眾，負恃強力，故欲求為霸也。至其弟桓公，即求而得之。」是齊國可以為霸，但襄公無德而不可求耳。上二章刺其求大功，卒章刺其不能修德，皆言其所求非道之事。「勞心忉忉」，是誌大心勞。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj102">
      <lg type="stanza" xml:id="sj102s01">
        <lg type="couplet" xml:id="sj102s01e1">
          <l xml:id="sj102s01l1">無田甫田，</l>
          <l xml:id="sj102s01l2">維莠驕驕。</l>
        </lg>
        <lg type="couplet" xml:id="sj102s01e2">
          <l xml:id="sj102s01l3">無思遠人，</l>
          <l xml:id="sj102s01l4">勞心忉忉。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj102s02">
        <lg type="couplet" xml:id="sj102s02e1">
          <l xml:id="sj102s02l1">無田甫田，</l>
          <l xml:id="sj102s02l2">維莠桀桀。</l>
        </lg>
        <lg type="couplet" xml:id="sj102s02e2">
          <l xml:id="sj102s02l3">無思遠人，</l>
          <l xml:id="sj102s02l4">勞心怛怛。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj102s03">
        <lg type="couplet" xml:id="sj102s03e1">
          <l xml:id="sj102s03l1">婉兮孌兮，</l>
          <l xml:id="sj102s03l2">總角丱兮。</l>
        </lg>
        <lg type="couplet" xml:id="sj102s03e2">
          <l xml:id="sj102s03l3">「未幾見兮，</l>
          <l xml:id="sj102s03l4">突而弁兮！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj102s01" type="commentaries">
      <div source="#sj102s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj102s01e1_com_Mao">
          <ab>興也。甫，大也。大田過度，而無人功，終不能獲。</ab>
        </div>
        <div resp="#ZX" xml:id="sj102s01e1_com_ZX">
          <head>箋云：</head>
          <ab>興者，喻人君欲立功致治，必勤身修德，積小以成高大。</ab>
        </div>
        <div resp="#LDM" xml:id="sj102s01e1_com_LDM">
          <ab>莠，羊九反。無田，音佃，下同。治，直吏反。</ab>
        </div>
      </div>
      <div source="#sj102s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj102s01e2_com_Mao">
          <ab>忉忉，憂勞也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj102s01e2_com_ZX">
          <head>箋云：</head>
          <ab>言無德而求諸侯，徒勞其心忉忉耳。</ab>
        </div>
        <div resp="#LDM" xml:id="sj102s01e2_com_LDM">
          <ab>忉音刀。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj102s01">
          <head>疏「無田」至「忉忉」。正義曰：</head>
          <ab>上田謂墾耕，下田謂土地。以襄公所求非道，故設辭以戒之。言人治田，無得田此大田，若大田過度，力不充給，田必蕪穢，維有莠草驕驕然。以喻公無霸德，思念遠人，若思彼遠人，德不致物，人必不至，維勞其心忉忉然。言人之欲種田求穀，必準功治田，穀乃可獲，喻人君欲立功致治，必勤身修德，功乃可立。無德而求諸侯，徒勞其心也。責襄公之妄求諸侯也。</ab>
        </div>
        <div source="#sj102s01e1_com_Mao">
          <head>傳「甫田」至「能獲」。正義曰：</head>
          <ab>「甫，大」，《釋詁》文。言「無田甫田」，猶《多方》云「宅爾宅田」。爾田，今人謂佃，食古之遺語也。禁人言「無田甫田」，猶下句云「無思遠人」。無田與無思相對為喻。《周禮》授民田，「上地家百畝，中地家二百畝，下地家三百畝」。謂其人力堪治，故禮以此為度。過度，謂過此數而廣治田也。</ab>
        </div>
        <div source="#sj102s01e2_com_Mao">
          <head>傳「忉忉，憂勞」。正義曰：</head>
          <ab>《釋訓》云：「忉忉，憂也。」以言勞心，故云「憂勞也」。</ab>
        </div>
      </div>
    </div>
    <div source="#sj102s02" type="commentaries">
      <div source="#sj102s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj102s02e1_com_Mao">
          <ab>桀桀，猶驕驕也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj102s02e1_com_LDM">
          <ab>桀，居竭反，徐又居謁反。</ab>
        </div>
      </div>
      <div source="#sj102s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj102s02e2_com_Mao">
          <ab>怛怛，猶忉忉也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj102s02e2_com_LDM">
          <ab>怛，旦末反。</ab>
        </div>
      </div>
    </div>
    <div source="#sj102s03" type="commentaries">
      <div source="#sj102s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj102s03e1_com_Mao">
          <ab>婉孌，少好貌。總角，聚兩髦也。丱，幼稚也。弁，冠也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj102s03e1_com_ZX">
          <head>箋云：</head>
          <ab>人君內善其身，外修其德，居無幾何，可以立功，猶是婉孌之童子，少自修飾，丱然而稚，見之無幾何，突耳加冠為成人也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj102s03e1_com_LDM">
          <ab>婉，於阮反。孌，力轉反。緫，本又作揔，子孔反。丱，古患反。幾，居豈反，注同。「見兮」，一本作「見之」。突，吐活反，注同；《方言》云「凡卒相見謂之突」，吐訥反。弁，皮眷反。髦音毛。少，詩照反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj102s03">
          <head>疏「婉兮」至「弁兮」。正義曰：</head>
          <ab>言有童子婉然而少，孌然而好兮，緫聚其發，以為兩角丱然兮，幼稚如此。與別，未經幾時而更見之，突然已加冠弁為成人兮。言童子少自修飾，未幾時而即得成人，以喻人君能善身修德，未幾時而可以立功。今君不修其德，欲求有功，故刺之。</ab>
        </div>
        <div source="#sj102s03_com_Mao">
          <head>傳「婉孌」至「弁冠」。正義曰：</head>
          <ab>《候人》傳曰：「婉，少貌。孌，好貌。」此並訓之，故言少好貌。《內則》云：「男女未冠笄者，緫角，衿纓。」冠所以覆發，未冠則緫角，故知「緫角，聚兩髦」，言緫聚其髦以為兩角也。「丱兮」與「緫角」共文，故為幼稚。《周禮》掌冠冕者，其職謂之弁師，則弁者冠之大號，故為弁冠也。《士冠禮》及《冠義》記士之冠云：「始加緇布冠，次加皮弁，次加爵弁。三加而後字之，成人之道也。」然則士有三加冠。此言「突若弁兮」，指言童子成人加冠而已，不主斥其一冠也。若猶耳也，故箋言「突耳加冠為成人」。《猗嗟》「頎若」，言若者，皆然耳之義，古人語之異耳。定本云「突而弁兮」，不作「若」字。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《甫田》三章，章四句。</ab>
  </back>
</text>
</TEI>

