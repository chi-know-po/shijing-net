<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風王風之黍離（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Shuli in the Wangfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="329" to="331"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=2</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj065" type="preface">
        <ab resp="#Mao" xml:id="sj065_com_Mao">《黍離》，閔宗周也。周大夫行役至於宗周，過故宗廟宮室，盡為禾黍。閔周室之顛覆，彷徨不忍去，而作是詩也。</ab>
        <ab resp="#ZX" xml:id="sj065_com_ZX">宗周，鎬京也，謂之西周。周王城也，謂之東周。幽王之亂而宗周滅，平王東遷，政遂微弱，下列於諸侯，其詩不能復雅，而同於國風焉。</ab>
        <ab resp="#LDM" xml:id="sj065_com_LDM">離，如字，《說文》作「#UNKNOWN離」。過，古臥反，又古禾反。覆，芳服反。彷，蒲皇反。徨音皇。鎬，胡老反。復，扶又反。「而同於國風焉」，崔《集注》本此下更有「猶尊之，故稱王也」。今《詩》本皆無。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj065+_com_poem">
          <head>疏「《黍離》三章，章十句」至「是詩」。正義曰：</head>
          <ab>作《黍離》詩者，言閔宗周也。周之大夫行從征役，至於宗周鎬京，過曆故時宗廟宮室，其地民皆墾耕，盡為禾黍。以先王宮室忽為平田，於是大夫閔傷周室之顛墜覆敗，彷徨省視，不忍速去，而作《黍離》之詩以閔之也。言「過故宗廟」，則是有所適，因過舊墟，非故詣宗周也。周室顛覆，正謂幽王之亂，王室覆滅，致使東遷洛邑，喪其舊都，雖作在平王之時，而誌恨幽王之敗，但主傷宮室生黍稷，非是追刺幽王，故為平王詩耳。又宗周喪滅，非平王之咎，故不刺平王也。「彷徨不忍去」，敘其作詩之意，未必即在宗周而作也。言「宗周宮室，盡為禾黍」，章首上二句是也。「閔周顛覆，彷徨不忍去」，三章下八句是也。言「周大夫行役至於宗周」，敘其所傷之由，於經無所當也。</ab>
        </div>
        <div source="#sj065_com_ZX">
          <head>箋「宗周」至「風焉」。正義曰：</head>
          <ab>鄭先為箋而復作《譜》，故此箋與《譜》大同。《周語》云：「幽王三年，西周三川皆震。」是鎬京謂之西周也，即知王城謂之東周也。《論語》「孔子曰：『如有用我者，吾其為東周乎。』」注云「據時東周則謂成周為東周」者，以敬王去王城而遷於成周，自是以後，謂王城為西周，成周為東周。故昭二十二年，王子猛入於王城，《公羊傳》曰：「王城者何？西周也。」二十六年，天王入於成周，《公羊傳》曰：「成周者何？東周也。」孔子設言之時，在敬王居成周之後，且意取周公之教頑民，故知其為東周，據時成周也。此在敬王之前，王城與鎬京相對，故言王城謂之東周也。《周本紀》云：「平王東徙洛邑，避戎寇。平王之時，周室微弱，諸侯以強並弱，齊、楚、秦、晉始大，政由方伯。」 是平王東遷，政遂微弱。《論語》注云「平王東遷，政始微弱」者，始者，從下本上之辭，遂者，從上鄉下之稱。彼言十世希不失矣，據末而本初，故言始也。此言天子當為雅，從是作風，據盛以及衰，故言遂也。下列於諸侯，謂化之所及，才行境內，政教不加於諸侯，與諸侯齊其列位，故其詩不能復更作大雅、小雅，而與諸侯同為國風焉。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj065">
      <lg type="stanza" xml:id="sj065s01">
        <lg type="couplet" xml:id="sj065s01e1">
          <l xml:id="sj065s01l1">彼黍離離，</l>
          <l xml:id="sj065s01l2">彼稷之苗。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s01e2">
          <l xml:id="sj065s01l3">行邁靡靡，</l>
          <l xml:id="sj065s01l4">中心搖搖。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s01e3">
          <l xml:id="sj065s01l5">知我者，</l>
          <l xml:id="sj065s01l6">謂我心憂，</l>
        </lg>
        <lg type="couplet" xml:id="sj065s01e4">
          <l xml:id="sj065s01l7">不知我者，</l>
          <l xml:id="sj065s01l8">謂我何求。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s01e5">
          <l xml:id="sj065s01l9">悠悠蒼天，</l>
          <l xml:id="sj065s01l10">此何人哉！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj065s02">
        <lg type="couplet" xml:id="sj065s02e1">
          <l xml:id="sj065s02l1">彼黍離離，</l>
          <l xml:id="sj065s02l2">彼稷之穗。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s02e2">
          <l xml:id="sj065s02l3">行邁靡靡，</l>
          <l xml:id="sj065s02l4">中心如醉。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s02e3">
          <l xml:id="sj065s02l5">知我者，</l>
          <l xml:id="sj065s02l6">謂我心憂，</l>
        </lg>
        <lg type="couplet" xml:id="sj065s02e4">
          <l xml:id="sj065s02l7">不知我者，</l>
          <l xml:id="sj065s02l8">謂我何求。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s02e5">
          <l xml:id="sj065s02l9">悠悠蒼天，</l>
          <l xml:id="sj065s02l10">此何人哉！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj065s03">
        <lg type="couplet" xml:id="sj065s03e1">
          <l xml:id="sj065s03l1">彼黍離離，</l>
          <l xml:id="sj065s03l2">彼稷之實。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s03e2">
          <l xml:id="sj065s03l3">行邁靡靡，</l>
          <l xml:id="sj065s03l4">中心如噎。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s03e3">
          <l xml:id="sj065s03l5">知我者，</l>
          <l xml:id="sj065s03l6">謂我心憂，</l>
        </lg>
        <lg type="couplet" xml:id="sj065s03e4">
          <l xml:id="sj065s03l7">不知我者，</l>
          <l xml:id="sj065s03l8">謂我何求。</l>
        </lg>
        <lg type="couplet" xml:id="sj065s03e5">
          <l xml:id="sj065s03l9">悠悠蒼天，</l>
          <l xml:id="sj065s03l10">此何人哉！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj065s01" type="commentaries">
      <div source="#sj065s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj065s01e1_com_Mao">
          <ab>彼，彼宗廟宮室。</ab>
        </div>
        <div resp="#ZX" xml:id="sj065s01e1_com_ZX">
          <head>箋云：</head>
          <ab>宗廟宮室毀壞，而其地盡為禾黍。我以黍離離時至，稷則尚苗。</ab>
        </div>
      </div>
      <div source="#sj065s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj065s01e2_com_Mao">
          <ab>邁，行也。靡靡，猶遲遲也。搖搖，憂無所。</ab>
        </div>
        <div resp="#ZX" xml:id="sj065s01e2_com_ZX">
          <head>箋云：</head>
          <ab>行，道也。道行，猶行道也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj065s01e2_com_LDM">
          <ab>搖音遙。，蘇路反。</ab>
        </div>
      </div>
      <div source="#sj065s01e3" type="commentary">
        <div resp="#ZX" xml:id="sj065s01e3_com_ZX">
          <head>箋云：</head>
          <ab>知我者，知我之情。</ab>
        </div>
      </div>
      <div source="#sj065s01e4" type="commentary">
        <div resp="#ZX" xml:id="sj065s01e4_com_ZX">
          <head>箋云：</head>
          <ab>謂我何求，怪我久留不去。</ab>
        </div>
      </div>
      <div source="#sj065s01e5" type="commentary">
        <div resp="#Mao" xml:id="sj065s01e5_com_Mao">
          <ab>悠悠，遠意。蒼天，以體言之。尊而君之，則稱皇天；元氣廣大，則稱昊天；仁覆閔下，則稱旻天；自上降鑒，則稱上天；據遠視之蒼蒼然，則稱蒼天。</ab>
        </div>
        <div resp="#ZX" xml:id="sj065s01e5_com_ZX">
          <head>箋云：</head>
          <ab>遠乎蒼天，仰欲其察己言也。此亡國之君，何等人哉！疾之甚。</ab>
        </div>
        <div resp="#LDM" xml:id="sj065s01e5_com_LDM">
          <ab>蒼天，本亦作「倉」，采郎反，《爾雅》云：「春為蒼天。」《莊子》云：「天之蒼蒼，其正色邪？」昊，胡老反。夏為昊天。旻，密巾反，閔也。秋為旻天。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj065s01">
          <head>疏「彼黍」至「人哉」。正義曰：</head>
          <ab>鎬京宮室毀壞，其地盡為禾黍。大夫行役，見而傷之，言彼宗廟宮室之地，有黍離離而秀，彼宗廟宮室之地，又有稷之苗矣。大夫見之，在道而行，不忍速去，遲遲然而安舒，中心憂思，搖搖然而無所告訴。大夫乃言，人有知我之情者，則謂我為心憂，不知我之情者，乃謂我之何求乎。見我久留不去，謂我有何所求索。知我者希，無所告語，乃訴之於天。悠悠而遠者，彼蒼蒼之上天，此亡國之君，是何等人哉！而使宗廟丘墟至此也？疾之太甚，故云「此何人哉」！</ab>
        </div>
        <div source="#sj065s01e1_com_Mao">
          <head>傳「彼，彼宗廟宮室」。正義曰：</head>
          <ab>序云「宗廟宮室，盡為禾黍」，故知彼黍彼稷是宗廟宮室之地黍與稷也。作者言彼黍彼稷，正謂黍、稷為彼耳。傳言「彼宗廟宮室」者，言彼宗廟宮室之地有此黍、稷也。</ab>
        </div>
        <div source="#sj065s01e1_com_ZX">
          <head>箋「宗廟」至「尚苗」。正義曰：</head>
          <ab>言毀壞者，以傳文質略，嫌宗廟尚存，階庭生禾黍，故辨之。《湛露》傳曰：「離離，垂然。」則黍離離亦謂秀而垂也。黍言離離，稷言苗，則是黍秀，稷未秀，故云：「我以黍離離時至，稷則尚苗。」苗謂禾未秀。《出車》云「黍稷方華」，則二物大時相類，但以稷比黍，黍差為稙，故黍秀而稷苗也。詩人以黍秀時至，稷則尚苗，六月時也。未得還歸，遂至於稷之穗，七月時也。又至於稷之實，八月時也。是故三章曆道其所更見，稷則穗、實改易，黍則常云離離，欲記其初至，故不變黍文。大夫役當有期而反，但事尚未周了故也。</ab>
        </div>
        <div source="#sj065s01e2_com_Mao">
          <head>傳「邁，行」至「所」。正義曰：</head>
          <ab>「邁，行」，《釋言》文。靡靡，行舒之意，故言猶遲遲也。《釋訓》云：「遲遲，徐也。」《戰國策》云：「楚威王謂蘇秦曰：『寡人心搖搖然如懸旌而無所薄。』」然則搖搖是心憂無所附著之意，故為憂思無所也。</ab>
        </div>
        <div source="#sj065s01e2_com_ZX">
          <head>箋「行，道也。道行，猶行道」。正義曰：</head>
          <ab>今定本文當如此。傳訓經之邁以為行，箋又訓經之行以為道，嫌相涉，故又釋之，云：「道行，猶行道也。」</ab>
        </div>
        <div source="#sj065s01e5_com_Mao">
          <head>傳「悠悠」至「蒼天」。正義曰：</head>
          <ab>《釋詁》云：「悠，遠也。」故知「悠悠，遠意」。《釋天》云：「穹蒼，蒼天。」李巡曰：「古詩人質，仰視天形，穹隆而高，其色蒼蒼，故曰穹蒼。是蒼天以體言之也。皇，君也，故尊而君之，則稱皇天。昊，大貌，故言其混元之氣昊昊廣大，則稱昊天。旻，閔也，言其以仁慈之恩覆閔在下，則稱旻天。從上而下視萬物，則稱上天。據人遠而視之，其色蒼蒼然，則稱蒼天。」然以經、傳言天，其號不一，故因蒼天而總釋之，當有成文，不知出何書。《釋天》云：「春為蒼天，夏為昊天，秋為旻天，冬為上天。」李巡曰：「春，萬物始生，其色蒼蒼，故曰蒼天。夏，萬物盛壯，其氣昊大，故曰昊天。秋，萬物成熟，皆有文章，故曰旻天。冬，陰氣在上，萬物伏藏，故曰上天。」郭璞曰：「旻猶湣也，湣萬物凋落。」冬時無事，在上臨下而已。如《爾雅·釋天》以四時異名，此傳言天，各用所宜為稱，鄭君和合二說，故《異義》天號，「《今尚書》歐陽說：『春曰昊天，夏曰蒼天，秋曰旻天，冬曰上天。』《爾雅》亦云『《古尚書》說與毛同』。謹案：《尚書·堯典》羲、和以昊天，總敕以四時，故知昊天不獨春也。《左傳》『夏四月，孔丘卒』，稱曰『旻天不吊』，非秋也。」玄之聞也，《爾雅》者，孔子門人所作，以釋六藝之言，蓋不誤也。春氣博施，故以廣大言之。夏氣高明，故以達人言之。秋氣或生或殺，故以閔下言之。冬氣閉藏而清察，故以監下言之。皇天者，至尊之號也。六藝之中，諸稱天者，以情所求之耳，非必於其時稱之。「浩浩昊天」，求天之博施。「蒼天蒼天」，求天之高明。「旻天不吊」，求天之生殺當得其宜。「上天同云」，求天之所為當順其時也。此之求天，猶人之說事，各從其主耳。若察於是，則「堯命羲和，欽若昊天」，「孔丘卒，旻天不吊」，無可怪耳。是鄭君和合二說之事也。《爾雅》春為蒼天，夏為昊天；歐陽說春為昊天，夏為蒼天。鄭既言《爾雅》不誤，當從《爾雅》，而又從歐陽之說，以春昊、夏蒼者，鄭《爾雅》與孫、郭本異，故許慎既載《今尚書》說，即言「《爾雅》亦云」明見《爾雅》與歐陽說同，雖蒼、昊有春、夏之殊，則未知孰是，要二物理相符合，故鄭和而釋之。</ab>
        </div>
        <div source="#sj065s01e5_com_ZX">
          <head>箋 「此亡國」至「之甚」。正義曰：</head>
          <ab>《正月》云：「赫赫宗周，褒姒滅之。」亡國之君者，幽王也。《史記·宋世家》云：「箕子朝周，過殷故墟，城壞生黍。箕子傷之，乃作《麥秀》之詩以歌之。其詩曰：『麥秀漸漸兮，禾黍油油兮。彼狡童兮，不我好兮。』所謂狡童者，紂也。」過殷墟而傷紂，明此亦傷幽王，但不是主刺幽王，故不為雅耳。何等人猶言何物人，大夫非為不知，而言何物人，疾之甚也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj065s02" type="commentaries">
      <div source="#sj065s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj065s02e1_com_Mao">
          <ab>穗，秀也。詩人自黍離離見稷之穗，故曆道其所更見。</ab>
        </div>
        <div resp="#LDM" xml:id="sj065s02e1_com_LDM">
          <ab>穗音遂。更音庚。</ab>
        </div>
      </div>
      <div source="#sj065s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj065s02e2_com_Mao">
          <ab>醉於憂也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj065s03" type="commentaries">
      <div source="#sj065s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj065s03e1_com_Mao">
          <ab>自黍離離見稷之實。</ab>
        </div>
      </div>
      <div source="#sj065s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj065s03e2_com_Mao">
          <ab>噎，憂不能息也。</ab>
        </div>
        <div resp="#E3" xml:id="sj065s03e2_com_E3">
          <ab>知我者，謂我心憂，不知我者，謂我何求。悠悠蒼天，此何人哉！</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj065s03e2_com_Mao">
          <head>疏傳「噎，憂不能息」。正義曰：</head>
          <ab>噎者，咽喉蔽塞之名，而言中心如噎，故知憂深，不能喘息，如噎之然。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《黍離》三章，章十句。</ab>
  </back>
</text>
</TEI>

