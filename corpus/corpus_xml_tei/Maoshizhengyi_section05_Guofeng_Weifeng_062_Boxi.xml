<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風衛風之伯兮（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Boxi in the Weifeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="326" to="327"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷三</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷三</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80142&amp;amp;page=122</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj062" type="preface">
        <ab resp="#Mao" xml:id="sj062_com_Mao">《伯兮》，刺時也。言君子行役，為王前驅，過時而不反焉。</ab>
        <ab resp="#ZX" xml:id="sj062_com_ZX">衛宣公之時，蔡人、衛人、陳人從王伐鄭。伯也為王前驅久，故家人思之。</ab>
        <ab resp="#LDM" xml:id="sj062_com_LDM">為，於偽反，又如字。注下「為王」並同。「從王伐鄭」，讀者或連下「伯也」為句者，非。</ab>
      </div>
    </div>
    <div type="poem" xml:id="sj062">
      <lg type="stanza" xml:id="sj062s01">
        <lg type="couplet" xml:id="sj062s01e1">
          <l xml:id="sj062s01l1">伯兮兮，</l>
          <l xml:id="sj062s01l2">邦之桀兮。</l>
        </lg>
        <lg type="couplet" xml:id="sj062s01e2">
          <l xml:id="sj062s01l3">伯也執殳，</l>
          <l xml:id="sj062s01l4">為王前驅。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj062s02">
        <lg type="couplet" xml:id="sj062s02e1">
          <l xml:id="sj062s02l1">自伯之東，</l>
          <l xml:id="sj062s02l2">首如飛蓬。</l>
        </lg>
        <lg type="couplet" xml:id="sj062s02e2">
          <l xml:id="sj062s02l3">豈無膏沐？</l>
          <l xml:id="sj062s02l4">誰適為容！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj062s03">
        <lg type="couplet" xml:id="sj062s03e1">
          <l xml:id="sj062s03l1">其雨其雨，</l>
          <l xml:id="sj062s03l2">杲杲出日。</l>
        </lg>
        <lg type="couplet" xml:id="sj062s03e2">
          <l xml:id="sj062s03l3">願言思伯，</l>
          <l xml:id="sj062s03l4">甘心首疾！</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj062s04">
        <lg type="couplet" xml:id="sj062s04e1">
          <l xml:id="sj062s04l1">焉得諼草，</l>
          <l xml:id="sj062s04l2">言樹之背？</l>
        </lg>
        <lg type="couplet" xml:id="sj062s04e2">
          <l xml:id="sj062s04l3">願言思伯，</l>
          <l xml:id="sj062s04l4">使我心痗！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj062s01" type="commentaries">
      <div source="#sj062s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj062s01e1_com_Mao">
          <ab>伯，州伯也。，武貌。桀，特立也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj062s01e1_com_ZX">
          <head>箋云：</head>
          <ab>伯，君子字也。桀，英桀，言賢也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s01e1_com_LDM">
          <ab>，丘列反。桀，其列反。</ab>
        </div>
      </div>
      <div source="#sj062s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj062s01e2_com_Mao">
          <ab>殳長丈二而無刃。</ab>
        </div>
        <div resp="#ZX" xml:id="sj062s01e2_com_ZX">
          <head>箋云：</head>
          <ab>兵車六等，軫也，戈也，人也，殳也，車戟也，酋矛也，皆以四尺為差。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s01e2_com_LDM">
          <ab>殳，市朱反。長如字，又直亮反。軫，本亦作「爾」，之忍反。酋，在由反，發聲。矛音謀。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj062s01e1_com_Mao">
          <head>疏傳「伯州伯」至「特立」。正義曰：</head>
          <ab>言為王前驅，則非賤者。今言伯兮，故知為州伯，謂州裏之伯。若牧下州伯，則諸侯也，非衛人所得為諸侯之州長也。謂之伯者，伯，長也。《內則》云「州史獻諸州伯，州伯命藏諸州府」。彼州伯對閭史、閭府，亦謂州裏之伯。傑者，俊秀之名，人莫能及，故云特立。</ab>
        </div>
        <div source="#sj062s01e1_com_ZX">
          <head>箋 「伯，君子字」。正義曰：</head>
          <ab>伯、仲、叔、季，長幼之字，而婦人所稱云伯也，宜呼其字，不當言其官也。此在前驅而執兵，則有勇力，為車右，當亦有官，但不必州長為之。為武貌，則傑為有德，故云英傑。傑亦特立，與傳一也。</ab>
        </div>
        <div source="#sj062s01e2_com_Mao">
          <head>疏傳「殳長丈二而無刃」。正義曰：</head>
          <ab>《考工記》云：「殳長尋有四尺。」尋八尺，又加四尺，是丈二也。冶氏為戈戟之刃，不言殳刃，是無刃也。</ab>
        </div>
        <div source="#sj062s01e2_com_ZX">
          <head>箋「兵車」至「為差」。正義曰：</head>
          <ab>因殳是兵車之所有，故曆言六等之差。《考工記》曰：「兵車六等之數：車軫四尺，謂之一等。戈祕六尺有六寸，既建而迤，崇於軫四尺，謂之二等。人長八尺，崇於戈四尺，謂之三等。殳長尋有四尺，崇於人四尺，謂之四等。車戟常崇於殳四尺，謂之五等。酋矛常有四尺，崇於戟四尺，謂之六等。」是也。彼注云：「戈、殳、戟、矛皆插車奇。」此云執之者，在車當插，用則執之，此據用以言也。又《廬人》先言戈、殳、車戟、酋矛、夷矛之長短，乃云「攻國之兵」。又云：「六建既備，車不反覆。」注云：「六建，五兵與人也。」則六建於六等不數軫而數夷矛。不引之者，因六等自軫曆數人殳以上為差之備故。引之六等者，自地以上數之，其等差有六，故注云「法《易》之三才六畫」，非六建也。建者，建於車上，非車上所建也。凡兵車皆有六建，故《廬人》先言戈、殳、車戟、酋矛、夷矛，乃云「攻國之兵」，又云「六建既備」，六建在車，明矣。但記者因酋矛、夷矛同為矛稱，故自軫至矛為六等，象三材之六畫，故不數夷矛。其實六建與六等一也。若自戈以上數為六等，則人於六建不處其中。故鄭云「車有天地之象，人在其中焉」，明為由此，故自軫數之，以戈、軫為地材。人、殳為人材，矛、戟為天材，人處地上，故在殳下。如此則得其象矣。或以為，凡兵車則六建，前驅則六等。知不然者，以《考工記》「兵車六等之數」，鄭云「此所謂兵車也」，明兵車皆然，非獨前驅也。前驅在車之右，其當有勇力以用五兵，不得無夷矛也。《司兵》云「掌五兵」，鄭司農云：「五兵者，戈、殳、戟、酋矛、夷矛。」又曰：「軍事，建車之五兵。」注云：「車之五兵，司農所云者是也。」步卒之五兵則無夷矛，而有弓矢，則前驅非步卒，必有夷矛明矣。知步卒五兵與在車不同者，《司右》云：「凡國之勇力之士，能用五兵者屬焉。」注云：「勇力之士屬焉者，選右當於中。」《司馬法》云弓矢、殳、矛、戈、戟相助，「凡五兵，長以衛短，短以救長」。以《司兵》云「建車之五兵」，則步卒五兵與車兵異矣。夷矛長，非步卒所宜用，故以《司馬法》五兵弓矢、殳、矛、戈、戟當之。車之五兵云「建」，與「六建」文同，故以司農所云戈、殳、戟、酋矛、夷矛當之。勇力之士屬司右，選右當於中，則仍是步卒，未為右也，故以步卒五兵解之。步卒無夷矛，數弓矢為五兵，在車則六建，除人即五兵。以弓矢不在建中，故不數也。其實兵車皆有弓矢，故《司弓矢》云：「唐大利車戰、野戰。枉矢、絜矢用諸守城、車戰。」又《檀弓》注云：「射者在左。」又《左傳》曰：「前驅歂犬，射而殺之。」是皆有弓矢也。</ab>
        </div>
      </div>
    </div>
    <div source="#sj062s02" type="commentaries">
      <div source="#sj062s02l2" type="commentary">
        <div resp="#Mao" xml:id="sj062s02l2_com_Mao">
          <ab>婦人，夫不在，無容飾。</ab>
        </div>
      </div>
      <div source="#sj062s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj062s02e2_com_Mao">
          <ab>適，主也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s02e2_com_LDM">
          <ab>適，都曆反，注同。為，於偽反，或如字。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj062s02l1">
          <head>疏「自伯之東」。正義曰：</head>
          <ab>此時從王伐鄭，鄭在衛之西南，而言東者，時蔡、衛、陳三國從王伐鄭，則兵至京師乃東行伐鄭也。上云「為王前驅」，即云「自伯之東」，明從王為前驅而東行，故據以言之，非謂鄭在衛東。</ab>
        </div>
      </div>
    </div>
    <div source="#sj062s03" type="commentaries">
      <div source="#sj062s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj062s03e1_com_Mao">
          <ab>杲杲然日復出矣。</ab>
        </div>
        <div resp="#ZX" xml:id="sj062s03e1_com_ZX">
          <head>箋云：</head>
          <ab>人言其雨其雨，而杲杲然日復出，猶我言伯且來，伯且來，則復不來。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s03e1_com_LDM">
          <ab>杲，右老反。出如字，沈推類反。復，扶又反，下同。</ab>
        </div>
      </div>
      <div source="#sj062s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj062s03e2_com_Mao">
          <ab>甘，厭也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj062s03e2_com_ZX">
          <head>箋云：</head>
          <ab>願，念也。我念思伯，心不能已。如人心嗜欲所貪，口味不能絕也。我憂思以生首疾。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s03e2_com_LDM">
          <ab>厭，於豔反，下同。嗜，市誌反。憂思，息嗣反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj062s03e2">
          <head>疏「願言思伯，甘心首疾」。</head>
          <ab>毛於《二子乘舟》傳曰：「願，每也。」則此「願」亦為「每」。言我每有所言，則思念於伯，思之厭足於心，由此故生首疾。</ab>
        </div>
        <div source="#sj062s03e2_com_Mao">
          <head>傳「甘，厭」。正義曰：</head>
          <ab>謂思之不已，乃厭足於心，用是生首疾也。凡人飲食口甘，遂至於厭足，故云「甘，厭也」。</ab>
        </div>
        <div source="#sj062s03e2_com_ZX">
          <head>箋「如人」至「不能絕」。正義曰：</head>
          <ab>箋以甘心者，思之不能已，如口味之甘，故《左傳》云「請受而甘心焉」。始欲取以甘心，則甘心未得為厭，故云「我念思伯，心不能已」。如人心嗜欲，甘口不能絕。「甘與子同夢」，義亦然。</ab>
        </div>
      </div>
    </div>
    <div source="#sj062s04" type="commentaries">
      <div source="#sj062s04e1" type="commentary">
        <div resp="#Mao" xml:id="sj062s04e1_com_Mao">
          <ab>諼草令人善忘，背北堂也。</ab>
        </div>
        <div resp="#ZX" xml:id="sj062s04e1_com_ZX">
          <head>箋云：</head>
          <ab>憂以生疾，恐將危身，欲忘之。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s04e1_com_LDM">
          <ab>焉，於虔反。諼，本又作「萱」，況爰反，《說文》作 「藼」，云「令人忘憂也」，或作「蕿」。背音佩，沈又如字。令，力呈反。忘，亡向反，又如字。</ab>
        </div>
      </div>
      <div source="#sj062s04e2" type="commentary">
        <div resp="#Mao" xml:id="sj062s04e2_com_Mao">
          <ab>痗，病也。</ab>
        </div>
        <div resp="#LDM" xml:id="sj062s04e2_com_LDM">
          <ab>痗音每，又音悔。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj062s04">
          <head>疏「焉得」至「心痗」。</head>
          <ab>毛以為，君子既過時不反，己思之至甚，既生首疾，恐以危身，故言我憂如此，何處得一忘憂之草，我樹之於北堂之上，冀觀之以忘憂。伯也既久而不來，每有所言思此伯也，使我心病。鄭以「願」為「念」為異。</ab>
        </div>
        <div source="#sj062s04e1_com_Mao">
          <head>傳「諼草」至「北堂」。正義曰：</head>
          <ab>諼訓為忘，非草名，故傳本其意，言焉得諼草，謂欲得令人善忘憂之草，不謂諼為草名，故《釋訓》云：「諼，忘也。」孫氏引《詩》云「焉得諼草」，是諼非草名也。背者，鄉北之義，故知在北。婦人欲樹草於堂上，冀數見之，明非遠地也。婦人所常處者，堂也，故知北堂。《士昏禮》云「婦洗在北堂」，《有司徹》云「致爵於主婦，主婦北堂」，注皆云： 「北堂，房半以北為北堂。堂者，房室所居之地，總謂之堂。房半以北為北堂，房半以南為南堂也。」《昏禮》注云：「洗南北直室東隅，東西直房戶與隅間。」謂在房室之內也。此欲樹草，蓋在房室之北。堂者，總名，房外內皆名為堂也。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《伯兮》四章，章四句。</ab>
  </back>
</text>
</TEI>

