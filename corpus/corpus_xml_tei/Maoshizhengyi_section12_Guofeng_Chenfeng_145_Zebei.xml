<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風陳風之澤陂（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Zebei in the Chenfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="379" to="379"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷七</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷七</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80145&amp;amp;page=88</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div type="preface" source="#sjChenfeng">
        <ab resp="#Mao" xml:id="sjChenfeng_com_Mao">《澤陂》，刺時也。言靈公君臣淫於其國，男女相說，憂思感傷焉。</ab>
        <ab resp="#ZX" xml:id="sjChenfeng_com_ZX">君臣淫於國，謂與孔寧、儀行父也。感傷，謂涕泗滂沱。</ab>
        <ab resp="#LDM" xml:id="sjChenfeng_com_LDM">陂，彼皮反。思，息嗣反。父音甫。涕，他弟反，自目曰涕。泗音四，自鼻曰泗。滂，普光反。沱，徒何反，下文同。</ab>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjChenfeng_com_Mao">
          <head>疏「《澤陂》三章，章六句」至「傷焉」。正義曰：</head>
          <ab>作《澤陂》詩者，刺時也。由靈公與孔寧、儀行父等君臣並淫於其國之內，共通夏姬，國人效之，男女遞相悅愛，為此淫泆。毛以為，男女相悅，為此無禮，故君子惡之，憂思感傷焉。憂思時世之淫亂，感傷女人之無禮也。男女相悅者，章首上二句是也。感傷者，次二句是也。憂思者，下二句是也。言靈公君臣淫於其國者，本其男女相悅之由，由化效君上，故言之耳，於經無所當也。經先感傷，序先憂思者，經以章首二句既言男女之美好，因傷女而為惡行，傷而不己，故至於憂思，事之次也。序以感傷憂思，為事既同，取其語便，故先言憂思也。鄭以為，由靈公君臣淫於其國，故國人淫泆，男女相悅。聚會則共相悅愛，別離則憂思感傷，言其相思之極也。男女相悅者，章首上二句是也。憂思者，次二句是也。感傷者，下二句是也。毛於「傷如之何」下傳曰「傷無禮」，則是君子傷此「有美一人」之無禮也，「傷如之何」。既傷「有美一人」之無禮，「寤寐無為」二句又在其下，是為憂思感傷時世之淫亂也。此君子所傷，傷此「有美一人」，而「有美一人」又承蒲、荷之下，則蒲、荷二物共喻一女。上二句皆是男悅女之辭也。經文止舉其男悅女，明女亦悅男，不然則不得共為淫矣。故序言「男女相悅」以明之。三章大意皆同。首章言荷，指芙蕖之莖。卒章言菡萏，指芙蕖之華。二者皆取華之美以喻女色，但變文以取韻耳。二章言蘭者，蘭是芬香之草，喻女有善聞。此淫泆之女，必無善聲聞，但悅者之意言其善耳。鄭以為，首章上二句，同姓之中有男悅女、女悅男，是其男女相悅也。次二句言離別之後，不能相見，念之而為憂思也。既憂不能相見，故下二句感傷而淚下。首章言荷，喻女之容體。二章言蓮，喻女之言信。卒章言菡萏，以喻女之色美。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sjChenfeng">
      <lg type="stanza" xml:id="sjChenfengs01">
        <lg type="couplet" xml:id="sjChenfengs01e1">
          <l xml:id="sjChenfengs01l1">彼澤之陂，</l>
          <l xml:id="sjChenfengs01l2">有蒲與荷。</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs01e2">
          <l xml:id="sjChenfengs01l3">有美一人，</l>
          <l xml:id="sjChenfengs01l4">傷如之何！</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs01e3">
          <l xml:id="sjChenfengs01l5">寤寐無為，</l>
          <l xml:id="sjChenfengs01l6">涕泗滂沱。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjChenfengs02">
        <lg type="couplet" xml:id="sjChenfengs02e1">
          <l xml:id="sjChenfengs02l1">彼澤之陂，</l>
          <l xml:id="sjChenfengs02l2">有蒲與蕳。</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs02e2">
          <l xml:id="sjChenfengs02l3">有美一人，</l>
          <l xml:id="sjChenfengs02l4">碩大且卷。</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs02e3">
          <l xml:id="sjChenfengs02l5">寤寐無為，</l>
          <l xml:id="sjChenfengs02l6">中心悁悁。</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sjChenfengs03">
        <lg type="couplet" xml:id="sjChenfengs03e1">
          <l xml:id="sjChenfengs03l1">彼澤之陂，</l>
          <l xml:id="sjChenfengs03l2">有蒲菡萏。</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs03e2">
          <l xml:id="sjChenfengs03l3">有美一人，</l>
          <l xml:id="sjChenfengs03l4">碩大且儼。</l>
        </lg>
        <lg type="couplet" xml:id="sjChenfengs03e3">
          <l xml:id="sjChenfengs03l5">寤寐無為，</l>
          <l xml:id="sjChenfengs03l6">輾轉伏枕。</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div type="commentaries" source="#sjChenfengs01">
      <div type="commentary" source="#sjChenfengs01e1">
        <div resp="#Mao" xml:id="sjChenfengs01e1_com_Mao">
          <ab>興也。陂，澤障也。荷，芙蕖也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjChenfengs01e1_com_ZX">
          <head>箋云：</head>
          <ab>蒲，柔滑之物。芙蕖之莖曰荷，生而佼大。興者，蒲以喻所說男之性，荷以喻所說女之容體也。正以陂中二物興者，喻淫風由同姓生。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs01e1_com_LDM">
          <ab>荷音荷。障，章亮反。夫音符，本亦作「芙」，下同。渠，其居反，本亦作「蕖」。莖，幸耕反。佼，古卯反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs01e2">
        <div resp="#Mao" xml:id="sjChenfengs01e2_com_Mao">
          <ab>傷無禮也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjChenfengs01e2_com_ZX">
          <head>箋云：</head>
          <ab>傷，思也。我思此美人，當如之何而得見之。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs01e3">
        <div resp="#Mao" xml:id="sjChenfengs01e3_com_Mao">
          <ab>自目曰涕，自鼻曰泗。</ab>
        </div>
        <div resp="#ZX" xml:id="sjChenfengs01e3_com_ZX">
          <head>箋云：</head>
          <ab>寤，覺也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs01e3_com_LDM">
          <ab>覺音教。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjChenfengs01e1_com_Mao">
          <head>傳「陂，澤障。荷，芙蕖」。正義曰：</head>
          <ab>澤障，謂澤畔障水之岸。以陂內有此二物，故舉陂畔言之，二物非生於陂上也。《釋草》云：「荷，芙蕖。其莖茄，其葉蕸，其本蔤，其華菡萏，其實蓮，其根藕，其中的，的中薏。」李巡曰：「皆分別蓮莖葉華實之名。菡萏，蓮華也。的，蓮實也。薏，中心也。」郭璞曰：「蔤，莖下白蒻在泥中者。今江東人呼荷華為芙蓉，北方人便以藕為荷，亦以蓮為荷。蜀人以藕為茄。或用其母為華名，或用根子為母葉號。此皆名相錯，習俗傳誤，失其正體者也。」陸機《疏》云：「蓮青皮裏白子為的，的中有青為薏，味甚苦。故里語云『苦如薏』是也。」傳正解荷為芙蕖，不言興意。以下傳云「傷無禮」者，傷「有美一人」，則此「有蒲與荷」，共喻美人之貌。蒲草柔滑，荷有紅華，喻必以象，當以蒲喻女之容體，以華喻女之顏色。當如下章言菡萏，而此云荷者，以荷是此草大名，故取荷為韻。</ab>
        </div>
        <div source="#sjChenfengs01e1_com_ZX">
          <head>箋「蒲柔」至「姓生」。正義曰：</head>
          <ab>如《爾雅》，則芙蕖之莖曰茄。此言荷者，意欲取莖為喻，亦以荷為大名，故言荷耳。樊光注《爾雅》，引《詩》「有蒲與茄」，然則《詩》本有作「茄」字者也。箋以序云「男女相悅」，則經中當有相悅之言，以蒲喻所悅男之性。女悅男，言男之心性和柔似蒲也。荷以喻所悅女之容體。男悅女，言女形體佼大如荷也。正以陂中二物興者，淫風由同姓生，二物共在一陂，猶男女同在一姓。</ab>
        </div>
        <div source="#sjChenfengs01e2_com_ZX">
          <head>箋「傷思」至「見之」。正義曰：</head>
          <ab>「傷，思」，《釋言》文。以《溱洧》、《桑中》亦刺淫泆，舉其事而惡自見，其文皆無哀傷之言，此何獨傷其無禮，至於涕泗滂沱，輾轉伏枕也？故易傳以為思美人不得見之而憂傷也。孫毓以箋義為長。</ab>
        </div>
        <div source="#sjChenfengs01e3_com_Mao">
          <head>傳「自目」至「曰泗」。正義曰：</head>
          <ab>「經、傳言隕涕出涕，皆謂淚出於目。泗既非涕，亦涕之類，明其泗出於鼻也。〉</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjChenfengs02">
      <div type="commentary" source="#sjChenfengs02e1">
        <div resp="#Mao" xml:id="sjChenfengs02e1_com_Mao">
          <ab>蕳，蘭也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjChenfengs02e1_com_ZX">
          <head>箋云：</head>
          <ab>蕳當作「蓮」。蓮，芙蕖實也。蓮以喻女之言信。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs02e1_com_LDM">
          <ab>蕳，毛古顏反，鄭改作「蓮」，練田反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs02e2">
        <div resp="#Mao" xml:id="sjChenfengs02e2_com_Mao">
          <ab>卷，好貌。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs02e2_com_LDM">
          <ab>卷，本又作「卷」，同其員反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs02e3">
        <div resp="#Mao" xml:id="sjChenfengs02e3_com_Mao">
          <ab>悁悁，猶悒悒也。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs02e3_com_LDM">
          <ab>悁，烏玄反。疏傳「悁悁，猶悒悒」。</ab>
        </div>
      </div>
      <div type="subcommentary" resp="#KYD">
        <div source="#sjChenfengs02e1_com_Mao">
          <head>疏傳「蕳，蘭」。正義曰：</head>
          <ab>以《溱洧》「秉蕳」為執蘭，則知此蕳亦為蘭也。蘭是芬香之草，蓋喻女有聲聞。</ab>
        </div>
        <div source="#sjChenfengs02e1_com_ZX">
          <head>箋「蕳當」至「言信」。正義曰：</head>
          <ab>以上下皆言蒲、荷，則此章亦當為荷，不宜別據他草。且蘭是陸草，非澤中之物，故知蘭當作「蓮」，蓮是荷實，故喻女言信實。</ab>
        </div>
        <div source="#sjChenfengs02e2_com_Mao">
          <head>疏傳「悁悁，猶悒悒」。正義曰：</head>
          <ab>俗本多無之。</ab>
        </div>
      </div>
    </div>
    <div type="commentaries" source="#sjChenfengs03">
      <div type="commentary" source="#sjChenfengs03e1">
        <div resp="#Mao" xml:id="sjChenfengs03e1_com_Mao">
          <ab>菡萏，荷華也。</ab>
        </div>
        <div resp="#ZX" xml:id="sjChenfengs03e1_com_ZX">
          <head>箋云：</head>
          <ab>華以喻女之顏色。</ab>
        </div>
        <div resp="#LDM" xml:id="sjChenfengs03e1_com_LDM">
          <ab>菡，本又作「莟」，又作「𣣖」，戶感反。萏，本又作「𦻁」，大感反。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs03e2">
        <div resp="#Mao" xml:id="sjChenfengs03e2_com_Mao">
          <ab>儼，矜莊貌。</ab>
        </div>
      </div>
      <div type="commentary" source="#sjChenfengs03e3">
        <div resp="#LDM" xml:id="sjChenfengs03e3_com_LDM">
          <ab>輾，張輦反，本又作「展」。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《澤陂》三章，章六句。</ab>
  </back>
</text>
</TEI>

