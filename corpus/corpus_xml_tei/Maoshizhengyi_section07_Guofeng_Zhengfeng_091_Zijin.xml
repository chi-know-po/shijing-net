<?xml version="1.0" encoding="utf-8"?>
 <TEI xmlns:tei="http://www.tei-c.org/ns/1.0" version="5.0">
  <teiHeader>
  <fileDesc>
    <titleStmt>
      <title type="main" xml:lang="lzh">毛詩正義：國風鄭風之子衿（電子版）</title>
      <title type="alt" xml:lang="en">The Correct Meaning of the Poems according to Mao: Zijin in the Zhengfeng section of the Airs of the States (An electronic version)</title>
      <title type="short">TBD</title>
      <editor>
        <persName xml:id="MBL">
          <persName xml:lang="en">
            <forename>Marie</forename>
            <surname>Bizais-Lillig</surname>
          </persName>
          <persName xml:lang="zh">
            <forename>茉莉</forename>
            <surname>畢</surname>
          </persName>
          <ptr type="Orcid" target="http://orcid.org/0000-0002-2426-2641"/>
        </persName>
      </editor>
      <funder xml:lang="fr">Université de Strasbourg</funder>
      <funder xml:lang="en">University of Strasbourg</funder>
    </titleStmt>
    <editionStmt>
      <edition>Electronic edition: This edition aims at making visible the structure of the text - i.e. to set the preface and the lines of the poems out and to distinguish the voices of three main orthordox commentators, namely a certain Sir Mao, also known under the name of Mao Heng (1st century BC), Zheng Xuan (127-200) and Kong Yingda (574-648). As commentators make use of external ressources to interpret the Poems, identifiable ressources have also been tagged. For indication of status (low=draft/medium=completed/high=revised), see the editorial declaration and the revision section in the header.</edition>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName xml:lang="en" xml:id="IW">
          <forename>Ilaine</forename>
          <surname>Wang</surname>
        </persName>
      </respStmt>
    </editionStmt>
    <publicationStmt>
      <authority>Université de Strasbourg</authority>
      <pubPlace>France</pubPlace>
      <date>2020</date>
      <idno type="DOI">xxx</idno>
      <availability status="restricted">
        <licence target="https://creativecommons.org/licenses/by-sa/4.0/"> Creative Commons CC-BY-SA (Attribution-ShareAlike) 4.0 International Public License</licence>
      </availability>
    </publicationStmt>
    <seriesStmt>
      <title type="main" xml:lang="lzh"> 毛詩正義（電子版）</title>
      <title type="lat" xml:lang="en">The Correct Meaning of the Poems according to Mao (An electronic version)</title>
      <respStmt>
        <resp>Project Leader and Main Researcher</resp>
        <persName corresp="#MBL"/>
      </respStmt>
      <respStmt>
        <resp>Research Engineer</resp>
        <persName corresp="#IW"/>
      </respStmt>
    </seriesStmt>
    <notesStmt>
      <note>
        <p>The anthology of poems hereby edited is also known under the following names: the Classic of Poetry, le Classique des Poèmes, le Classique des Odes, les Odes.</p>
        <p>This edition takes as a reference the printed version of the Thirteen Classics 十三經. Its production was made easier by a previous digital version published on Wikisource (licensed: CC-BY_SA). It was revised, especially to indicate variants, thanks to the consultation of other editions.</p>
      </note>
    </notesStmt>
    <sourceDesc>
      <listPerson>
       <person role="First Commentator">
         <persName xml:id="Mao">
           <persName xml:lang="en">
             <forename>Heng</forename>
             <surname>Mao</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>亨</forename>
             <surname>毛</surname>
           </persName>
           <idno type="VIAF">53144772</idno>
         </persName>
       </person>
       <person role="Second Commentator">
         <persName xml:id="ZX">
           <persName xml:lang="en">
             <forename>Xuan</forename>
             <surname>Zheng</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>玄</forename>
             <surname>鄭</surname>
           </persName>
           <idno type="VIAF">62469483</idno>
         </persName>
       </person>
       <person role="Third Commentator">
         <persName xml:id="KYD">
           <persName xml:lang="en">
             <forename>Yingda</forename>
             <surname>Kong</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>穎達</forename>
             <surname>孔</surname>
           </persName>
           <idno type="VIAF">36920373</idno>
         </persName>
       </person>
       <person role="Phonetic Commentator">
         <persName xml:id="LDM">
           <persName xml:lang="en">
             <forename>Deming</forename>
             <surname>Lu</surname>
           </persName>
           <persName xml:lang="zh">
             <forename>德明</forename>
             <surname>陸</surname>
           </persName>
           <idno type="VIAF">27916739</idno>
         </persName>
       </person>
      </listPerson>
      <listBibl>
        <head>Reference Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
            <respStmt>
              <resp>Main Editor</resp>
              <persName corresp="#VIAF_36920373"/>
            </respStmt>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">十三經注疏</title>
            <edition>Reprint of the Siku Quanshu Edition</edition>
            <imprint>
              <publisher>Shanghai guji chubanshe 上海古籍出版社</publisher>
              <pubPlace>Shanghai 上海</pubPlace>
              <date when="1997"/>
              <biblScope unit="page" from="345" to="345"/>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title type="main" xml:lang="lzh">毛詩正義</title>
            <title type="sub" xml:lang="lzh">卷四</title>
            <ref type="url">https://zh.wikisource.org/wiki/毛詩正義/卷四</ref>
            <edition>Wikisource edition</edition>
            <imprint>
              <date when="2020"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
      <listBibl>
        <head>Consulted Works</head>
        <biblStruct>
          <analytic>
            <title level="a" xml:lang="lzh">毛詩正義</title>
          </analytic>
          <monogr>
            <title level="s" xml:lang="lzh">武英殿十三經注疏</title>
            <ref type="url">https://ctext.org/library.pl?if=en&amp;amp;file=80143&amp;amp;page=117</ref>
            <edition>Facsimile of Old Chinese Edition</edition>
            <imprint>
              <date when="1871">同治十年</date>
            </imprint>
          </monogr>
        </biblStruct>
        <biblStruct>
          <monogr>
            <title>詩經注析</title>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Junying</forename>
                  <surname>Cheng</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>俊英</forename>
                  <surname>程</surname>
                </persName>
              </persName>
            </respStmt>
            <respStmt>
              <resp>Editor</resp>
              <persName>
                <persName xml:lang="en">
                  <forename>Jiangyuan</forename>
                  <surname>Jiang</surname>
                </persName>
                <persName xml:lang="zh">
                  <forename>見元</forename>
                  <surname>蔣</surname>
                </persName>
              </persName>
            </respStmt>
            <edition>Chinese Modern Edition</edition>
            <imprint>
              <publisher>Zhonghua shuju</publisher>
              <pubPlace>Beijing</pubPlace>
              <date when="1991"/>
            </imprint>
          </monogr>
        </biblStruct>
      </listBibl>
    </sourceDesc>
  </fileDesc>
  <encodingDesc>
    <projectDesc>
      <p>Développer ici</p>
    </projectDesc>
    <editorialDecl>
      <correction status="low"><p>The editorial work on the text consist mainly in bringing to light three elements. The first one is the structure of the text that is made of different layers, i.e. the poem, a preface attributed to a certain Mao Heng, commentaries on words, lines, couplets or stanzas by Mao Heng and by Zheng Xuan, phonological annotations by Lu Deming, and subcommentaries by Kong Yingda related to words, lines, couplets, stanzas, or the entire poem, but also to Mao Heng and Zheng Xuan's commentaries. The edition also includes identification of quotations within commentaries. Finally, it takes into account editorial variants.</p>
      </correction>
      <punctuation>
        <p>Modern punctuation is added to the text. Within poems, a general principle of separating linked lines by simple comma is applied. Within prose, commas and periods are proposed, although some passages could be alternatively interpreted. Titles are identified using quotation marks of this type 《Title》, whereas citations begin and end with quotation marks of that type 「Citation」.</p>
      </punctuation>
      <quotation>
        <p>As often as possible, each citation is linked to its source (either a person or a title) and identified between quotation marks. Beginning of citation passage is identified using "cit" tag, followed by "ref" if source is identified, and by "quote" for quoted text material.</p>
      </quotation>
    </editorialDecl>
    <tagsDecl>
      <namespace name="http://www.tei-c.org/ns/1.0">
        <tagUsage gi="front">"front" is used at the beginning of the file and includes two parts ("div"): an introduction, and the poem. The introduction is subdivided in two parts ("types"): the preface, and subcommentaries. The preface might be authored distinctly by Mao Heng, Zheng Xuan or Lu Deming ("resp"). Subcommentaries were produced by Kong Yingda and might either of the prefaces.</tagUsage>
        <tagUsage gi="div">"div" is used in the "front" section to separate the introductory texts (prefaces and subcommentaries) from the poem itself. In the "body" section, it is used to distinguish commentaries, attributed to Mao Heng, Zheng Xuan and Lu Deming, from subcommentaries by Kong Yingda and his team. Commentaries are also subdividied ("div") depending on the line, the group of lines or the stanza it refers to.</tagUsage>
        <tagUsage gi="back">"back" is used to tag the sentence which wraps basic information about the poem just before the title of the next poem appears. It is composed of the title of the poem, the number of stanzas that compose it, and the number of lines for each stanza.</tagUsage>
      </namespace>
    </tagsDecl>
    <classDecl>
      <taxonomy>
        <category xml:id="commentaries">
          <catDesc>Commentaries</catDesc>
          <category xml:id="commentary">
            <catDesc>Commentary</catDesc>
          </category>
          <category xml:id="subcommentary">
            <catDesc>Subcommentary</catDesc>
          </category>
        </category>
      </taxonomy>
    </classDecl>
    <appInfo>
      <application version="1.0" ident="x">
        <label>xxx</label>
        <ptr target="#X0"/>
      </application>
    </appInfo>
  </encodingDesc>
  <profileDesc>
    <langUsage>
      <language ident="lzh">Literary Chinese</language>
      <language ident="och">Old Chinese </language>
      <language ident="ltc">Late Middle Chinese </language>
      <language ident="en">English</language>
      <language ident="fr">French</language>
    </langUsage>
   </profileDesc>
  <revisionDesc>
    <change when="2020" who="#MBL"><p> First version. As indicated in the editorial section, this file's status is considered low as the text has not been fully proofread.</p></change>
  </revisionDesc>
</teiHeader>
  <text>
  <front>
    <div type="introduction">
      <div source="#sj091" type="preface">
        <ab resp="#Mao" xml:id="sj091_com_Mao">《子矜》，刺學校廢也。亂世則學校不脩焉。</ab>
        <ab resp="#ZX" xml:id="sj091_com_ZX">鄭國謂學為校，言可以校正道藝。</ab>
        <ab resp="#LDM" xml:id="sj091_com_LDM">衿音金，本亦作「襟」，徐音琴。「世亂」，本或以世字在下者，誤。校，力孝反，注及下注同。注傳云「鄭人遊於鄉校」是也。公孫弘云：「夏曰校。」沈音教。</ab>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj091 #sj091_com_Mao">
          <head>疏「《子衿》三章，章四句」至「不脩焉」。正義曰：</head>
          <ab>鄭國衰亂，不脩學校，學者分散，或去或留，故陳其留者恨責去者之辭，以刺學校之廢也。經三章，皆陳留者責去者之辭也。定本云「刺學廢也」，無「校」字。</ab>
        </div>
        <div source="#sj091_com_ZX">
          <head>箋「鄭國」至「道藝」。正義曰：</head>
          <ab>襄三十一年《左傳》云：「鄭人遊於鄉校。」然明謂子產毀鄉校，是鄭國謂學為校，校是學之別名，故序連言之。又稱其名校之意，言於其中可以校正道藝，故曰校也。此序非鄭人言之，箋見《左傳》有鄭人稱校之言，故引以為證耳，非謂鄭國獨稱校也。《漢書》公孫弘奏云：「三代之道，鄉里有教，夏曰校，殷曰庠，周曰序。」是古亦名學為校也。禮：「人君立大學小學。」言學校廢者，謂鄭國之人廢於學問耳，非謂廢毀學宮也。</ab>
        </div>
      </div>
    </div>
    <div type="poem" xml:id="sj091">
      <lg type="stanza" xml:id="sj091s01">
        <lg type="couplet" xml:id="sj091s01e1">
          <l xml:id="sj091s01l1">青青子衿，</l>
          <l xml:id="sj091s01l2">悠悠我心。</l>
        </lg>
        <lg type="couplet" xml:id="sj091s01e2">
          <l xml:id="sj091s01l3">縱我不往，</l>
          <l xml:id="sj091s01l4">子寧不嗣音？</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj091s02">
        <lg type="couplet" xml:id="sj091s02e1">
          <l xml:id="sj091s02l1">青青子佩，</l>
          <l xml:id="sj091s02l2">悠悠我思。</l>
        </lg>
        <lg type="couplet" xml:id="sj091s02e2">
          <l xml:id="sj091s02l3">縱我不往，</l>
          <l xml:id="sj091s02l4">子寧不來？</l>
        </lg>
      </lg>
      <lg type="stanza" xml:id="sj091s03">
        <lg type="couplet" xml:id="sj091s03e1">
          <l xml:id="sj091s03l1">挑兮達兮，</l>
          <l xml:id="sj091s03l2">在城闕兮。</l>
        </lg>
        <lg type="couplet" xml:id="sj091s03e2">
          <l xml:id="sj091s03l3">一日不見，</l>
          <l xml:id="sj091s03l4">如三月兮！</l>
        </lg>
      </lg>
    </div>
  </front>
  <body>
    <div source="#sj091s01" type="commentaries">
      <div source="#sj091s01e1" type="commentary">
        <div resp="#Mao" xml:id="sj091s01e1_com_Mao">
          <ab>青衿，青領也，學子之所服。</ab>
        </div>
        <div resp="#ZX" xml:id="sj091s01e1_com_ZX">
          <head>箋云：</head>
          <ab>學子而俱在學校之中，己留彼去，故隨而思之耳。禮：「父母在，衣純以青」。</ab>
        </div>
        <div resp="#LDM" xml:id="sj091s01e1_com_LDM">
          <ab>青如字。學子以青為衣領緣衿也，或作菁，音非純、章允反，又之閏反。</ab>
        </div>
      </div>
      <div source="#sj091s01e2" type="commentary">
        <div resp="#Mao" xml:id="sj091s01e2_com_Mao">
          <ab>嗣，習也。古者教以詩樂，誦之歌之，弦之舞之。</ab>
        </div>
        <div resp="#ZX" xml:id="sj091s01e2_com_ZX">
          <head>箋云：</head>
          <ab>嗣，續也。女曾不傳聲問我，以恩責其忘己。</ab>
        </div>
        <div resp="#LDM" xml:id="sj091s01e2_com_LDM">
          <ab>嗣如字，《韓詩》作「詒」。詒，寄也，曾不寄問也。傳聲，直專反。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj091s01">
          <head>疏「青青」至「嗣音」。</head>
          <ab>毛以為，鄭國學校不修，學人散去，其留者思之言：青青之色者，是彼學子之衣衿也。此青衿之子，棄學而去，悠悠乎我心思而不見，又從而責之。縱使我不往彼見子，子寧得不來學習音樂乎？責其廢業去學也。鄭唯下句為異。言汝何曾不嗣續音聲，傳問於我。責其遺忘己也。</ab>
        </div>
        <div source="#sj091s01e1_com_Mao">
          <head>傳「青衿，青領」。正義曰：</head>
          <ab>《釋器》云：「衣皆謂之襟。」李巡曰：「衣皆，衣領之襟。」孫炎曰：「襟，交領也。」衿與襟音義同。衿是領之別名，故云「青衿，青領也」。衿、領一物。色雖一青，而重言青青者，古人之復言也。下言「青青子佩」，正謂青組綬耳。《都人士》「狐裘黃黃」，謂裘色黃耳，非有二事而重文也。箋云「父母在，衣純以青」，是由所思之人父母在，故言青衿。若無父母，則素衿。《深衣》云：「具父母衣純以青，孤子衣純以素。」是無父母者用素。</ab>
        </div>
        <div source="#sj091s01e2_com_Mao">
          <head>傳「嗣習」至「舞之」。正義曰：</head>
          <ab>所以責其不習者，古者教學子以詩樂，誦之謂背文闇誦之，歌之謂引聲長詠之，弦之謂以琴瑟播之，舞之謂以手足舞之。學樂學詩，皆是音聲之事，故責其不來習音。《王制》云：「樂正崇四術，立四教。春秋教以禮樂，冬夏教以詩書。」《文王世子》云：「春誦夏弦，太師詔之。」注云：「誦，謂歌樂也。弦，謂以絲播詩。」是學詩學樂，皆弦誦歌舞之。</ab>
        </div>
        <div source="#sj091s01e2_com_ZX">
          <head>箋「嗣續」至「忘己」。正義曰：</head>
          <ab>箋以下章云「子寧不來」，責其不來見己，不言來者有所學。則此云「不嗣音」，不宜為習樂，故易傳言留者責去者，子曾不傳續音聲存問我，以恩責其忘己。言與彼有恩，故責其斷絕。</ab>
        </div>
      </div>
    </div>
    <div source="#sj091s02" type="commentaries">
      <div source="#sj091s02e1" type="commentary">
        <div resp="#Mao" xml:id="sj091s02e1_com_Mao">
          <ab>佩，佩玉也。士佩需瑉而青組綬。</ab>
        </div>
        <div resp="#LDM" xml:id="sj091s02e1_com_LDM">
          <ab>鶗，本又作「需」，如兗反。瑉，亡巾反。組音祖。綬音受。</ab>
        </div>
      </div>
      <div source="#sj091s02e2" type="commentary">
        <div resp="#Mao" xml:id="sj091s02e2_com_Mao">
          <ab>不來者，言不一來也。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj091s02e1_com_Mao">
          <head>疏傳「佩，佩玉」至「組綬」。正義曰：</head>
          <ab>《玉藻》云：「古之君子必佩玉，君子於玉比德焉。」故知子佩為佩玉也。禮不佩青玉，而云「青青子佩」者，佩玉以組綬帶之。士佩需瑉而青組綬，故云青青謂組綬也。案《玉藻》「士佩需玟而縕組綬」，此云青組綬者，蓋毛讀《禮記》作青字，其本與鄭異也。學子非士，而傳以士言之，以學子得依士禮故也。</ab>
        </div>
        <div source="#sj091s02e2_com_Mao">
          <head>傳「不來者，言不一來」。正義曰：</head>
          <ab>準上傳，則毛意以為責其不一來習業。鄭雖無箋，當謂不來見己耳。</ab>
        </div>
      </div>
    </div>
    <div source="#sj091s03" type="commentaries">
      <div source="#sj091s03e1" type="commentary">
        <div resp="#Mao" xml:id="sj091s03e1_com_Mao">
          <ab>挑達，往來相見貌。乘城而見闕。</ab>
        </div>
        <div resp="#ZX" xml:id="sj091s03e1_com_ZX">
          <head>箋云：</head>
          <ab>國亂，人廢學業，但好登高見於城闕，以候望為樂。</ab>
        </div>
        <div resp="#LDM" xml:id="sj091s03e1_com_LDM">
          <ab>挑，他羔反，又敕彫反，《說文》作「叟」。達，他末反，《說文》云：「達，不相遇也。」好，呼報反。樂音洛。</ab>
        </div>
      </div>
      <div source="#sj091s03e2" type="commentary">
        <div resp="#Mao" xml:id="sj091s03e2_com_Mao">
          <ab>言禮樂不可一日而廢。</ab>
        </div>
        <div resp="#ZX" xml:id="sj091s03e2_com_ZX">
          <head>箋云：</head>
          <ab>君子之學，以文會友，以友輔仁。獨學而無友，則孤陋而寡聞，故思之甚。</ab>
        </div>
      </div>
      <div resp="#KYD" type="subcommentary">
        <div source="#sj091s03">
          <head>疏「挑兮」至「月兮」。</head>
          <ab>毛以為，學人廢業，候望為樂，故留者責之云：汝何故棄學而去？挑兮達兮，乍往乍來，在於城之闕兮。禮樂之道，不學則廢。一日不見此禮樂，則如三月不見兮，何為廢學而遊觀？鄭以下二句為異。言一日不與汝相見，如三月不見兮。言己思之甚也。</ab>
        </div>
        <div source="#sj091s03e1_com_Mao">
          <head>傳「挑達」至「見闕」。正義曰：</head>
          <ab>城闕雖非居止之處，明其乍往乍來，故知挑達為往來貌。《釋宮》云：「觀謂之闕。」孫炎曰：宮門雙闕，舊章懸焉，使民觀之，因謂之觀。如《爾雅》之文，則闕是人君宮門，非城之所有，且宮門觀闕不宜乘之候望。此言在城闕兮，謂城之上別有高闕，非宮闕也。乘城見於闕者，乘猶登也，故箋申之，登高見於城闕，以候望為樂。</ab>
        </div>
        <div source="#sj091s03e2_com_ZX">
          <head>箋「君子」至「之甚」。正義曰：</head>
          <ab>「君子以文會友，以友輔仁」，《論語》文。「獨學而無友，則孤陋而寡聞」，《學記》文。由其須友以如此，故思之甚。</ab>
        </div>
      </div>
    </div>
  </body>
  <back>
    <ab>《子衿》三章，章四句。</ab>
  </back>
</text>
</TEI>

