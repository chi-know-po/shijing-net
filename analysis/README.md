# Shijing-net: Illustration of Analysis

This repository contains an example of the output of similarity analysis conducted on the prefaces by Mao, Zheng Xuan and Kong Yingda related to the "Airs of the States" section of the poetic *Shijing* 詩經 Anthology.

Analysis was conducted based on Paul Vierthaler's BLAST-based, Language-agnostic Text Reuse Algorithm, as explained in the `scripts` folder.

Scripts were tuned in order to:
- allow it to run on a Mac computer,
- fix some incoherences (respect of match also at the end of lines),
- take into consideration graphic variants.

The output of this adaptation is to be found here: https://github.com/ilaine/chinesetextreuse

Parameters to compare pieces of prose written between 1st century BC and 7th century AD were set as follows:
- when running /detect_intertexuality.py/:
  - seedlength = 4
  - threshold = .5
  - matchlength = 6
  - match_comp = 100
- when running /compile_and_filter_results.py/:
  - shortquotelength = 20
  - repmax = 100
  - similarity threshold = .8
- when running /form_quote_system.py/:
  - scorelimit = 20

Also, the corpus was first limited to poems 3 to 74 of the *Shijing* Anthology (the preface of poem n°1 is very peculiar). Once a first table of comparison was first produced (/edgetable.csv), the corpus was narrowed down to poems n°3, 6, 7, 9, 11, 17, 20, 24, 25, 31; 37, 47, 48, 50 and 65.

Scripts with the above parameters produced the files hereby presented:
1. `/corpus.pickle` was produced by `prepare_corpus.py` (`index_corpus.py` was not used);
2. `results` folder along with `/corpus_text_lengths.txt` and `/completed_files.txt` were produced by `detect_intertexuality.py`;
3. `/corpus_results.txt` was produced by `compile_and_filter_results.py`;
4. `/edgetable.csv` was produced by `form_quote_system.py`;
5. `/corpus_alignment.txt` was produced by `align_quotes.py`;
6. `/edge_data.js` and `/viz.html` were produced by `build_chord_viz.py`.

General outcome of the analysis can be found by simply opening the `viz.html` file. It is not completely satisfactory. It proves however that computer assisted analysis to find similarities and align them can be efficiently done.

It allows for example to identify a high degree of similarity such as the following one, between Zheng Xuan and Kong Yingda's prefaces to poem 11 of the Anthology (identical strings of characters appear in black, slight differences in pink, and addings in green):

![011_preface_ZX_versus_KYD](./011_preface_ZX_versus_KYD.png?raw=true "flowchart")
