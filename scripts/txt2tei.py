#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
usage: python txt2tei.py input_file

description: transforms the preannotated txt files into XML-TEI files.
This script was built for a research project (CHI-KNOW-PO) conducted at
Université de Strasbourg under the supervision of Marie Bizais-Lillig.

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import os
import argparse
import json
import copy

import lxml.etree as etree

from fill_header import *
from to_XML import *

TEI = "{http://www.tei-c.org/ns/1.0}"
XML = "{http://www.w3.org/XML/1998/namespace}"

NSMAP = {"tei": TEI[1:-1], "xml": XML[1:-1]}

OUTPUT_DIR = 'output'

def get_section(text):
    ''' Gets the poem's section
        text: string

        return string
    '''
    return text.split('_')[3]

def get_subsection(text):
    ''' Gets the poem's subsection
        text: string

        return string
    '''
    return text.split('_')[2]

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('input', \
        help='input directory with annotated files of the Shijing')
    parser.add_argument('-o','--output', help='output directory')
    args = parser.parse_args()

    # Create output directory if necessary
    if args.output is None:
        try:
            os.mkdir(OUTPUT_DIR)
            print("folder '{}' created ".format(OUTPUT_DIR))
        except FileExistsError:
            print("folder {} already exists".format(OUTPUT_DIR))
    else:
        OUTPUT_DIR = args.output

    # Get metadata
    with io.open('../databases/poem_data.json', newline='', \
        encoding='utf-8') as meta:
        metadata = json.load(meta)

    # Get header template
    with io.open('./header.xml', encoding='utf-8') as header:
        header_template = etree.parse(header)

    with os.scandir(args.input) as files:
        for file in files:
            # Deal with macOS .DS_Store...
            if file.name.startswith('.'):
                continue
            print(file.name)

            tei_root = etree.Element(f"TEI", nsmap=NSMAP, \
                attrib={f"version": "5.0"})

            tei_header = fill_header(header_template, \
                metadata[os.path.splitext(file.name)[0]])
            copy_header = copy.deepcopy(tei_header.getroot())   # Workaround
            tei_root.insert(0, copy_header)

            tei_body = conversion_XML(args.input + file.name)

            tei_root.append(tei_body)

            with io.open(OUTPUT_DIR + '/' + os.path.splitext(file.name)[0] + '.xml','w', encoding='utf-8') as out:
                print('<?xml version="1.0" encoding="utf-8"?>\n',etree.tostring(tei_root, encoding='unicode', pretty_print=True), file=out)
