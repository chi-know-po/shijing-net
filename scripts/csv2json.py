#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
usage: python csv2json.py

description: extracts information from the concordance file and outputs a json.
This script was built for a research project (CHI-KNOW-PO) conducted at
Université de Strasbourg under the supervision of Marie Bizais-Lillig.

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import csv
import json

data = {}

def replace_keys(d):
    ''' Replaces column titles with simplified English equivalent
        d: dict

        return dict
    '''
    d['No'] = d.pop('N° Poème Shi')
    d['section'] = d.pop('Section Shi')
    d['subsection'] = d.pop('Sous-section ( Shi KYD)')
    d['title'] = d.pop('Titre Poème Shi')
    d['manual_filename'] = d.pop('Titre fichier')
    d['filename'] = d.pop('Titre généré')
    d['link'] = d.pop('Lien Ctext')
    d['from'] = d.pop('Page début')
    d['to'] = d.pop('Page fin')
    return d

with io.open('../databases/concordance_anthology.tsv', newline='', \
    encoding='utf-8') as meta:
    reader = csv.DictReader(meta, delimiter='\t')

    for row in reader:
        simplified_row = replace_keys(row)
        key = simplified_row['filename']
        data[key] = simplified_row

with io.open('../databases/poem_data.json', 'w', encoding='utf-8') as jsonfile:
    json.dump(data, jsonfile, indent=4, ensure_ascii=False)
