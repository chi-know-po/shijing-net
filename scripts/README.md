# Shijing-net: Scripts

This folder contains the scripts that were used to build the *Shijing-Net* corpus.

The role of each script is shown in the following flowchart:

![shijingnet-flowchart](./ShijingNet_corpus_constitution.png?raw=true "flowchart")

The steps of the constitution of the corpus are four-fold:

1. Our corpus is first divided into HTML files, each containing a whole section of poems. The first step consists in *splitting the sections into poems* using a command line:

```shell
cd /path/to/working/folder
mkdir poems_html
cd poems_html
csplit /path/to/html/files/folder/section03.html --elide-empty-files --prefix=Maoshizhengyi_section03_ --suffix-format="%03d" '/<h2/' '{*}'
rename 's/(\d{2})$/$1 + 26/e' * # where 26 is the number of the first poem of the section
```

This command line does not work on macOS because its version of csplit is not up to date. If you use macOS, simply use `csplit -k -f /path/to/html/files/folder/section03.html '/<h2/' '{99}'` and use the Finder to rename files with the right poem number (csplit automatically starts numbering at 01).

2. Then, we can proceed to the *automatic preannotation* of the corpus. During this stage, we use textual hints to automatically identify the poem and its title, the commentaries and their authors, Kong Yingda's subcommentaries as well as the closing sentence(s). To make sure the preannotation is correct, we manually checked and completed the preannotated data.

```shell
cd /path/to/git/scripts
mkdir poems_txt
for file in `ls /path/to/working/folder/poems_html/`; do python preannotation.py /path/to/working/folder/poems_html/$file ; done
```
Resulting files are stored in a `poems_txt` folder created with `mkdir`.

3. The third step is the transformation of the annotated corpus into TEI-compliant XML files.

```shell
python txt2tei.py poems_txt/
```

Resulting files are stored in a folder named `output` (default).
There is no need to run the `csv2json.py` script as the json file is provided. However, if you need to update the metadata in the json, you need to run this script prior to `txt2tei.py`.

4. The corpus is ready to be analysed using a corpus exploration tool. As we choose to use and adapt Paul Vierthaler's chinesetextreuse program, we had to retransform our corpus back into txt files.

```shell
python corpus2analysis.py output -t [poem/preface/commentary/full] -o corpus
```
This script takes three arguments: 
- an input folder (named `output` from the preceding script), 
- the *type* of the target text to be extracted and analysed,
- and the output folder (name `corpus` here).
The type can only be `poem`, `preface`, `commentary` or `full`. If you choose `full`, the resulting txt files will contain the poem, commentaries that are in the preface as well as those in the commentaries section.

The scripts from chinesetextreuse that we adapted to our corpus can be found at https://github.com/ilaine/chinesetextreuse.
