#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
description: module that fills the header for the Shijing's XML-TEI files.
This script was built for a research project (CHI-KNOW-PO) conducted at
Université de Strasbourg under the supervision of Marie Bizais-Lillig.

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import re

section_map = {'國風': 'Airs of the States',
    '小雅': 'Lesser Court Hymns',
    '大雅': 'Major Court Hymns',
    '周頌': 'Eulogies of Zhou',
    '魯頌': 'Eulogies of Lu',
    '商頌': 'Eulogies of Shang',
    }

def fill_header(header, poem_data):
    ''' Provides information that are specific to each poem
        header: lxml.etree.ElementTree
        poem_data: json

        return lxml.etree.ElementTree
    '''
    # titleStmt
    title = header.xpath('//titleStmt/title[@xml:lang="lzh"]')
    if title is not None:
        title[0].text =  fill_chinese_title(poem_data)
    else:
        print('Error from the <title xml:lang="lzh"> tag in <titleStmt>')

    engtitle = header.xpath('//titleStmt/title[@xml:lang="en"]')
    if engtitle is not None:
        engtitle[0].text =  fill_english_title(poem_data)
    else:
        print('Error from the <title xml:lang="en"> tag in <titleStmt>')

    # sourceDesc
    section_num = header.xpath('//sourceDesc//monogr/title[@type="sub"]')
    if section_num is not None:
        section_num[0].text =  get_subtitle(poem_data)
    else:
        print('Error from the <title type="sub" xml:lang="lzh"> tag \
        in <sourceDesc>')

    wiki_url = header.xpath('//title[@type="sub"]/following-sibling::ref')
    if wiki_url is not None:
        wiki_url[0].text =  'https://zh.wikisource.org/wiki/毛詩正義/' +  get_subtitle(poem_data)
    else:
        print('Error from the <ref type="url"> tag for Wikisource')

    ctext_url = header.xpath('//title[@level="s"]/following-sibling::ref')
    if ctext_url is not None:
        ctext_url[0].text =  poem_data['link']
    else:
        print('Error from the <ref type="url"> tag for Ctext')

    biblscope = header.xpath('//biblScope')
    if biblscope is not None:
        biblscope[0].attrib['from'] = poem_data['from']
        biblscope[0].attrib['to'] =  poem_data['to']
    else:
        print('Error from the <biblScope> tag for 十三經注疏')

    return header

def fill_chinese_title(poem_data):
    ''' Builds the Chinese title
        poem_data: json

        return string
    '''
    title = ('毛詩正義：'
    + poem_data['section']
    + poem_data['subsection']
    + '之'
    + poem_data['title']
    + '（電子版）')
    return title

def fill_english_title(poem_data):
    ''' Builds the English title
        poem_data: json

        return string
    '''
    filename = poem_data['filename'].split('_')
    title = ('The Correct Meaning of the Poems according to Mao: '
    + filename[5]
    + ' in the ' + filename[3] + ' section of the '
    + section_map[poem_data['section']]
    + ' (An electronic version)')
    return title

def get_pages(poem_data):
    ''' Builds the chinese title
        poem_data: json

        return string
    '''

def get_subtitle(poem_data):
    ''' Builds the subtitle (alternative section number)
        poem_data: json

        return string
    '''
    return '卷' + get_subtitle_number(poem_data['No'])

def get_subtitle_number(number):
    ''' Builds the chinese title
        number: Integer

        return string
    '''
    cases = {range(1, 26): '一',
        range(26, 45): '二',
        range(45, 65): '三',
        range(65, 96): '四',
        range(96, 114): '五',
        range(114, 136): '六',
        range(136, 155): '七',
        range(155, 161): '八',
        range(161, 180): '九',
        range(180, 194): '十',
        range(194, 197): '十一',
        range(197, 207): '十二',
        range(207, 217): '十三',
        range(217, 227): '十四',
        range(227, 241): '十五',
        range(241, 251): '十六',
        range(251, 261): '十七',
        range(261, 272): '十八',
        range(272, 303): '十九',
        range(303, 312): '二十'
        }

    switch = { k: v for rng, v in cases.items() for k in rng }
    return switch[int(number)]
