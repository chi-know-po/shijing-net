#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
file: corpus2analysis.py
usage: python corpus2analysis.py input_dir -t type -o output_dir

description: extracts text from the XML-TEI version of the Shijing and creates
a corpus to be used as an input for Paul Vierthaler's chinesetextreuse module

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import os
import argparse
import re
import json

import lxml.etree as etree

OUTPUT_DIR = 'corpus'

def extract(xml,poem_data,author,type):
    ''' Extracts text from selected sections
        xml: lxml.etree.ElementTree
        poem_data: json
        author: string

        return lxml.etree.ElementTree
    '''
    id = poem_data['No'].zfill(3)

    # Build title following Vierthaler's recommendations
    title = (id + ' '
        + poem_data['title']
        + '-' + authors_map[author][1] + '-'
        + authors_map[author][0])

    text = ''

    # Search selected sections and store in list
    if type != 'full':
        results = types_map[type][1](xml,id,author)
    else:
        results = []
        full = ['poem','preface','commentary']

        for t in full:
            if t != 'poem':
                author = ['Mao','ZX','KYD']
                for a in author:
                    results.extend(types_map[t][1](xml,id,a))
            else:
                results.extend(types_map[t][1](xml,id,author))
        # Rename era for full
        title = re.sub(u'周',u'唐',title)

    if results != []:
        for item in results:
            text += item.text + '\n'
        return (title,text)
    else:
        return('','')

def get_commentaries(xml,id,author):
    ''' Extracts commentaries from the commentaries part only
        xml: lxml.etree.ElementTree
        id: Integer
        author: string

        return list
    '''
    return xml.xpath(f'//body//div[@resp="#{author}"]//ab')

def get_poem(xml,id,author):
    ''' Extracts the poem line by line
        xml: lxml.etree.ElementTree
        id: Integer
        author: string

        return list
    '''
    return xml.xpath('//l')

def get_preface(xml,id,author):
    ''' Extracts commentaries from the preface only
        xml: lxml.etree.ElementTree
        id: Integer
        author: string

        return list
    '''
    if author != 'KYD':
        # Get Mao and ZX's commentaries
        return xml.xpath(f'//front//ab[@resp="#{author}"]')
    else:
        # Get KYD's subcommentaries
        return xml.xpath('//front//div[@resp="#KYD"]//ab')

authors_map = {'Mao': ['毛','前漢'],
    'ZX': ['鄭玄','後漢'],
    'KYD': ['孔穎達','唐'],
    'MaoshiZhengyi': ['毛詩正義','周'],
    }

types_map = {'commentary': ['c',get_commentaries],
    'poem': ['poem',get_poem],
    'preface': ['p',get_preface],
    'full': ['full'],
    }

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('input', help='input directory with XML files')
    parser.add_argument('-t','--type', \
        choices=['commentary','poem','preface','full'], \
        help='type of text to be extracted', required=True)
    parser.add_argument('-o','--output', help='output directory')
    args = parser.parse_args()

    # Creates output directory if necessary
    if args.output:
        OUTPUT_DIR = args.output
    try:
        os.mkdir(OUTPUT_DIR)
        print(f'Output directory {OUTPUT_DIR} created.')
    except FileExistsError:
        print(f'Output directory {OUTPUT_DIR} already exists')

    # Reads metadata file (json)
    with io.open('../databases/poem_data.json', newline='', \
        encoding='utf-8') as meta:
        metadata = json.load(meta)

    # Extracts and outputs selected sections
    with os.scandir(args.input) as files:
        for file in files:
            print(f'Processing {file.name}')
            with io.open(args.input + '/' + file.name) as input:
                xml_file = etree.parse(input)

            if args.type == 'preface' or args.type == 'commentary':
                authors = ['Mao','ZX','KYD']    # 3 files for p and c
            else:
                authors = ['MaoshiZhengyi'] # Only 1 file for poem and full
            for author in authors:
                (title,text) = extract(xml_file, \
                    metadata[os.path.splitext(file.name)[0]],author,args.type)

                if text != '':
                    type = types_map[args.type][0]
                    with io.open(f'{OUTPUT_DIR}/{title}-{type}.txt','w', \
                    encoding='utf-8') as out:
                        out.write(text)
