#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
usage: python preannotation.py input_file

description: transforms the html source code of the 毛詩正義 from Wikisource
into a preannotated txt for annotation purposes.
This script was built for a research project (CHI-KNOW-PO) conducted at
Université de Strasbourg under the supervision of Marie Bizais-Lillig.

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import sys
import re

import pinyin
from bs4 import BeautifulSoup as bs

def get_filenames(csv):
    ''' Gets the filenames (last column) from metadata csv file
        csv: list of strings

        return list of strings
    '''
    res = []
    i = 0
    subsection = ''

    for lines in csv:
        row = lines.strip('\n').split('\t')

        # Get section number automatically (when subsection name changes)
        if subsection != row[2]:
            subsection = row[2]
            i += 1

        num_subsection = str(i).zfill(2)

        section_name = pinyin.get(row[3], format='strip').capitalize()

        if num_subsection == '02':
            subsection_name = 'Shaonan'    # Correct reading, not Zhaonan
        else:
            subsection_name = pinyin.get(row[2], format='strip').capitalize()

        num_poem = str(int(row[0])).zfill(3)

        if row[0] == '26' or row[0] == '45':
            poem_name = 'Bozhou'    # Correct reading, not Baizhou
        elif row[0] == '20':
            poem_name = 'Biaoyoumei'
        else:
            poem_name = pinyin.get(row[1], format='strip').capitalize()

        res.append('Maoshizhengyi_section'
        + num_subsection + '_'
        + section_name + '_'
        + subsection_name + '_'
        + num_poem + '_'
        + poem_name)
    return res

def get_id(text):
    ''' Gets the poem's number
        text: string

        return Integer
    '''
    return int(text.split('_')[-1])

def get_poem(text):
    ''' Gets the poem verse by verse
        text: BeautifulSoup object (html)

        return string
    '''
    res = ''
    if text.findAll('font',{'color':'navy'}) != []:
        for verse in text.findAll('font',{'color':'navy'}):
            res += verse.text + '\n'
    else:
        for verse in text.findAll('font',{'style':'font-size:115%; color:navy'}):
            res += verse.text + '\n'
    return res

def standardise_verses(text):
    ''' Replaces <span> verses with <font> for uniformity
        text: BeautifulSoup objet (html)

        return BeautifulSoup objet (html)
    '''
    while True:
        span = text.find('span')
        if not span:
            break
        span.name = 'font'
    return text

def pre_treatment(html):
    ''' Replaces missing characters by an #UNKNOWN tag
        html: string

        return string
    '''
    return re.sub(u'<dfn.*?dfn>',u'#UNKNOWN',html)

def post_treatment(text):
    ''' Adds paragraphs based on ○ and the names of annotators that are
        easily identifiable.
        text: string

        return string
    '''
    text = re.sub(u'○', u'\n\t', text)
    text = re.sub(u'\n+\t疏', u'\nsous-commentaire KYD\n\t疏', text)
    text = re.sub(u'(?<!\w)箋云：', u'\nZX\t箋云：', text)
    text = re.sub(u'Mao\t（?\n', u'', text)
    return re.sub(u'正義曰：', u'\n正義曰：', text)

def clean(text):
    ''' Gets rid of parentheses, extra empty lines and spaces
        text: string

        return string
    '''
    text = post_treatment(text)
    text = re.sub(u'\n\n\n', u'\n\n', text)
    text = re.sub(u'[\(\)（）]', u'', text)
    text = re.sub(u'\n\n\t。', u'。', text)
    text = re.sub(u'commentaire\n\n(?!fin)', u'commentaire\n', text)
    text = re.sub(u'\n\n\t', u'\n\t', text)
    text = re.sub(u'\n\t\n正義曰：', u'\n正義曰：', text)
    text = re.sub(u'\t\n\t', u'\t', text)
    text = re.sub(u'\n\t箋', u'\nZX\t箋', text)
    return re.sub(u'\t傳', u'Mao\t傳', text)

res = ''

# Get metadata from concordance file
with io.open('../databases/concordance_anthology.tsv', \
    encoding='utf-8') as meta:
    data = meta.readlines()[1:]

titles = get_filenames(data)

# Open html files
with io.open(sys.argv[1], encoding='utf-8') as f:
    prepage = f.read()
    prepage = pre_treatment(prepage)
    page = bs(prepage, 'html.parser')

id = get_id(sys.argv[1])

# Output folder is 'poems_txt'
output_file = f'poems_txt/{titles[id-1]}.txt'
print(f'Processing {output_file}')

# Get title
res = page.find('span',{'class':'mw-headline'}).string + '\n\n'

# Get poem
res += get_poem(standardise_verses(page))

# Get commentaries and subcommentaries
res += '\npreface\nMao'

i = 0 # counting <p>s
text = page.findAll('p')

for p in text:
    i += 1
    comm = p.findAll(text=True, recursive=False) # commentaries without verses
    mixed_comm = p.find_all(text=re.compile(u'.'),recursive=True) # with verses

    # Case of last <p> (closing line, used as the 'back')
    if i == len(text):
        res += f'fin\n{p.text}'
    # Case of verses
    elif p.font is not None:
        # Case where the final verse has no commentary OR next is also verse
        if i == len(text) - 1 and comm[0] == '\n' \
            or text[i].font is not None and comm[0] == '\n':
            res += f'E1 {p.text}\n'
        # Case where there are several verses and commentaries in a single <p>
        elif comm[0] != '\n':
            j = 0
            for item in mixed_comm:
                # Target verses only
                if j == 0 or (j % 2) == 0:
                    res += f'E {item}\ncommentaire\nMao'
                    j += 1
                # Target commentaries
                else:
                    res += f'\t{item.lstrip()}\n\n'
                    j += 1
        else:
            res += f'E {p.text.rstrip()}\ncommentaire\nMao'
    # Case of commentaries
    else:
        res += f'\t{p.text}\n'

with io.open(output_file,'w', encoding='utf-8') as out:
    out.write(clean(res))
