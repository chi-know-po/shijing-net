#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
description: module that builds the body of the Shijing's XML-TEI files.
This script was built for a research project (CHI-KNOW-PO) conducted at
Université de Strasbourg under the supervision of Marie Bizais-Lillig.

author: Ilaine Wang

MIT License

Copyright (c) 2021 Ilaine Wang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import io
import re

import lxml.etree as etree

section_map = {'國風': 'Airs of the States',
    '小雅': 'Lesser Court Hymns',
    '大雅': 'Major Court Hymns',
    '周頌': 'Eulogies of Zhou',
    '魯頌': 'Eulogies of Lu',
    '商頌': 'Eulogies of Shang',
    }

stanza_map = {1: 'line',
    2: 'couplet',
    3: 'tercet',
    4: 'quatrain',
    }

def conversion_XML(filename):
    ''' Converts the preannotation txt files to XML-TEI files
        filename: string

        return lxml.etree.ElementTree
    '''
    with io.open(filename, encoding='utf-8') as f:
        data = f.read()
        sections = data.split('\n\n')

    root = etree.Element('text')
    tei_front = define_front(sections[1], sections[2], filename.split('_')[4])
    tei_body = define_body(sections[3:-1], filename.split('_')[4])
    tei_back = define_back(sections[-1].split('\n'))
    root.append(tei_front)
    root.append(tei_body)
    root.append(tei_back)

    return get_comments(root)

def define_front(poem,intro,id):
    ''' Builds the <front> in XML-TEI format
        poem: string
        intro: string
        id: string

        return lxml.etree.ElementTree
    '''
    root = etree.Element('front')

    tei_intro = etree.Element('div', type="introduction")
    comments = intro.split('\nsous-commentaire KYD\n')
    tei_intro.append(get_preface(comments[0].split('\n'), id))
    try:
        tei_intro.append(get_subcommentary(comments[1].split('\n'), id))
    except IndexError:
        print('This poem does not have subcommentaries.')
    tei_poem = get_poem(poem.split('\n'), id)
    root.append(tei_intro)
    root.append(tei_poem)

    return root

def define_body(text,id):
    ''' Builds the <body> in XML-TEI format
        text: List
        id: string

        return lxml.etree.ElementTree
    '''
    root = etree.Element('body')

    for section in text:
        lines = section.split('\n')
        stanza_id = "s" + lines[0][1].zfill(2)

        div_coms = etree.SubElement(root,'div', type="commentaries", \
        source="#sj" + id + stanza_id)

        comments = section.split('\nsous-commentaire KYD\n')
        div_com = get_commentary(comments[0].split('\n'), id+stanza_id)
        div_coms.append(div_com)
        etree.strip_tags(div_coms,'group')  # Get rid of unwanted <group>

        if len(comments) == 2:
            div_subcom = get_subcommentary(comments[1].split('\n'), \
                id+stanza_id)
            div_coms.append(div_subcom)
        else:
            print('Cette section n\'a pas de sous-commentaires !')

    return root

def define_back(text):
    ''' Builds the <body> in XML-TEI format
        text: List

        return lxml.etree.ElementTree
    '''
    root = etree.Element('back')

    for line in text[1:]:
        if line != '':
            ab = etree.SubElement(root,'ab')
            ab.text = line
    return root

def get_preface(text, id):
    ''' Converts the preface into a XML-TEI format
        text: string
        id: string

        return lxml.etree.ElementTree
    '''
    div_preface = etree.Element('div', type="preface", source="#sj"+id)

    for line in text[1:]:
        item = line.split('\t')
        source = "#sj" + id

        author_id = "#" + get_author(item[0])
        xml_id = "sj" + id + "_com_" + get_author(item[0])
        ab = etree.Element('ab', attrib={"resp":author_id, \
            "{http://www.w3.org/XML/1998/namespace}id":xml_id})
        ab.text = no_comments(item[1])
        div_preface.append(ab)

    return div_preface

def get_commentary(text,id):
    ''' Converts the commentary section into a XML-TEI format
        text: string
        id: string

        return lxml.etree.ElementTree
    '''
    comments = etree.Element('group')   # Cannot append without super Element

    for num, line in enumerate(text, start=1):
        item = line.split('\t')

        # Case we don't know the author
        if item[0] == '':
            xml_id = ref[1:] + '_com_UNKNOWN'

            div = etree.Element('div', attrib={"resp":'#UNKNOWN', \
                "{http://www.w3.org/XML/1998/namespace}id":xml_id})
            ab = etree.SubElement(div,'ab')
            # Directly get the text from following item
            ab.text = item[1]
            div_com.append(div)
            comments.append(div_com)
        # Case of a regular commentary
        elif len(item) != 1:
            # Case of commentary
            xml_id = clean(ref[1:]) + '_com_' + get_author(item[0])

            div = etree.Element('div', \
                attrib={"resp":'#' + get_author(item[0]), \
                    "{http://www.w3.org/XML/1998/namespace}id":xml_id})
            if re.match(r'箋云：',item[1]):
                head = etree.SubElement(div,'head')
                head.text = '箋云：'
                item[1] = item[1][3:]
            ab = etree.SubElement(div,'ab')
            # Directly get the text from following item
            ab.text = no_comments(item[1])  # Version without comments

            div_com.append(div)
            comments.append(div_com)
        # Case this is the end of the commentary section
        elif num == len(text):
            break
        # Cases of verses/elements
        elif text[num] == 'commentaire':
            src = get_theme(item[0].split(' ')[0],id)
            div_com = etree.Element('div', type='commentary', source=src)
            ref = '#sj' + id + item[0].split(' ')[0].lower()

    return comments

def clean(text):
    text = re.sub(r'S\d$',u'',text)
    return re.sub(u'\+',u'',text)

def no_comment(text):
    ''' Deletes comments left by Marie for Ilaine in the preannotated files
        text: string

        return string
    '''
    return re.sub(r'<!--.+?-->',u'',text)

def no_comments(text):
    ''' Deletes comments left by Marie for analysis purposes
        Useful for the project so here we can comment the return line
        text: string

        return string
    '''
    # return re.sub(r'<!--.+?-->',u'',text) # Version without comments
    return text # version with comments

def get_theme(ref,id):
    ''' Gets the object of the subcommentary (recursive if multiplereferences)
        ref: string
        id: string

        return string
    '''
    if '+' in ref:
        ref = re.sub(u' \+ ',u'+',ref)
        refs = ref.split('+')
        multiref = ''
        for item in refs:
            multiref += get_theme(item,id) + ' '
        return multiref[:-1]
    else:
        return '#sj' + id + ref.lower()

def get_subcommentary(text,id):
    ''' Converts the subcommentary section into a XML-TEI format
        poem: string
        id: string

        return lxml.etree.ElementTree
    '''
    div_subcom = etree.Element('div', type="subcommentary", \
    resp="#"+get_author("KYD"))

    for num, line in enumerate(text, start=1):
        item = line.split('\t')

        if re.match(r'[EL]\d ',item[0]):
            break
        elif len(item) != 1:
            # Case of the head
            div = etree.Element('div', source=get_reference(item[0],id))
            if re.match(r'正義曰：',text[num]):
                item[1] += '正義曰：'
                text[num] = text[num][4:]
            head = etree.SubElement(div,'head')
            ab = etree.SubElement(div, 'ab')
            head.text = item[1]
            # Directly get the text from following item
            ab.text = no_comments(text[num])
            div_subcom.append(div)

    return div_subcom

def get_reference(ref, id):
    ''' Converts the preannotation into @source ID
        ref: string
        id: string

        return string
    '''
    sj_s = '#sj' + id
    items = ref.split(' ')
    # Unique case of subcom on the poem as a whole
    if ref.lower() == 'poem':
        return '#sj' + id[:3]
    # Unique case of "Poem + Mao"
    if ref == 'Poem + Mao' or ref == 'Poem+Mao':
        return '#sj' + id[:3] + " " + sj_s + '_com_Mao'
    # Case of single stanza
    elif re.match(r'S\d', items[0]) and len(items) == 1:
        return sj_s
    # Case of single element
    elif re.match(r'[EL]\d', items[0]) and len(items) == 1:
        return sj_s + items[0].lower()
    # Case of single author
    elif len(items) == 1 and '+' not in ref:
        return sj_s + '_com_' + get_author(ref)
    elif '+' not in ref:
        # Need to clean in case of "Mao S1"
        return sj_s + clean(items[1]).lower() + '_com_' + get_author(items[0])
    else:
        items[1] = re.sub(u' \+ ',u'+',items[1])
        return sj_s + items[1].lower() + '_com_' + get_author(items[0])

def get_author(name):
    ''' Gets author's ID
        name: string

        return string
    '''
    if ' ' in name:
        return name.split(' ')[0]
    return name

def get_poem(poem, id):
    ''' Converts the poem into a XML-TEI format
        poem: string
        id: string

        return lxml.etree.ElementTree
    '''
    div_poem = etree.Element('div', attrib={"type":"poem", \
        "{http://www.w3.org/XML/1998/namespace}id":'sj' + id})
    for item in poem:
        if len(item) == 2:
            stanza_id = item[1].zfill(2)
            count = 1
            line_count = 0

            xml_id = 'sj' + id + 's' + stanza_id
            stanza = etree.Element('lg', attrib={"type":"stanza", \
                "{http://www.w3.org/XML/1998/namespace}id":xml_id})
        else:
            # stanza_id = item[1].zfill(2)
            xml_id = 'sj' + id + 's' + stanza_id + 'e' + str(count)

            # Get verses (non-empty strings)
            cln_item = re.sub(u'[「」]',u'',no_comment(item))
            verses = list(filter(None,re.split('ㆍ|[^\s\w《》/「」：]',cln_item)))
            type = stanza_map[len(verses)]

            substanza = etree.SubElement(stanza, 'lg', attrib={"type":type, \
                "{http://www.w3.org/XML/1998/namespace}id":xml_id})

            unit = re.sub(u'((ㆍ|[^\s\w《》/「」：]))', r'\1 ', \
                no_comment(item).split('\t')[1])
            unit = re.sub(u' 」', u'」 ', unit)
            for line in unit.split(' ')[:-1]:
                line_count += 1
                l = etree.SubElement(substanza,'l', attrib={ \
                    "{http://www.w3.org/XML/1998/namespace}id":xml_id[:-2] + \
                    'l' + str(line_count)})
                l.text = line
            count += 1
        div_poem.append(stanza)
    return div_poem

def get_comments(tree):
    ''' Wordaround to actually keep the comments in the XML files
        tree: lxml.etree.ElementTree

        return lxml.etree.ElementTree
    '''
    text = etree.tostring(tree, pretty_print=True,encoding='unicode')
    text = re.sub(r'\&lt;!--(.*?)--\&gt;',r'<!--\1-->',text)
    return etree.fromstring(text)
